package com.gem.mailbox.integration;

import com.gem.mailbox.dto.UserDTO;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 2/28/15
 * Time: 9:34 AM
 */
public class UserServiceITest extends InitialTest
{
    @Test
    public void testRegisterNewUserSuccess() throws Exception
    {
        UserDTO requestedUserDTO = new UserDTO();
        requestedUserDTO.setEmail("test@gmail.com");
        requestedUserDTO.setFirstName("test");
        requestedUserDTO.setPassword("123456");

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                .post("/register")
                .content(asJsonString(requestedUserDTO))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();

        String result = mvcResult.getResponse().getContentAsString();
        assertEquals(mvcResult.getResponse().getStatus(), 200);
    }

}
