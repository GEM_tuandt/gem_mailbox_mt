package com.gem.mailbox.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gem.mailbox.Application;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 2/28/15
 * Time: 9:11 AM
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest("server.port:8080")
public abstract class InitialTest
{

    protected MockMvc mvc;


    @Autowired
    protected WebApplicationContext wac;

    @Before
    public void setUp()
    {
        mvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @After
    public void tearDown()
    {

    }

    public String asJsonString(Object object)
    {
        try
        {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(object);
            return jsonContent;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

}
