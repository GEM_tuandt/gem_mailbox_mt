package com.gem.mailbox.mapping;

import com.gem.mailbox.dto.LetterDTO;
import com.gem.mailbox.dto.UserDTO;
import com.gem.mailbox.model.Letter;
import org.dozer.DozerBeanMapper;
import org.dozer.MappingException;
import org.dozer.loader.api.BeanMappingBuilder;
import org.dozer.loader.api.TypeMappingOptions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * MapUtils
 * Created by neo on 1/29/2016.
 */
public class MapUtils {
    /* DozerMapperBean is thread safe so no locking */
    private static final DozerBeanMapper MAPPER;

    static {
        MAPPER = new DozerBeanMapper();
        BeanMappingBuilder builder = new BeanMappingBuilder() {
            protected void configure() {
                mapping(UUID.class, UUID.class, TypeMappingOptions.oneWay(), TypeMappingOptions.beanFactory(UuidBeanFactory.class.getName()));
            }
        };
        MAPPER.addMapping(builder);
    }

    private MapUtils() {
    }

    public static <T> List<T> mapList(List sourceList, Class<T> destination) throws MappingException {
        List<T> ret = new ArrayList<>();
        for (Object source : sourceList) {
            if (source != null) {
                ret.add(map(source, destination));
            }
        }

        return ret;
    }

    public static <T> T map(Object source, Class<T> destination) throws MappingException {
        return MAPPER.map(source, destination);
    }

    public static List<LetterDTO> map(List<Letter> letters, boolean includeUser) throws MappingException {
        List<LetterDTO> letterDTOs = new ArrayList<>();
        for (Letter letter : letters) {
            // Mapping Letter to LetterDTO
            LetterDTO letterDTO = MapUtils.map(letter, LetterDTO.class);
            if (letterDTO == null) {
                continue;
            }

            // Assign user for letter
            if (includeUser) {
                UserDTO userDTO = MapUtils.map(letter.getUser(), UserDTO.class);
                if (userDTO != null)
                    letterDTO.setUserDTO(userDTO);
            }

            letterDTOs.add(letterDTO);
        }

        return letterDTOs;
    }
}
