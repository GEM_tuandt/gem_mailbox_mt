package com.gem.mailbox.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.format.Formatter;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 3/2/15
 * Time: 9:06 AM
 */
public class DateConverter implements Formatter<Date>, Converter<String, Date> {
    @Override
    public Date convert(String dateTimeStamp) {
        return new Date(Long.parseLong(dateTimeStamp));
    }

    @Override
    public Date parse(String text, Locale locale) throws ParseException {
        return new Date(Long.parseLong(text));
    }

    @Override
    public String print(Date object, Locale locale) {
//        return object.toLocaleString();
        DateFormat formatter = DateFormat.getDateTimeInstance();
        return formatter.format(this);
    }
}
