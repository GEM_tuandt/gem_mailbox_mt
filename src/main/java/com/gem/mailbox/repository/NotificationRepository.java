package com.gem.mailbox.repository;

import com.gem.mailbox.model.Letter;
import com.gem.mailbox.model.Notification;
import com.gem.mailbox.model.User;
import com.gem.mailbox.model.UserEmail;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

/**
 * Notification Repository
 * Created by neo on 1/30/2016.
 */
@Repository
public interface NotificationRepository extends CrudRepository<Notification, UUID> {

    List<Notification> findByUser(User user, Pageable pageable);

    List<Notification> findByUuidIn(List<UUID> notificationIds);
}
