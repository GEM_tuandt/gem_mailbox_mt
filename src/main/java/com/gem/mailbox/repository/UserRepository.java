package com.gem.mailbox.repository;

import com.gem.mailbox.model.Letter;
import com.gem.mailbox.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

/**
 * User Repository
 * Created by quynhtq on 12/11/14.
 */
@Repository
public interface UserRepository extends CrudRepository<User, UUID> {
    public User findByEmail(String email);

    public List<User> findByIsAdminTrue();

//    @Query("SELECT u, l, COUNT(l.uuid) AS letter_count FROM User u RIGHT OUTER JOIN FETCH u.letters l WHERE l.isScan = false GROUP BY u.uuid")
//    List<User> findUserHaveSendLetter();
}
