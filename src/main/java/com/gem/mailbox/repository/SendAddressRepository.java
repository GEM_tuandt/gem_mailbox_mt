package com.gem.mailbox.repository;

import com.gem.mailbox.model.SendAddress;
import com.gem.mailbox.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Letter Repository
 * Created by neo on 1/30/2016.
 */
@Repository
public interface SendAddressRepository extends CrudRepository<SendAddress, UUID> {
    SendAddress findByAddressAndUser(String address, User user);
}
