package com.gem.mailbox.repository;

import com.gem.mailbox.model.Letter;
import com.gem.mailbox.model.LetterScan;
import com.gem.mailbox.model.ServiceProvider;
import com.gem.mailbox.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

/**
 * Letter Repository
 * Created by neo on 1/30/2016.
 */
@Repository
public interface ServiceProviderRepository extends CrudRepository<ServiceProvider, UUID> {
    public ServiceProvider findByProviderName(String providerName);

    public List<LetterScan> findByUuidIn(List<UUID> uuids);


    // usa2me.com
    //ipostal1.com
    //UPS, FedEx, usps

}
