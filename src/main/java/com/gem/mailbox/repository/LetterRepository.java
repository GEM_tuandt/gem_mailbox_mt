package com.gem.mailbox.repository;

import com.gem.mailbox.model.Letter;
import com.gem.mailbox.model.LetterScan;
import com.gem.mailbox.model.SendOrder;
import com.gem.mailbox.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Letter Repository
 * Created by neo on 1/30/2016.
 */
@Repository
public interface LetterRepository extends CrudRepository<Letter, UUID> {
    List<Letter> findByIsNew(Boolean isNew);

    List<Letter> findByIsSend(Boolean isSend);

    List<Letter> findByIsScanFalseAndIsSendIsNullAndIsTrashIsNull();

    List<Letter> findByUserAndIsTrashFalseOrderByDateTrashAsc(User user);

    @Query("SELECT l FROM Letter l " +
            "WHERE l.user=:user " +
            "AND l.isTrash = FALSE " +
            "ORDER BY l.dateTrash NULLS FIRST")
    List<Letter> findTrashLettersByUser(@Param("user") User user);

    @Query("SELECT l FROM Letter l " +
            "WHERE l.user=:user " +
            "AND (l.isSend IS NULL OR l.isSend = FALSE) " +
            "AND l.isTrash IS NULL " +
            "AND (l.isInDocument IS NULL OR l.isInDocument = FALSE)" +
            "ORDER BY l.dateUpdated DESC")
    List<Letter> findLettersCustomer(@Param("user") User user);

    List<Letter> findByUuidIn(List<UUID> uuids);

    List<Letter> findByUserAndIsScanFalseAndIsSendIsNullAndIsTrashIsNullAndUuidNotInOrderByDateUpdatedAsc(User user, List<UUID> uuids);
    List<Letter> findByUserAndIsScanFalseAndIsSendIsNullAndIsTrashIsNullOrderByDateUpdatedAsc(User user);

    @Query("SELECT l FROM Letter l " +
            "WHERE l.user=:user " +
            "AND l.isTrash = FALSE " +
            "AND (l.dateTrash IS NULL OR l.dateTrash < :dateTrash) " +
            "ORDER BY l.dateTrash ASC")
    List<Letter> findAdminTrashLetterByUser(@Param("user") User user, @Param("dateTrash")Date dateTrash);

    @Query("SELECT l FROM Letter l " +
            "WHERE l.isTrash = FALSE " +
            "AND (l.dateTrash IS NULL OR l.dateTrash < :dateTrash) " +
            "ORDER BY l.dateTrash ASC")
    List<Letter> findAdminTrashList(@Param("dateTrash")Date dateTrash);

    Letter findByQrCode(String qrCode);

    List<Letter> findBySendOrder(SendOrder sendOrder);

    List<Letter> findByUuidInAndSendOrderIsNotNull(List<UUID> uuids);

    @Query("SELECT l FROM Letter l " +
            "WHERE l.user=:user " +
            "AND l.isInDocument = TRUE " +
            "AND l.isTrash IS NULL " +
            "ORDER BY l.dateUpdated DESC ")
    List<Letter> findLettersInDocument(@Param("user")User user);
}
