package com.gem.mailbox.repository;

import com.gem.mailbox.model.Session;
import com.gem.mailbox.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 2/12/15
 * Time: 9:18 AM
 */
@Repository
public interface SessionRepository extends CrudRepository<Session, UUID> {
    List<Session> findByUser(User user);

    Session findByToken(String token);

    List<Session> findByUserAndDeviceTokenNot(User user, String deviceToken);

    List<Session> findByUserAndDeviceToken(User user, String deviceToken);

    List<Session> findByDeviceToken(String deviceToken);
}
