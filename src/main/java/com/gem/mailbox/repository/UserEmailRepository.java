package com.gem.mailbox.repository;

import com.gem.mailbox.model.OtherName;
import com.gem.mailbox.model.UserEmail;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Letter Repository
 * Created by neo on 1/30/2016.
 */
@Repository
public interface UserEmailRepository extends CrudRepository<UserEmail, UUID> {

}
