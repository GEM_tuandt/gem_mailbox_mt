package com.gem.mailbox.repository;

import com.gem.mailbox.model.LetterScan;
import com.gem.mailbox.model.SendOrder;
import com.gem.mailbox.model.ServiceProvider;
import com.gem.mailbox.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

/**
 * Letter Repository
 * Created by neo on 1/30/2016.
 */
@Repository
public interface SendOrderRepository extends CrudRepository<SendOrder, UUID> {
    /**
     * Find all orders that being sent to user
     */
//    @Query("SELECT s FROM SendOrder s LEFT JOIN FETCH Letter l WHERE l.sendOrder IS NULL")
    List<SendOrder> findByUserAndIsArrivedFalseAndIsSentTrue(User user);

    List<SendOrder> findByIsSentFalse();
}
