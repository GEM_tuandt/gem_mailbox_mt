package com.gem.mailbox.repository;

import com.gem.mailbox.model.LetterScan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 2/27/15
 * Time: 9:46 AM
 */
@Repository
public interface LetterScanRepository extends CrudRepository<LetterScan, UUID> {
    public List<LetterScan> findByUuidIn(List<UUID> documents);

//    public List<LetterScan> findByUserAndDateUpdatedAfter(User user, Date lastUpdated, Pageable pageable);

//    public List<LetterScan> findByUser(User user, Pageable pageable);

//    public LetterScan findByUserAndUuid(User user, UUID uuid);

//    List<LetterScan> findByUserAndImageTypeAndEntityIdIn(User user, ImageType imageType, List<UUID> entityIdList);

//    List<LetterScan> findByUserAndUuidIn(User user, List<UUID> uuidList);

//    List<LetterScan> findByUserAndEntityIdInAndUuidNotIn(User user, List<UUID> insuranceIdList, List<UUID> frontAndBackImageList);

//    List<LetterScan> findByEntityIdInAndNameIsNotNullAndDeleted(List<UUID> vaccinationIdList, boolean deleted);
}
