package com.gem.mailbox.repository;

import com.gem.mailbox.model.JobHistory;
import com.gem.mailbox.model.SendOrder;
import com.gem.mailbox.model.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

/**
 * Job History Repository
 * Created by neo on 1/30/2016.
 */
@Repository
public interface JobHistoryRepository extends CrudRepository<JobHistory, UUID> {
    List<JobHistory> findByUser(User user, Pageable pageable);
}
