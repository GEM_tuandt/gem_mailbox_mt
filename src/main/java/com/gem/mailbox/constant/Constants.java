package com.gem.mailbox.constant;

import org.apache.commons.lang.time.DateUtils;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 2/24/15
 * Time: 4:53 PM
 */
public class Constants {
    public static final String SIMULATOR_DEVICE_TOKEN = "SIMULATOR";

    public static final String THUMBNAIL_PREFIX = "Thumb_";
    public static final String LETTER_THUMBNAIL_PREFIX = "Letter_";
    public static final int THUMBNAIL_PREFERRED_SIZE = 200;
    public static final int PDF_PREFERRED_SIZE = 530;


    public static final String PDF = "PDF_";

    public static final String[] SERVICE_PROVIDERS;


    // Initialize
    static {
        SERVICE_PROVIDERS = new String[]{"UPS", "FedEx", "USPS"};
    }

    // Required date to be sent
    public static final long SEND_NOW = 0;
    public static final long SEND_AT_WEEKEND = 1;
    public static final long TIME_TO_DESTROY = 30 * DateUtils.MILLIS_PER_DAY; // Letter will be destroyed after 30days
}
