package com.gem.mailbox.constant;

/**
 * Queries definition
 * Created by neo on 2/3/2016.
 */
public interface QueryConst {
    //    public static final String SEARCH_USER = "SELECT * FROM user_account WHERE " +
//            "LOWER(first_name) SIMILAR TO LOWER('(%s)%%') " +
//            "OR LOWER(last_name) SIMILAR TO LOWER('(%s)%%') " +
//            "OR LOWER(address) SIMILAR TO LOWER('(%s)%%');";
    public static final String SEARCH_USER = "SELECT * FROM user_account WHERE " +
            "(LOWER(first_name) SIMILAR TO LOWER('(%s)%%') " +
            "OR LOWER(last_name) SIMILAR TO LOWER('(%s)%%') " +
            "OR LOWER(address) SIMILAR TO LOWER('(%s)%%')) "
            + "AND NOT is_admin;";

    public static final String GET_SCAN_LETTER_GROUP_BY_USER =
            "SELECT u.*, " +
                    "COUNT(l.uuid) AS letter_count " +
                    "FROM user_account AS u LEFT JOIN letter AS l ON u.uuid = l.user_id  " +
                    "WHERE l.is_scan = false GROUP BY u.uuid;";

    public static final String GET_IMMEDIATE_ORDERS =
            "SELECT o.* FROM " +
                    "send_order AS o " +
                    "LEFT JOIN user_account AS u ON o.user_id = u.uuid " +
                    "WHERE require_date = 0 AND NOT o.is_sent " +
                    "ORDER BY first_name, user_id;";

    public static final String GET_FUTURE_ORDERS =
            "SELECT o.* FROM " +
                    "send_order AS o " +
                    "LEFT JOIN user_account AS u ON o.user_id = u.uuid " +
                    "WHERE require_date > 0 AND NOT o.is_sent " +
                    "ORDER BY require_date, first_name, user_id;";
}
