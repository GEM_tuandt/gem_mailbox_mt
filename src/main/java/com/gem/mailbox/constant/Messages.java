package com.gem.mailbox.constant;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 2/24/15
 * Time: 4:52 PM
 */
public enum Messages {
    TOKEN_IS_EXPIRED {
        @Override
        public String toString() {
            return "Token is expired";
        }
    }
    ,
    TOKEN_IS_INVALID_OR_NOT_AVAILABLE {
        @Override
        public String toString() {
            return "Token is invalid or not avaiable";
        }
    },
    USER_ACCOUNT_IS_EXISTED {
        @Override
        public String toString() {
            return "Email is existed";
        }
    },
    USER_NOT_FOUND {
        @Override
        public String toString() {
            return "User not found";
        }
    },
    USER_NOT_LOGIN {
        @Override
        public String toString() {
            return "User not login";
        }
    },
    WRONG_PASSWORD {
        @Override
        public String toString() {
            return "Password is incorrect";
        }
    },
    WRONG_OLD_PASSWORD {
        @Override
        public String toString() {
            return "Old password is incorrect";
        }
    },
    IMAGE_UPLOAD_ERROR {
        @Override
        public String toString() {
            return "Upload image error";
        }
    },
    PDF_UPLOAD_ERROR {
        @Override
        public String toString() {
            return "Upload pdf error";
        }
    },
    DATE_IS_REQUIRED {
        @Override
        public String toString() {
            return "Date is required";
        }
    },
    LETTER_IS_ORDERED {
        @Override
        public String toString() {
            return "Letter is ordered to send";
        }
    },
    FAIL {
        @Override
        public String toString() {
            return "Failed";
        }
    },
    ERROR_MAPPING {
        @Override
        public String toString() {
            return "Mapping object error";
        }
    },
    LETTER_NOT_FOUND {
        @Override
        public String toString() {
            return "Letter not found";
        }
    },
    SERVICE_PROVIDER_NOT_FOUND {
        @Override
        public String toString() {
            return "Service provider not found";
        }
    },
    ORDER_NOT_FOUND {
        @Override
        public String toString() {
            return "Order not found";
        }
    },
    ORDER_NOT_SENT {
        @Override
        public String toString() {
            return "Order is not sent";
        }
    },

    USER_NOT_OWN_LETTER {
        @Override
        public String toString() {
            return "User does not own this letter";
        }
    },
    LETTER_IS_NOT_SCANNED{
        @Override
        public String toString() {
            return "Letter is not scanned";
        }
    },

}
