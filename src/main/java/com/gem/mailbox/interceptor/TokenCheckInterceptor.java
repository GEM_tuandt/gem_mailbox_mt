package com.gem.mailbox.interceptor;

import com.gem.mailbox.constant.Messages;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.model.Session;
import com.gem.mailbox.repository.SessionRepository;
import org.apache.commons.collections.map.LRUMap;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

public class TokenCheckInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory.getLogger(TokenCheckInterceptor.class);
    private static final Long REFRESH_THRESHOLD = 6 * DateUtils.MILLIS_PER_HOUR; // refresh token when 6 hours before expired
    public static final Long TOKEN_EXPIRED_IN = Session.DATE_EXPIRE * DateUtils.MILLIS_PER_DAY;
//    private LRUMap cachedToken = new LRUMap(512); // Default max size = 100

    @Autowired
    private SessionRepository sessionRepository;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws
            Exception {
        String requestURI = request.getRequestURI();
        request.setCharacterEncoding("utf-8");
        if (requestURI.equals("/")
                || requestURI.contains("/login") || requestURI.contains("/hello") || requestURI.contains("/error")
                || requestURI.contains("/register") || requestURI.contains("/forgotPassword")
                || requestURI.contains("/changePassword") || requestURI.contains("/confirm")
                || requestURI.contains("/configuration")
                || requestURI.contains("/getWholeProfile")
                || requestURI.contains("/migrate")
                || requestURI.contains("document")
                || requestURI.contains("/signup"))
        {
            return true; // pass-through login
        }
        String token = request.getHeader("token");

//        if (token != null && cachedToken.containsKey(token)) {
//            Long expiredTime = (Long) cachedToken.get(token);
//            if (expiredTime < System.currentTimeMillis()) {
//                throw new AuthenticationException(Messages.TOKEN_IS_EXPIRED.toString());
//            }
//            refreshToken(token, expiredTime);
//            return true; // Continue
//        } else
        if (token != null) {
            Session session = sessionRepository.findByToken(token);
            if (session != null) {
//                cachedToken.put(session.getToken(), session.getTokenExpireOn().getTime());
                Long expiredTime = session.getTokenExpireOn().getTime();
                if (expiredTime < System.currentTimeMillis()) {
                    throw new AuthenticationException(Messages.TOKEN_IS_EXPIRED.toString());
                }
                refreshToken(session);
                return true;
            } else {
                throw new AuthenticationException(Messages.TOKEN_IS_INVALID_OR_NOT_AVAILABLE.toString());
            }
        } else {
            throw new AuthenticationException(Messages.TOKEN_IS_INVALID_OR_NOT_AVAILABLE.toString());
        }
    }

    private void refreshToken(String token, Long expiredTime) {
        Long now = System.currentTimeMillis();

        if ((expiredTime - now) < REFRESH_THRESHOLD) {
            Session session = sessionRepository.findByToken(token);
            if (session != null) {
                refreshToken(session);
            }
        }
    }

    private void refreshToken(Session session) {
        Long now = System.currentTimeMillis();
        if ((session.getTokenExpireOn().getTime() - now) < REFRESH_THRESHOLD) {
            session.setDateUpdated(new Date());
            session.setTokenExpireOn(new Date(now + TOKEN_EXPIRED_IN));
            sessionRepository.save(session);
//            cachedToken.put(session.getToken(), session.getTokenExpireOn().getTime());
        }
    }
}