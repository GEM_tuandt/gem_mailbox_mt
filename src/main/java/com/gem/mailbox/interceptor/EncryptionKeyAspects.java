package com.gem.mailbox.interceptor;

import com.gem.mailbox.service.UserService;
import org.apache.commons.collections.map.LRUMap;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 3/12/15
 * Time: 10:15 AM
 */
@Aspect
@Component
public class EncryptionKeyAspects {
    @Autowired
    ApplicationContext applicationContext;
    @Autowired
    UserService userService;

    private LRUMap cachedEncryptionKey = new LRUMap(1024);

    private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(EncryptionKeyAspects.class);

    @Pointcut("execution(* com.gem.mailbox.service.*.sync*(..))")
    public void syncServicePointcut() {

    }

    @Pointcut("execution(* com.gem.mailbox.service.*.add*(..))")
    public void addServicePointcut() {

    }

    @Pointcut("execution(* com.gem.mailbox.service.*.delete*(..))")
    public void deleteServicePointcut() {

    }

    @Pointcut("execution(* com.gem.mailbox.service.*.update*(..))")
    public void updateServicePointcut() {

    }

    @Around("syncServicePointcut() || addServicePointcut() || deleteServicePointcut() || updateServicePointcut()")
    public Object syncUpInterceptor(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
    //disable key from amazon
/*
        String token = (String) proceedingJoinPoint.getArgs()[0];
        String encryptionKey="UTm99wbOcGU96QQBGlVm1XbiUkv3FHGrtLD3cHNq5GI=";
        if (cachedEncryptionKey.get(token) != null) {
            encryptionKey = (String) cachedEncryptionKey.get(token);
        } else {
            User user = userService.getUserFromToken(token);
            encryptionKey = Base64.encodeAsString(keyManagementService.getSecureKey(user.getEncryptionCode()));
            cachedEncryptionKey.put(token, encryptionKey);
        }
        encryptionKey="UTm99wbOcGU96QQBGlVm1XbiUkv3FHGrtLD3cHNq5GI=";
        HibernatePBEEncryptorRegistry registry =
                HibernatePBEEncryptorRegistry.getInstance();
        if (registry.getPBEStringEncryptor(ConfigConstants.DEFAULT_DB_ENCRYPTOR_NAME) == null) {
            StandardPBEStringEncryptor standardPBEStringEncryptor = new StandardPBEStringEncryptor();
            standardPBEStringEncryptor.setPassword(encryptionKey);
            registry.registerPBEStringEncryptor(ConfigConstants.DEFAULT_DB_ENCRYPTOR_NAME, standardPBEStringEncryptor);
        }
        LOGGER.info("User token " + token + " obtain encryption key " + encryptionKey);
*/
        return proceedingJoinPoint.proceed();
    }
}
