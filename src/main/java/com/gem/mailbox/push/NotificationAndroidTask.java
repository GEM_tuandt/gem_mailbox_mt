package com.gem.mailbox.push;

import com.gem.mailbox.model.constant.JobAction;
import com.gem.mailbox.model.constant.JobTarget;
import org.apache.http.HttpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.UUID;

/**
 * Android Notification Task
 * Created by neo on 3/3/2016.
 */
public class NotificationAndroidTask implements Runnable {
    public String gcmServerApiKey;
    public String gcmServerUrl;
    RestTemplate restTemplate;

    private PushBody body;

    public NotificationAndroidTask(RestTemplate restTemplate, String gcmServerApiKey, String gcmServerUrl, PushBody body) {
        this.body = body;
        this.gcmServerApiKey = gcmServerApiKey;
        this.gcmServerUrl = gcmServerUrl;
        this.restTemplate = restTemplate;
    }

    @Override
    public void run() {
        try {
            sendGcmNotification(body);
        } catch (HttpException | IOException e) {
            e.printStackTrace();
        }
    }

    private Boolean sendGcmNotification(PushBody body) throws HttpException, IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "key=" + gcmServerApiKey);
        headers.add("Content-Type", "application/json");


        HttpEntity<PushBody> request = new HttpEntity<>(body, headers);


        PushGcmResponse response = restTemplate.postForObject(gcmServerUrl, request, PushGcmResponse.class);

        System.out.println("response " + response.getSuccess());

        return response.getSuccess() == 1;
    }
}
