package com.gem.mailbox.push;

import com.gem.mailbox.model.constant.JobAction;
import com.gem.mailbox.model.constant.JobTarget;
import com.gem.mailbox.model.constant.UserType;

import java.util.UUID;

/**
 * Android Body of request push notification
 * Created by neo on 3/1/2016.
 */
public class PushBody {
    private String to;
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public PushBody(Data data, String to) {
        this.data = data;
        this.to = to;
    }

    /**
     * Data format is sent to devices
     */
    public static class Data {
        private JobAction jobAction;
        private JobTarget jobTarget;
        private  UUID targetId;
        private UserType userType;

        // Constructor
        public Data(JobAction jobAction, JobTarget jobTarget, UUID targetId, UserType userType) {
            this.jobAction = jobAction;
            this.jobTarget = jobTarget;
            this.targetId = targetId;
            this.userType = userType;
        }

        // GETTER & SETTER
        public JobAction getJobAction() {
            return jobAction;
        }

        public void setJobAction(JobAction jobAction) {
            this.jobAction = jobAction;
        }

        public JobTarget getJobTarget() {
            return jobTarget;
        }

        public void setJobTarget(JobTarget jobTarget) {
            this.jobTarget = jobTarget;
        }

        public UUID getTargetId() {
            return targetId;
        }

        public void setTargetId(UUID targetId) {
            this.targetId = targetId;
        }

        public UserType getUserType() {
            return userType;
        }

        public void setUserType(UserType userType) {
            this.userType = userType;
        }
    }
}
