package com.gem.mailbox;

import com.gem.mailbox.config.*;
import com.gem.mailbox.utils.JasyptSecureUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

@Configuration
@ComponentScan
@Import({EMRepositoryRestMvcConfiguration.class, EMWebConfiguration.class,
        MailConfiguration.class, AmazonConfiguration.class, AppDozerConfig.class, NotificationConfig.class})
@EnableAutoConfiguration
@EnableTransactionManagement
@EnableScheduling
@EnableAsync
public class Application {
    private static final Logger appLogger = (Logger) LoggerFactory.getLogger(Application.class);
    private static final String SPRING_DATA_USERNAME = "spring.datasource.username";
    private static final String SPRING_DATA_PASSWORD = "spring.datasource.password";

    @Value("${spring.datasource.driverClassName}")
    private String driver;
    @Value("${spring.datasource.url}")
    private String url;

    public static void main(String[] args) throws Exception {
        InputStream inputStream = Application.class.getClassLoader().getResourceAsStream("application.properties");
        Properties properties = new Properties();

        // Bootstrap license server connection
        try {
            properties.load(inputStream);
            appLogger.info("-------- Start bootstrap license server ------------");

            Class.forName(properties.getProperty("spring.datasource.driverClassName"));
            Connection connection = DriverManager.getConnection(
                    properties.getProperty("spring.datasource.url"),
                    JasyptSecureUtils.getSecuredValue(SPRING_DATA_USERNAME),
                    JasyptSecureUtils.getSecuredValue(SPRING_DATA_PASSWORD));

            if (connection != null) {
                appLogger.info("Bootstrap license server ok!");
            } else {
                appLogger.info("Bootstrap license server failed!");
            }
        } catch (Exception e) {
            appLogger.error("Unable to load config. Terminated. Cause: " + e.getMessage());
            throw e;
        }

        SpringApplication.run(Application.class, args);
    }

    @Bean
    public DataSource datasource() throws IOException {
        org.apache.tomcat.jdbc.pool.DataSource ds = new org.apache.tomcat.jdbc.pool.DataSource();
        ds.setDriverClassName(driver);
        ds.setUrl(url);
        ds.setUsername(JasyptSecureUtils.getSecuredValue(SPRING_DATA_USERNAME));
        ds.setPassword(JasyptSecureUtils.getSecuredValue(SPRING_DATA_PASSWORD));

        return ds;
    }

//    @Scheduled(fixedRate = 2000)
//    public void cronJob() {
//        System.out.println("Start cron job");
//    }
}
