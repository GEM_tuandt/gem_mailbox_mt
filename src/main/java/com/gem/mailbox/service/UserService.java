package com.gem.mailbox.service;

import com.gem.mailbox.dto.ResponseDTO;
import com.gem.mailbox.dto.UserDTO;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.model.User;
import com.gem.mailbox.model.constant.DeviceType;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 2/24/15
 * Time: 4:22 PM
 */
public interface UserService extends BaseService {
    UserDTO signUp(UserDTO userDTO) throws ServiceException;

    UserDTO login(UserDTO userDTO, String deviceToken) throws ServiceException;

    String logout(String token) throws ServiceException, AuthenticationException;

    UserDTO changePassword(String token, String oldPassword, String newPassword, String deviceToken) throws ServiceException, AuthenticationException;

    Boolean newPassword(String token, String newPassword) throws ServiceException, AuthenticationException;

//    UserDTO edit(String token, String firstName, String lastName, String phoneNumber, String address, MultipartFile avatar) throws ServiceException, AuthenticationException;
    UUID edit(String token, UserDTO userDTO, MultipartFile avatar) throws ServiceException, AuthenticationException;

    UserDTO getInfo(String token, String userId) throws ServiceException, AuthenticationException;

    List<UserDTO> searchUser(String keyword) throws ServiceException;

    Boolean updateDeviceToken(String token, DeviceType deviceType, String deviceToken) throws ServiceException, AuthenticationException;
}
