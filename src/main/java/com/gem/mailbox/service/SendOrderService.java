package com.gem.mailbox.service;

import com.gem.mailbox.dto.AdminSendOrdersDTO;
import com.gem.mailbox.dto.SendOrderDTO;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;

import java.util.List;
import java.util.UUID;

/**
 * Send order services
 * Created by neo on 2/2/2016.
 */
public interface SendOrderService extends BaseService {
    Boolean customerRequestSend(String token, String sendAddress, String serviceProviderId,
                                Long requireDate, String note, String letterIds) throws ServiceException;

    AdminSendOrdersDTO adminGetSendRequestList() throws ServiceException;

    Boolean adminConfirmSentOrder(String token, UUID orderId, String trackingCode, float price) throws ServiceException, AuthenticationException;

    /**
     * Check and delete all orders that contain no letter
     */
    void checkOrderEmpty() throws ServiceException;

    Boolean customerConfirmOrderArrived(String sendOrderId) throws ServiceException;

    SendOrderDTO orderDetail(String sendOrderId) throws ServiceException;
}
