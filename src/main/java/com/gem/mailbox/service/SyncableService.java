package com.gem.mailbox.service;

import com.gem.mailbox.dto.BaseDTO;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 2/28/15
 * Time: 2:41 PM
 */
public interface SyncableService<D extends BaseDTO> {
    public String add(String token, D dto) throws ServiceException, AuthenticationException;

    public String update(String token, D dto) throws ServiceException, AuthenticationException;

    public Class<D> getDTOClass();
}
