package com.gem.mailbox.service;

import com.gem.mailbox.dto.*;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.model.SendOrder;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

/**
 * Letter services
 * Created by neo on 1/29/2016.
 */
public interface LetterService extends BaseService {
    LetterDTO addNewLetter(String token, String userId, Long date, String qrCode, String note, MultipartFile letterThumb) throws ServiceException, AuthenticationException;

    AdminSummaryDTO adminGetLetterSummary(String token) throws ServiceException, AuthenticationException;

    LetterDTO getLetterDetail(UUID letterId) throws ServiceException;

    SendOrderDTO getOrderFromLetter(UUID letterId) throws ServiceException;

    Boolean customerRequestScan(String letterIds) throws ServiceException;

    Boolean customerRequestTrash(String letterIds) throws ServiceException;

    List<UserDTO> adminGetScanList(String excludeLetters) throws ServiceException;

    List<LetterDTO> adminGetSendList() throws ServiceException;

    List<UserDTO> adminGetTrashList() throws ServiceException;

    CustomerLettersDTO customerGetLetterList(String token, String userId) throws ServiceException, AuthenticationException;

    Boolean adminConfirmTrashed(String token, UUID letterId) throws ServiceException, AuthenticationException;

    Boolean adminScanLetter(String token, UUID letterId, MultipartFile pdfFile) throws ServiceException, AuthenticationException;

    Boolean validateQrcode(String qrCode) throws ServiceException;

    LetterDTO findLetterByQrCode(String qrCode) throws ServiceException;

    Boolean customerRestoreFromTrash(String letterIds) throws ServiceException;

    Boolean customerEditLetter(String token, String letterId,
                               String title,
                               String userNote,
                               String letterFrom,
                               String letterTo) throws ServiceException, AuthenticationException;

    Boolean customerRequestDestroy(String token, String letterId) throws ServiceException, AuthenticationException;

    Boolean customerSaveToDocument(String token, String uuids) throws ServiceException, AuthenticationException;

    Boolean customerRemoveFromDocument(String uuids) throws ServiceException;
}
