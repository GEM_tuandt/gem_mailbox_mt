package com.gem.mailbox.service.loader;

import com.gem.mailbox.dto.LetterDTO;
import com.gem.mailbox.dto.SendOrderDTO;
import com.gem.mailbox.dto.ServiceProviderDTO;
import com.gem.mailbox.dto.UserDTO;
import com.gem.mailbox.mapping.MapUtils;
import com.gem.mailbox.model.Letter;
import com.gem.mailbox.model.SendOrder;
import com.gem.mailbox.model.User;
import com.gem.mailbox.repository.LetterRepository;
import com.gem.mailbox.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Mapping between SendOrder and SendOrderDTO
 * Created by neo on 2/26/2016.
 */
@Service
@Transactional
public class SendOrderLoader {
    @Autowired
    LetterRepository letterRepository;
    @Autowired
    UserRepository userRepository;

    public SendOrderDTO toDTO(SendOrder sendOrder) {
        // Find letter entities
        List<Letter> letters = letterRepository.findBySendOrder(sendOrder);

        // Map letters entities to DTOs
        List<LetterDTO> letterDTOs = MapUtils.mapList(letters, LetterDTO.class);

        // Map SendOrder entity to DTO
        SendOrderDTO sendOrderDTO = MapUtils.map(sendOrder, SendOrderDTO.class);

        // Set letter list to DTO
        sendOrderDTO.setLetterDTOs(letterDTOs);

        // Add ordered user
        User user = userRepository.findOne(sendOrder.getUser().getUuid());
        sendOrderDTO.setUserDTO(MapUtils.map(user, UserDTO.class));

        // Add Service provider
        sendOrderDTO.setServiceProviderDTO(MapUtils.map(sendOrder.getServiceProvider(), ServiceProviderDTO.class));

        return sendOrderDTO;
    }
}
