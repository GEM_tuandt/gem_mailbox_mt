package com.gem.mailbox.service;

import com.gem.mailbox.dto.LetterScanDTO;
import com.gem.mailbox.dto.ResponseDTO;
import com.gem.mailbox.model.constant.ImageType;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.model.LetterScan;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 2/26/15
 * Time: 3:07 PM
 */
public interface DocumentService extends BaseService, SyncableService<LetterScanDTO> {
    String uploadLetterThumb(UUID letterId, MultipartFile letterThumb)throws ServiceException ;

    String uploadUserAvatar(String userId, MultipartFile file) throws ServiceException, AuthenticationException;

    LetterScan uploadImage(String token, MultipartFile file, UUID entityId, UUID documentId, ImageType imageType) throws ServiceException, AuthenticationException;

    LetterScanDTO download(String documentId, boolean isThumb) throws ServiceException;

    LetterScan deleteImage(String token, UUID uuid) throws ServiceException;

    //Async task need be call from documentService instance
    void deleteRemoteFile(String objectKey);

    ResponseDTO getAllImageMetadata(String token, List<LetterScanDTO> letterScanDTOList) throws ServiceException;

    LetterScanDTO uploadDocument(String token, MultipartFile file, UUID entityId, UUID documentId) throws ServiceException;

    String getDocumentURL(LetterScan letterScan, Boolean isThumb);

    byte[] getDocumentByFilePath(String filePath) throws IOException;

    String uploadScanFile(UUID letterId, MultipartFile scanFile) throws ServiceException;
}
