package com.gem.mailbox.service;

import com.gem.mailbox.dto.JobHistoryDTO;
import com.gem.mailbox.dto.ServiceProviderDTO;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;

import java.util.List;

/**
 * Job history Services
 * Created by neo on 2/2/2016.
 */
public interface JobHistoryService extends BaseService {
    List<JobHistoryDTO> getJobHistory(String token, Integer pageIndex, Integer pageSize) throws ServiceException, AuthenticationException;
}
