package com.gem.mailbox.service;

import com.amazonaws.services.elastictranscoder.model.Job;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.model.User;
import com.gem.mailbox.model.constant.DeviceType;
import com.gem.mailbox.model.constant.JobAction;
import com.gem.mailbox.model.constant.JobTarget;

import java.util.List;
import java.util.UUID;

/**
 * Service that push notification to mobile devices
 * Created by neo on 3/1/2016.
 */
public interface PushNotificationService extends BaseService {
    Boolean pushToUser(User user, JobAction jobAction, JobTarget jobTarget, UUID targetId) throws ServiceException;
    Boolean pushToAdmin(JobAction jobAction, JobTarget jobTarget, UUID targetId) throws ServiceException;
}
