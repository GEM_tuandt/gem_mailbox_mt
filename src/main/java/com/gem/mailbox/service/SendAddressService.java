package com.gem.mailbox.service;

import com.gem.mailbox.dto.SendAddressDTO;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.model.SendAddress;
import com.gem.mailbox.model.User;

import java.util.List;

/**
 * Service Provider Services
 * Created by neo on 2/2/2016.
 */
public interface SendAddressService extends BaseService {
    List<SendAddressDTO> findSendAddress(String token, String keyword) throws ServiceException, AuthenticationException;
    SendAddress findByAddressAndUser(User user, String address) throws ServiceException;
}
