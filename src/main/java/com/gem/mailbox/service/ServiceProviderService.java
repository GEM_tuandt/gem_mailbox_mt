package com.gem.mailbox.service;

import com.gem.mailbox.dto.ServiceProviderDTO;
import com.gem.mailbox.exception.ServiceException;

import java.util.List;

/**
 * Service Provider Services
 * Created by neo on 2/2/2016.
 */
public interface ServiceProviderService extends BaseService {
    List<ServiceProviderDTO> getServiceProviders() throws ServiceException;
}
