package com.gem.mailbox.service.impl;

import com.gem.mailbox.repository.OtherNameRepository;
import com.gem.mailbox.repository.UserEmailRepository;
import com.gem.mailbox.service.OtherNameService;
import com.gem.mailbox.service.UserEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implement for service provider services
 * Created by neo on 2/2/2016.
 */
@Transactional
@Service
public class UserEmailServiceImpl extends BaseServiceImpl implements UserEmailService {
    @Autowired
    UserEmailRepository userEmailRepository;
}
