package com.gem.mailbox.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gem.mailbox.constant.Messages;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.model.Notification;
import com.gem.mailbox.model.Session;
import com.gem.mailbox.model.User;
import com.gem.mailbox.model.constant.DeviceType;
import com.gem.mailbox.model.constant.JobAction;
import com.gem.mailbox.model.constant.JobTarget;
import com.gem.mailbox.model.constant.UserType;
import com.gem.mailbox.push.NotificationAndroidTask;
import com.gem.mailbox.push.PushBody;
import com.gem.mailbox.push.PushGcmResponse;
import com.gem.mailbox.repository.UserRepository;
import com.gem.mailbox.service.NotificationService;
import com.gem.mailbox.service.PushNotificationService;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import org.apache.http.HttpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.*;

/**
 * Implement for push notification service
 * Created by neo on 3/1/2016.
 */
@Transactional
@Service
public class PushNotificationServiceImpl extends BaseServiceImpl implements PushNotificationService {
    @Value("${gcm.server.key}")
    public String gcmServerApiKey;
    @Value("${gcm.server.url}")
    public String gcmServerUrl;
    @Autowired
    UserRepository userRepository;
    @Autowired
    RestTemplate restTemplate;
    @Autowired
    private ApnsService apnsService;
    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;
    @Autowired
    NotificationService notificationService;

    @Override
    public Boolean pushToUser(User user, JobAction jobAction, JobTarget jobTarget, UUID targetId) throws ServiceException {
        // Push to all sessions of user
        List<Session> sessions = user.getSessions();
        for (Session session : sessions) {
            try {
                pushToDevice(session.getDeviceType(), session.getDeviceToken(),
                        jobAction, jobTarget, targetId, UserType.USER);

                notificationService.add(user, jobAction, jobTarget, targetId);

            } catch (JsonProcessingException e) {
                e.printStackTrace();
                throw new ServiceException(Messages.ERROR_MAPPING);
            }
        }
        return true;
    }

    @Override
    public Boolean pushToAdmin(JobAction jobAction, JobTarget jobTarget, UUID targetId) throws ServiceException {
        // Admins
        List<User> users = userRepository.findByIsAdminTrue();

        List<Session> sessions = new ArrayList<>();
        // Push to all sessions of Admin
        for (User user : users) {
            sessions.addAll(user.getSessions());
        }

        for (Session session : sessions) {
            try {
                pushToDevice(session.getDeviceType(), session.getDeviceToken(),
                        jobAction, jobTarget, targetId, UserType.ADMIN);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                throw new ServiceException(Messages.ERROR_MAPPING);
            }
        }
        return true;
    }

    /**
     * Push to specific devices
     */
    private Boolean pushToDevice(DeviceType deviceType, String deviceToken, JobAction jobAction, JobTarget jobTarget, UUID targetId, UserType userType) throws ServiceException, JsonProcessingException {
        if (deviceType == null) {
            return false;
        }

        switch (deviceType) {
            case ANDROID:
                return pushToAndroid(deviceToken, jobAction, jobTarget, targetId, userType);
            case IOS:
                return pushToIos(deviceToken, jobAction, jobTarget, targetId, userType);
            default:
                break;
        }
        return false;
    }

    /**
     * Push notification to android device
     */
    public Boolean pushToAndroid(String deviceToken, JobAction jobAction, JobTarget jobTarget, UUID targetId, UserType userType) throws ServiceException {
        taskExecutor.execute(new NotificationAndroidTask(restTemplate, gcmServerApiKey, gcmServerUrl,
                new PushBody(new PushBody.Data(jobAction, jobTarget, targetId, userType), deviceToken)));
        return true;
    }

    /**
     * Push to iOS device
     */
    private Boolean pushToIos(String deviceToken, JobAction jobAction,
                              JobTarget jobTarget, UUID targetId, UserType userType) throws JsonProcessingException {
        // Title of notification
        String body = getIosNotificationBody(jobAction, userType);

        String payload = APNS.newPayload()
                .alertBody(body)
                .sound("default")
                .customField("jobAction", jobAction.ordinal())
                .customField("jobTarget", jobTarget.ordinal())
                .customField("targetId", targetId.toString())
                .customField("userType", userType.ordinal())
//                .badge(numberNotification)
                .build();
        apnsService.push(deviceToken, payload);
        return true;
    }

    private String getIosNotificationBody(JobAction jobAction, UserType userType) {
        switch (userType) {
            case ADMIN:
                switch (jobAction) {
                    case SCAN:
                        return "You have a new Scan request";
                    case SEND:
                        return "You have a new Send request";
                    case DESTROY:
                        return "You have a new Destroy request";
                    default:
                        return "You have a new notification";
                }
            case USER:
                switch (jobAction) {
                    case ADD:
                        return "You have a new letter";
                    case SCAN:
                        return "You have a new letter is scanned";
                    case SEND:
                        return "Your letters is sent";
                    case DESTROY:
                        return "Your letter is destroyed";
                    default:
                        return "You have a new notification";
                }
            default:
                return "You have a new notification";
        }
    }
}
