package com.gem.mailbox.service.impl;

import com.gem.mailbox.dto.SendAddressDTO;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.mapping.MapUtils;
import com.gem.mailbox.model.SendAddress;
import com.gem.mailbox.model.User;
import com.gem.mailbox.repository.SendAddressRepository;
import com.gem.mailbox.service.SendAddressService;
import com.gem.mailbox.utils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implement for send address services
 * Created by neo on 2/19/2016.
 */
@Transactional
@Service
public class SendAddressServiceImpl extends BaseServiceImpl implements SendAddressService{
    @Autowired
    SendAddressRepository sendAddressRepository;

    @Override
    public List<SendAddressDTO> findSendAddress(String token, String keyword) throws ServiceException, AuthenticationException {
        User user = getUserFromToken(token);

        String keyCombine = StringUtil.keywordToQueryParam(keyword.trim());
        List<SendAddress> sendAddresses = nativeQuery("find_send_address", SendAddress.class, user.getUuid(), keyCombine);
        return MapUtils.mapList(sendAddresses, SendAddressDTO.class);
    }

    @Override
    public SendAddress findByAddressAndUser(User user, String address) throws ServiceException {
        return sendAddressRepository.findByAddressAndUser(address, user);
    }
}
