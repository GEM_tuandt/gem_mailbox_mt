package com.gem.mailbox.service.impl;

import com.gem.mailbox.constant.Messages;
import com.gem.mailbox.dto.JobHistoryDTO;
import com.gem.mailbox.dto.UserDTO;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.mapping.MapUtils;
import com.gem.mailbox.model.JobHistory;
import com.gem.mailbox.model.Letter;
import com.gem.mailbox.model.SendOrder;
import com.gem.mailbox.model.User;
import com.gem.mailbox.model.constant.JobTarget;
import com.gem.mailbox.repository.JobHistoryRepository;
import com.gem.mailbox.repository.LetterRepository;
import com.gem.mailbox.repository.SendOrderRepository;
import com.gem.mailbox.service.JobHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Implement for Job history services
 * Created by neo on 2/2/2016.
 */
@Transactional
@Service
public class JobHistoryServiceImpl extends BaseServiceImpl implements JobHistoryService {
    @Autowired
    JobHistoryRepository jobHistoryRepository;
    @Autowired
    LetterRepository letterRepository;
    @Autowired
    SendOrderRepository sendOrderRepository;

    @Override
    public List<JobHistoryDTO> getJobHistory(String token, Integer pageIndex, Integer pageSize) throws ServiceException, AuthenticationException {
        User user = getUserFromToken(token);
        if (!user.getIsAdmin()) {
            throw new ServiceException(Messages.USER_NOT_FOUND);
        }

        // Paging
        Pageable pageable = new PageRequest(pageIndex, pageSize, new Sort(Sort.Direction.DESC, "actionDate"));

        // Mapping
        List<JobHistory> jobHistories = jobHistoryRepository.findByUser(user, pageable);
        List<JobHistoryDTO> jobHistoryDTOs = new ArrayList<>();

        for (JobHistory jobHistory : jobHistories) {
            JobHistoryDTO jobHistoryDTO = MapUtils.map(jobHistory, JobHistoryDTO.class);

            if (jobHistory.getTargetType() == JobTarget.LETTER) {
                Letter letter = letterRepository.findOne(jobHistory.getTargetId());

                // Set target user
                if (letter != null && letter.getUser() != null) {
                    jobHistoryDTO.setUserDTO(MapUtils.map(letter.getUser(), UserDTO.class));
                }
            } else if (jobHistory.getTargetType() == JobTarget.ORDER){
                SendOrder sendOrder = sendOrderRepository.findOne(jobHistory.getTargetId());

                // Set target user
                if (sendOrder != null && sendOrder.getUser() != null) {
                    jobHistoryDTO.setUserDTO(MapUtils.map(sendOrder.getUser(), UserDTO.class));
                }
            }

            jobHistoryDTOs.add(jobHistoryDTO);
        }

        return jobHistoryDTOs;
    }
}
