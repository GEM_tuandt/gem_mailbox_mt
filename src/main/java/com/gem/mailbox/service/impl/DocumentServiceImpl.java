package com.gem.mailbox.service.impl;

import com.gem.mailbox.constant.Constants;
import com.gem.mailbox.constant.Messages;
import com.gem.mailbox.dto.LetterScanDTO;
import com.gem.mailbox.dto.ResponseDTO;
import com.gem.mailbox.model.constant.ImageType;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.model.LetterScan;
import com.gem.mailbox.repository.LetterScanRepository;
import com.gem.mailbox.service.DocumentService;
import com.gem.mailbox.service.loader.DocumentLoader;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 2/26/15
 * Time: 3:07 PM
 */
@Transactional
@Service
public class DocumentServiceImpl extends BaseServiceImpl implements DocumentService {
    @Autowired
    LetterScanRepository letterScanRepository;

    @Value("${files.tempPath}")
    public String defaultTempPath;
    @Value("${file.documentPath}")
    public String rootFilePath;
    @Autowired
    DocumentLoader documentLoader;

    private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(DocumentServiceImpl.class);

    @Override
    public String uploadLetterThumb(UUID letterId, MultipartFile letterThumb) throws ServiceException {
        LOGGER.info("Start upload avatar");
        if (letterThumb == null || letterThumb.isEmpty()) {
            throw new ServiceException(Messages.IMAGE_UPLOAD_ERROR);
        }

        String fileName;
        try {
            // Get file extension
            String extension = FilenameUtils.getExtension(letterThumb.getOriginalFilename());
            fileName = letterId + "." + extension;

            // Thumbnail of image
            String thumbImageName = Constants.THUMBNAIL_PREFIX + fileName;
            String originalImageFilePath = generateTemporaryFilePath(defaultTempPath, fileName);
            String thumbImageFilePath = generateTemporaryFilePath(defaultTempPath, thumbImageName);

            IOUtils.write(letterThumb.getBytes(), new FileOutputStream(originalImageFilePath));
            LOGGER.info("Save original image to temp folder success");

            // Get thumbnail from temp file
            File originalImageFile = new File(originalImageFilePath);
            Thumbnails.of(originalImageFile)
                    .size(Constants.THUMBNAIL_PREFERRED_SIZE, Constants.THUMBNAIL_PREFERRED_SIZE)
                    .toFile(thumbImageFilePath);

            LOGGER.info("Save thumb image to temp folder success");

            return fileName;
        } catch (IOException e) {
            throw new ServiceException(Messages.IMAGE_UPLOAD_ERROR.toString());
        }
    }

    @Override
    public String uploadUserAvatar(String userId, MultipartFile file) throws ServiceException, AuthenticationException {
        LOGGER.info("Start upload avatar");
        if (file == null || file.isEmpty()) {
            throw new ServiceException(Messages.IMAGE_UPLOAD_ERROR.toString());
        }
        String fileName;
        try {
            String extension = FilenameUtils.getExtension(file.getOriginalFilename());
            fileName = userId + "." + extension;
            String thumbImageName = Constants.THUMBNAIL_PREFIX + fileName;
            String originalImageFilePath = generateTemporaryFilePath(defaultTempPath, fileName);
            String thumbImageFilePath = generateTemporaryFilePath(defaultTempPath, thumbImageName);

            IOUtils.write(file.getBytes(), new FileOutputStream(originalImageFilePath));
            LOGGER.info("Save original image to temp folder success");

            // Get thumbnail from temp file
            File originalImageFile = new File(originalImageFilePath);
            Thumbnails.of(originalImageFile)
                    .size(Constants.THUMBNAIL_PREFERRED_SIZE, Constants.THUMBNAIL_PREFERRED_SIZE)
                    .toFile(thumbImageFilePath);

            LOGGER.info("Save thumb image to temp folder success");

//            File thumbImageFile = new File(thumbImageFilePath);
//            amazonS3Client.putObject(generatePutObjectRequest(generateRemoteFilePath(originalImageName), originalImageFile));
//            amazonS3Client.putObject(generatePutObjectRequest(generateRemoteFilePath(thumbImageName), thumbImageFile));
//            LOGGER.info("Upload images to AWS success");

//            User user = getUserFromToken(token);
//            LetterScan letterScan = saveDocumentInfo(documentId, file.getOriginalFilename(), originalImageName, extension, user, entityId, imageType);
            return fileName;
        } catch (IOException e) {
            throw new ServiceException(Messages.IMAGE_UPLOAD_ERROR.toString());
        }
    }

    @Override
    public LetterScan uploadImage(String token, MultipartFile file, UUID entityId, UUID documentId, ImageType imageType) throws ServiceException {
//        LOGGER.info("Start upload image");
//        if (file.isEmpty()) {
//            throw new ServiceException(Messages.IMAGE_UPLOAD_ERROR.toString());
//        }
//        try {
//            String extension = FilenameUtils.getExtension(file.getOriginalFilename());
//            String originalImageName = UUID.randomUUID().toString() + "." + extension;
//            String thumbImageName = Constants.THUMBNAIL_PREFIX + originalImageName;
//            String pdfImageName = Constants.PDF + originalImageName;
//            String originalImageFilePath = generateTemporaryFilePath(defaultTempPath, originalImageName);
//            String thumbImageFilePath = generateTemporaryFilePath(defaultTempPath, thumbImageName);
//            String pdfImageFilePath  = generateTemporaryFilePath(defaultTempPath, pdfImageName);
//
//            IOUtils.write(file.getBytes(), new FileOutputStream(originalImageFilePath));
//            LOGGER.info("Save original image to temp folder success");
//
//            File originalImageFile = new File(originalImageFilePath);
//            Thumbnails.of(originalImageFile)
//                    .size(Constants.THUMBNAIL_PREFERRED_SIZE, Constants.THUMBNAIL_PREFERRED_SIZE)
//                    .toFile(thumbImageFilePath);
//            Thumbnails.of(originalImageFile)
//                    .size(Constants.PDF_PREFERRED_SIZE, Constants.PDF_PREFERRED_SIZE)
//                    .toFile(pdfImageFilePath);
//
//            LOGGER.info("Save thumb image to temp folder success");
//
////            File thumbImageFile = new File(thumbImageFilePath);
////            amazonS3Client.putObject(generatePutObjectRequest(generateRemoteFilePath(originalImageName), originalImageFile));
////            amazonS3Client.putObject(generatePutObjectRequest(generateRemoteFilePath(thumbImageName), thumbImageFile));
////            LOGGER.info("Upload images to AWS success");
//
//            User user = getUserFromToken(token);
//            LetterScan letterScan = saveDocumentInfo(documentId, file.getOriginalFilename(), originalImageName, extension, user, entityId, imageType);
//            return letterScan;
//        } catch (IOException e) {
//            throw new ServiceException(Messages.IMAGE_UPLOAD_ERROR.toString());
//        }
        return null;
    }

    private String generateFolderName(String fileName) {
        fileName = FilenameUtils.getBaseName(fileName);
        return fileName.substring(fileName.length() - 3);
    }

    private String generateRemoteFilePath(String fileName) {
        return generateFolderName(fileName) + File.separator + fileName;
    }

    private String generateTemporaryFilePath(String root, String fileName) {
        return root + File.separator + fileName;
    }

//    private PutObjectRequest generatePutObjectRequest(String fileId, File file) {
//        ObjectMetadata objectMetadata = new ObjectMetadata();
//        objectMetadata.setServerSideEncryption(ObjectMetadata.AES_256_SERVER_SIDE_ENCRYPTION);
//        PutObjectRequest putObjectRequest = new PutObjectRequest(defaultImageBucket, fileId, file).withMetadata(objectMetadata);
//        return putObjectRequest;
//    }

//    private LetterScan saveDocumentInfo(UUID documentId, String name, String key, String type, User user, UUID entityId, ImageType imageType) {
//        LetterScan letterScan = new LetterScan();
//        letterScan.setUuid(documentId);
//        letterScan.setName(name);
//        letterScan.setObjectKey(key);
//        letterScan.setDocumentType(DocumentType.valueOf(type.toUpperCase()));
//        letterScan.setUser(user);
//        letterScan.setEntityId(entityId);
//        letterScan.setImageType(imageType);
//        documentRepository.save(letterScan);
//        return letterScan;
//    }

    @Override
    public LetterScanDTO download(String documentId, boolean isThumb) throws ServiceException {
        LetterScan letterScan = letterScanRepository.findOne(UUID.fromString(documentId));
        if (letterScan == null) {
            throw new ServiceException("LetterScan not found!");
        }
        String downloadLink = getDocumentURL(letterScan, isThumb);

        return new LetterScanDTO(letterScan, downloadLink);
    }

    private List<LetterScanDTO> generateDTOList(List<LetterScan> letterScen) {
        List<LetterScanDTO> letterScanDTOs = new ArrayList<>();
        for (LetterScan letterScan : letterScen) {
            LetterScanDTO letterScanDTO = new LetterScanDTO(letterScan);
            letterScanDTOs.add(letterScanDTO);
        }
        return letterScanDTOs;
    }

    @Override
    public String add(String token, LetterScanDTO dto) throws ServiceException, AuthenticationException {
        // Not need add
        return null;
    }

    @Override
    public String update(String token, LetterScanDTO dto) throws ServiceException, AuthenticationException {
        // Not need update
        return null;
    }

    @Override
    public Class<LetterScanDTO> getDTOClass() {
        return LetterScanDTO.class;
    }

    @Override
    public LetterScan deleteImage(String token, UUID uuid) throws ServiceException {
//        User user = getUserFromToken(token);
//        LetterScan letterScan = documentRepository.findByUserAndUuid(user, uuid);
//        if (letterScan == null) {
//            throw new ServiceException(Messages.DOCUMENT_NOT_FOUND.toString());
//        }
//        documentRepository.save(letterScan);
//        return letterScan;
        return null;
    }

    @Async
    @Override
    public void deleteRemoteFile(String objectKey) {
//        String thumbKey = Constants.THUMBNAIL_PREFIX + objectKey;
//        try {
//            amazonS3Client.deleteObject(defaultImageBucket, generateRemoteFilePath(objectKey));
//            amazonS3Client.deleteObject(defaultImageBucket, generateRemoteFilePath(thumbKey));
//        } catch (Exception ex) {
//            LOGGER.info("Delete object error");
//        }
    }

    @Override
    public ResponseDTO getAllImageMetadata(String token, List<LetterScanDTO> letterScanDTOList) throws ServiceException {
//        if (letterScanDTOList == null || letterScanDTOList.isEmpty()) {
//            return new ResponseDTO<>(null);
//        }
//
//        List<UUID> uuidList = new ArrayList<>();
//        for (LetterScanDTO letterScanDTO : letterScanDTOList) {
//            if (letterScanDTO.getUuid() != null) {
//                uuidList.add(letterScanDTO.getUuid());
//            }
//        }
//
//        User user = getUserFromToken(token);
//        List<LetterScan> letterScanList = documentRepository.findByUserAndUuidIn(user, uuidList);
//        List<LetterScanDTO> result = new ArrayList<>();
//        for (LetterScan letterScan : letterScanList) {
//            String downloadLink = getDocumentURL(letterScan, true);
//            result.add(new LetterScanDTO(letterScan, downloadLink));
//        }
//        return new ResponseDTO(result);
        return new ResponseDTO();
    }

    @Override
    public LetterScanDTO uploadDocument(String token, MultipartFile file, UUID entityId, UUID documentId) throws ServiceException {
        LetterScan letterScan = uploadImage(token, file, entityId, documentId, ImageType.NORMAL_PHOTO);
        String downloadLink = getDocumentURL(letterScan, true);
        return new LetterScanDTO(letterScan, downloadLink);
    }

    @Override
    public String uploadScanFile(UUID letterId, MultipartFile scanFile) throws ServiceException{
        LOGGER.info("Start upload PDF");
        if (scanFile == null || scanFile.isEmpty()) {
            throw new ServiceException(Messages.PDF_UPLOAD_ERROR);
        }

        String fileName;
        try {
            // Get file extension
            String extension = FilenameUtils.getExtension(scanFile.getOriginalFilename());
            fileName = letterId + "." + extension;

            String originalImageFilePath = generateTemporaryFilePath(defaultTempPath, fileName);
            IOUtils.write(scanFile.getBytes(), new FileOutputStream(originalImageFilePath));
            LOGGER.info("Save original image to temp folder success");

            LOGGER.info("Save thumb image to temp folder success");

            return fileName;
        } catch (IOException e) {
            throw new ServiceException(Messages.PDF_UPLOAD_ERROR);
        }
    }

    @Override
    public String getDocumentURL(LetterScan letterScan, Boolean isThumb) {
        if (isThumb) {
            return rootFilePath + "/Thumb_" + letterScan.getName() + "/";
        } else {
            return rootFilePath + "/" + letterScan.getName() + "/";
        }
    }

    @Override
    public byte[] getDocumentByFilePath(String rootFilePath) throws IOException {
        File file = new File(defaultTempPath + "/" + rootFilePath);
        if (!file.exists()) {
            return new byte[0];
        }
        return FileUtils.readFileToByteArray(file);
    }
}
