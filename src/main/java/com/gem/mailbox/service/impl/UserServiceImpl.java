package com.gem.mailbox.service.impl;

import com.gem.mailbox.constant.Messages;
import com.gem.mailbox.dto.*;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.interceptor.TokenCheckInterceptor;
import com.gem.mailbox.mapping.MapUtils;
import com.gem.mailbox.model.*;
import com.gem.mailbox.model.constant.DeviceType;
import com.gem.mailbox.repository.*;
import com.gem.mailbox.service.DocumentService;
import com.gem.mailbox.service.SendAddressService;
import com.gem.mailbox.service.UserService;
import com.gem.mailbox.utils.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.dozer.MappingException;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 2/24/15
 * Time: 4:22 PM
 */
@Transactional
@Service
public class UserServiceImpl extends BaseServiceImpl implements UserService, ApplicationContextAware {
    @Autowired
    UserRepository userRepository;
    @Autowired
    SessionRepository sessionRepository;
    @Autowired
    DocumentService documentService;
    @Autowired
    SendAddressRepository sendAddressRepository;
    @Autowired
    UserPhoneNumberRepository userPhoneNumberRepository;
    @Autowired
    UserEmailRepository userEmailRepository;
    @Autowired
    OtherNameRepository otherNameRepository;

    private ApplicationContext applicationContext;

    @Override
    public UserDTO signUp(UserDTO userDTO) throws ServiceException {
        if (isExistingUser(userDTO.getEmail())) {
            throw new ServiceException(Messages.USER_ACCOUNT_IS_EXISTED);
        }

        UserDTO returnUser;

        try {
            User user = createUser(userDTO);
            returnUser = MapUtils.map(user, UserDTO.class);
        } catch (MappingException e) {
            throw new ServiceException(Messages.ERROR_MAPPING, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return returnUser;
    }

    private Session createUserSession(String deviceToken, User user) {
        Session session = new Session();
        session.setUser(user);
        session.setDeviceToken(deviceToken);

        Date date = new Date();
        date.setTime(date.getTime() + TokenCheckInterceptor.TOKEN_EXPIRED_IN);
        session.setTokenExpireOn(date);

        session.setToken(UUID.randomUUID().toString());
        sessionRepository.save(session);
        return session;
    }

    private String encryptPassword(String password) {
        return new StrongPasswordEncryptor().encryptPassword(password);
    }

    private boolean checkPassword(String password, String encryptedPassword) {
        return new StrongPasswordEncryptor().checkPassword(password, encryptedPassword);
    }

    private User createUser(UserDTO userDTO) throws MappingException {
        User user = MapUtils.map(userDTO, User.class);
        user.setPassword(encryptPassword(userDTO.getPassword()));
        userRepository.save(user);
        return user;
    }

    private boolean isExistingUser(String email) {
        return userRepository.findByEmail(email) != null;
    }

    @Override
    public UserDTO login(UserDTO userDTO, String deviceToken) throws ServiceException {
        User user = validateLogin(userDTO.getEmail(), userDTO.getPassword(), userDTO.getIsAdmin());
        Calendar calendar = Calendar.getInstance();
        Session session;

        // Validate Session
        List<Session> sessions = sessionRepository.findByUserAndDeviceToken(user, deviceToken);

        if (sessions != null && sessions.size() > 0) { // update current session
            session = sessions.get(0);
            session.setDateUpdated(calendar.getTime());

            // Refresh token
            calendar.add(Calendar.DATE, 30);
//            now.setTime(now.getTime() + TokenCheckInterceptor.TOKEN_EXPIRED_IN);
            session.setTokenExpireOn(calendar.getTime());

            sessionRepository.save(session);

        } else { // Create new session
            session = createUserSession(deviceToken, user);
        }

        UserDTO userReturn = MapUtils.map(user, UserDTO.class);

        userReturn.setSendAddressDTOs(MapUtils.mapList(user.getSendAddresses(), SendAddressDTO.class));
        userReturn.setUserEmailDTOs(MapUtils.mapList(user.getUserEmails(), UserEmailDTO.class));
        userReturn.setOtherNameDTOs(MapUtils.mapList(user.getOtherNames(), OtherNameDTO.class));
        userReturn.setUserPhoneNumberDTOs(MapUtils.mapList(user.getUserPhoneNumbers(), UserPhoneNumberDTO.class));

        userReturn.setToken(session.getToken());
        return userReturn;
    }

    private User validateLogin(String email, String password, Boolean isAdmin) throws ServiceException {
        User user = getUserFromEmail(email, isAdmin);

        if (!checkPassword(password, user.getPassword())) {
            throw new ServiceException(Messages.WRONG_PASSWORD);
        }
        return user;
    }

    @Override
    public String logout(String token) throws ServiceException, AuthenticationException {
        Session session = sessionRepository.findByToken(token);
        if (session == null) {
            throw new AuthenticationException(Messages.TOKEN_IS_INVALID_OR_NOT_AVAILABLE);
        }

        sessionRepository.delete(session);
        return new MessageSuccessDTO().getMessage();
    }


    @Override
    public UserDTO changePassword(String token, String oldPassword, String newPassword, String deviceToken) throws ServiceException, AuthenticationException {
        User user = getUserFromToken(token);
        if (!checkPassword(oldPassword, user.getPassword())) {
            throw new ServiceException(Messages.WRONG_OLD_PASSWORD);
        }
        user.setPassword(encryptPassword(newPassword));
        userRepository.save(user);

        removeOtherToken(user);
        Session session = createUserSession(deviceToken, user);
        UserDTO userReturn = MapUtils.map(user, UserDTO.class);
        userReturn.setToken(session.getToken());
        return userReturn;
    }

    @Override
    public Boolean newPassword(String token, String newPassword) throws ServiceException, AuthenticationException {
        User user = getUserFromToken(token);
        if (user == null) {
            throw new ServiceException(Messages.USER_NOT_FOUND);
        }

        user.setPassword(encryptPassword(newPassword));
        userRepository.save(user);
        return null;
    }

    private void removeOtherToken(User user) {
        List<Session> sessions = sessionRepository.findByUser(user);
        sessionRepository.delete(sessions);
    }

    private void removeOtherTokens(User user, String deviceToken) {
        List<Session> sessions = sessionRepository.findByUserAndDeviceTokenNot(user, deviceToken);
        sessionRepository.delete(sessions);
    }

    @Override
    public UUID edit(String token, UserDTO userDTO, MultipartFile avatar) throws ServiceException, AuthenticationException {
        User user = getUserFromToken(token);

        if (userDTO != null) {
            if (!StringUtils.isBlank(userDTO.getFirstName())) {
                user.setFirstName(userDTO.getFirstName());
            }
            if (!StringUtils.isBlank(userDTO.getLastName())) {
                user.setLastName(userDTO.getLastName());
            }
            if (userDTO.getUserPhoneNumberDTOs() != null) {
                for (UserPhoneNumberDTO userPhoneNumberDTO : userDTO.getUserPhoneNumberDTOs()) {
                    if (userPhoneNumberDTO.getIsDeleted()) {
                        userPhoneNumberRepository.delete(userPhoneNumberDTO.getUuid());
                    } else {
                        UserPhoneNumber userPhoneNumber;

                        if (userPhoneNumberDTO.getUuid() == null) {
                            userPhoneNumber = new UserPhoneNumber();
                        }else {
                            userPhoneNumber = userPhoneNumberRepository.findOne(userPhoneNumberDTO.getUuid());
                            if (userPhoneNumber == null) {
                                userPhoneNumber = new UserPhoneNumber();
                            }
                        }

                        userPhoneNumber.setUser(user);
                        userPhoneNumber.setIsPrimary(userPhoneNumberDTO.getIsPrimary());
                        userPhoneNumber.setPhoneNumber(userPhoneNumberDTO.getPhoneNumber());
                        userPhoneNumberRepository.save(userPhoneNumber);
                    }
                }
            }
            if (userDTO.getUserEmailDTOs() != null) {
                for (UserEmailDTO userEmailDTO : userDTO.getUserEmailDTOs()) {
                    if (userEmailDTO.getIsDeleted()) {
                        userEmailRepository.delete(userEmailDTO.getUuid());
                    } else {
                        UserEmail userEmail;

                        if (userEmailDTO.getUuid() == null) {
                            userEmail = new UserEmail();
                        }else {
                            userEmail = userEmailRepository.findOne(userEmailDTO.getUuid());
                            if (userEmail == null) {
                                userEmail = new UserEmail();
                            }
                        }

                        userEmail.setUser(user);
                        userEmail.setIsPrimary(userEmailDTO.getIsPrimary());
                        userEmail.setEmail(userEmailDTO.getEmail());
                        userEmailRepository.save(userEmail);
                    }
                }
            }
            if (userDTO.getSendAddressDTOs() != null) {
                for (SendAddressDTO sendAddressDTO : userDTO.getSendAddressDTOs()) {
                    if (sendAddressDTO.getIsDeleted()) {
                        sendAddressRepository.delete(sendAddressDTO.getUuid());
                    } else {
                        SendAddress sendAddress;

                        if (sendAddressDTO.getUuid() == null) {
                            sendAddress = new SendAddress();
                        }else {
                            sendAddress = sendAddressRepository.findOne(sendAddressDTO.getUuid());
                            if (sendAddress == null) {
                                sendAddress = new SendAddress();
                            }
                        }

                        sendAddress.setUser(user);
                        sendAddress.setIsPrimary(sendAddressDTO.getIsPrimary());
                        sendAddress.setAddress(sendAddressDTO.getAddress());
                        sendAddressRepository.save(sendAddress);
                    }
                }
            }
            if (userDTO.getOtherNameDTOs() != null) {
                for (OtherNameDTO otherNameDTO : userDTO.getOtherNameDTOs()) {
                    if (otherNameDTO.getIsDeleted()) {
                        otherNameRepository.delete(otherNameDTO.getUuid());
                    } else {
                        OtherName otherName;

                        if (otherNameDTO.getUuid() == null) {
                            otherName = new OtherName();
                        }else {
                            otherName = otherNameRepository.findOne(otherNameDTO.getUuid());
                            if (otherName == null) {
                                otherName = new OtherName();
                            }
                        }

                        otherName.setIsPrimary(otherNameDTO.getIsPrimary());
                        otherName.setName(otherNameDTO.getName());
                        otherName.setUser(user);
                        otherNameRepository.save(otherName);
                    }
                }
            }
        }
        // User avatar
        if (avatar != null) {
            String fileName = documentService.uploadUserAvatar(user.getUuid().toString(), avatar);
            user.setAvatarName(fileName);
        }

        User userReturn = userRepository.save(user);
//        userReturn = userRepository.findOne(userReturn.getUuid());
////        User userReturn = userRepository.findOne(user.getUuid());
//        UserDTO returnUserDTO = MapUtils.map(userReturn, UserDTO.class);
//
//        returnUserDTO.setSendAddressDTOs(MapUtils.mapList(userReturn.getSendAddresses(), SendAddressDTO.class));
//        returnUserDTO.setUserEmailDTOs(MapUtils.mapList(userReturn.getUserEmails(), UserEmailDTO.class));
//        returnUserDTO.setOtherNameDTOs(MapUtils.mapList(userReturn.getOtherNames(), OtherNameDTO.class));
//        returnUserDTO.setUserPhoneNumberDTOs(MapUtils.mapList(userReturn.getUserPhoneNumbers(), UserPhoneNumberDTO.class));
//        returnUserDTO.setToken(token);
//        return returnUserDTO;

        return userReturn.getUuid();
    }

    /**
     * @Override public UserDTO edit(String token, String firstName, String lastName, String phoneNumber, String address, MultipartFile avatar) throws ServiceException, AuthenticationException {
     * User user = getUserFromToken(token);
     * <p/>
     * if (!StringUtils.isBlank(firstName)) {
     * user.setFirstName(firstName);
     * }
     * if (!StringUtils.isBlank(lastName)) {
     * user.setLastName(lastName);
     * }
     * if (!StringUtils.isBlank(phoneNumber)) {
     * user.setPhoneNumber(phoneNumber);
     * }
     * if (!StringUtils.isBlank(address)) {
     * address = address.trim();
     * user.setAddress(address);
     * <p/>
     * if (sendAddressRepository.findByAddressAndUser(address, user) == null) {
     * SendAddress sendAddress = new SendAddress();
     * sendAddress.setAddress(address);
     * sendAddress.setUser(user);
     * sendAddressRepository.save(sendAddress);
     * }
     * <p/>
     * }
     * <p/>
     * // User avatar
     * if (avatar != null) {
     * String fileName = documentService.uploadUserAvatar(user.getUuid().toString(), avatar);
     * user.setAvatarName(fileName);
     * }
     * <p/>
     * User userReturn = userRepository.save(user);
     * return MapUtils.map(userReturn, UserDTO.class);
     * }
     */
    @Override
    public UserDTO getInfo(String token, String userId) throws ServiceException, AuthenticationException {
        User user;
        if (userId == null) {
            // User get their own profile
            user = getUserFromToken(token);
        } else {
            //Other person get user profile
            try {
                user = userRepository.findOne(UUID.fromString(userId));

                if (user == null) {
                    throw new ServiceException(Messages.USER_NOT_FOUND);
                }
            } catch (IllegalArgumentException ex) {
                throw new ServiceException(Messages.USER_NOT_FOUND);
            }
        }
        UserDTO userReturn = MapUtils.map(user, UserDTO.class);
        userReturn.setSendAddressDTOs(MapUtils.mapList(user.getSendAddresses(), SendAddressDTO.class));
        userReturn.setUserEmailDTOs(MapUtils.mapList(user.getUserEmails(), UserEmailDTO.class));
        userReturn.setOtherNameDTOs(MapUtils.mapList(user.getOtherNames(), OtherNameDTO.class));
        userReturn.setUserPhoneNumberDTOs(MapUtils.mapList(user.getUserPhoneNumbers(), UserPhoneNumberDTO.class));
        userReturn.setToken(token);
        return userReturn;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public List<UserDTO> searchUser(String keyword) throws ServiceException {
        String keyCombine = StringUtil.keywordToQueryParam(keyword);
        List<User> users = nativeQuery("search_user", User.class,
                keyCombine, keyCombine, keyCombine, keyCombine);
        return MapUtils.mapList(users, UserDTO.class);
    }

    @Override
    public Boolean updateDeviceToken(String token, DeviceType deviceType, String deviceToken) throws ServiceException, AuthenticationException {
        // Find active session
        Session session = getSessionFromToken(token);

        session.setDeviceToken(deviceToken);
        session.setDeviceType(deviceType);
        sessionRepository.save(session);

        return true;
    }

    @PostConstruct
    public void createIndexes() {
        String sql = "SELECT C .relname AS index_name FROM pg_class AS A JOIN pg_index AS b ON (A .oid = b.indrelid) JOIN pg_class AS C ON (C .oid = b.indexrelid) WHERE A .relname = 'user_account';";
        List<String> indexes = entityManager.createNativeQuery(sql).getResultList();

        if (!indexes.contains("user_account_search_GIN")) {
            String sqlIndexGin = "CREATE INDEX user_account_search_GIN ON user_account USING GIN(first_name, last_name, address, email);";
            int index = entityManager.createNativeQuery(sqlIndexGin).getFirstResult();
            String sqlIndexTriggerEn = "CREATE TRIGGER TS_user_account_en BEFORE INSERT OR UPDATE ON user_account FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger(user_account, 'english', first_name, last_name, address, email)";
            String sqlIndexTriggerVn = "CREATE TRIGGER TS_user_account_vn BEFORE INSERT OR UPDATE ON user_account FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger(user_account, 'vietnamese', first_name, last_name, address, email)";
            int en = entityManager.createNativeQuery(sqlIndexTriggerEn).getFirstResult();
            int vn = entityManager.createNativeQuery(sqlIndexTriggerVn).getFirstResult();
            System.out.println("index " + index);
            System.out.println("en " + en);
            System.out.println("vn " + vn);
        } else {
            System.out.println("Already index text search");
        }
    }
}
