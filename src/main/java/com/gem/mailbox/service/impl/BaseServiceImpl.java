package com.gem.mailbox.service.impl;

import com.gem.mailbox.constant.Messages;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.model.Session;
import com.gem.mailbox.model.User;
import com.gem.mailbox.repository.SessionRepository;
import com.gem.mailbox.repository.UserRepository;
import com.gem.mailbox.service.BaseService;
import com.gem.mailbox.utils.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 2/25/15
 * Time: 11:39 AM
 */
@Transactional
@Service
public class BaseServiceImpl implements BaseService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    SessionRepository sessionRepository;

    @PersistenceContext(type = PersistenceContextType.EXTENDED) // tried this as well(type= PersistenceContextType.EXTENDED)
    protected EntityManager entityManager;

    @Override
    public User getUserFromToken(String token) throws ServiceException, AuthenticationException {
        Session session = getSessionFromToken(token);

        User user = session.getUser();
        if (user == null) {
            throw new ServiceException(Messages.USER_NOT_FOUND);
        }

        return user;
    }

    @Override
    public User getUserFromEmail(String email, Boolean isAdmin) throws ServiceException {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new ServiceException(Messages.USER_NOT_FOUND.toString());
        }

        return user;
    }

    @Override
    public Session getSessionFromToken(String token) throws ServiceException, AuthenticationException {
        if (token != null) {
            Session session = sessionRepository.findByToken(token);
            if (session != null) {
                Long expiredTime = session.getTokenExpireOn().getTime();
                if (expiredTime < System.currentTimeMillis()) {
                    throw new AuthenticationException(Messages.TOKEN_IS_EXPIRED.toString());
                }

                return session;
            } else {
                throw new AuthenticationException(Messages.TOKEN_IS_INVALID_OR_NOT_AVAILABLE.toString());
            }
        } else {
            throw new AuthenticationException(Messages.TOKEN_IS_INVALID_OR_NOT_AVAILABLE.toString());
        }
    }

    public <T> List<T> nativeQuery(String queryFileName,
                                          Class<T> clazz, Object... args) {
        String query = FileUtils.readQuery(queryFileName);

        if (args != null && args.length > 0) {
            query = String.format(query, args);
        }

        List<T> list = entityManager.createNativeQuery(query,
                clazz).getResultList();
        return list;
    }
}
