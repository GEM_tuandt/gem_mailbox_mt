package com.gem.mailbox.service.impl;

import com.gem.mailbox.model.constant.JobAction;
import com.gem.mailbox.model.constant.JobTarget;
import com.gem.mailbox.constant.Messages;
import com.gem.mailbox.dto.AdminSendOrdersDTO;
import com.gem.mailbox.dto.SendOrderDTO;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.model.*;
import com.gem.mailbox.repository.*;
import com.gem.mailbox.service.PushNotificationService;
import com.gem.mailbox.service.SendOrderService;
import com.gem.mailbox.service.loader.SendOrderLoader;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

/**
 * Implement for send order services
 * Created by neo on 2/2/2016.
 */
@Transactional
@Service
public class SendOrderServiceImpl extends BaseServiceImpl implements SendOrderService {
    @Autowired
    SendOrderRepository sendOrderRepository;
    @Autowired
    ServiceProviderRepository serviceProviderRepository;
    @Autowired
    LetterRepository letterRepository;
    @Autowired
    SendAddressRepository sendAddressRepository;
    @Autowired
    SendOrderLoader sendOrderLoader;
    @Autowired
    private JobHistoryRepository jobHistoryRepository;
    @Autowired
    private PushNotificationService pushNotificationService;

    @Override
    public Boolean customerRequestSend(String token, String sendAddress, String serviceProviderId,
                                       Long requireDate, String note, String letterIds) throws ServiceException {
        if (letterIds == null) {
            throw new ServiceException(Messages.LETTER_NOT_FOUND);
        }

        sendAddress = sendAddress.trim();

        // Build letter id list
        List<UUID> uuids = new ArrayList<>();
        String[] ids = letterIds.split(",");
        for (String id : ids) {
            try {
                uuids.add(UUID.fromString(id));
            }catch (IllegalArgumentException ex) {
                throw new ServiceException(Messages.LETTER_NOT_FOUND);
            }
        }

        // Check if has any letter that already in an order
        List<Letter> l = letterRepository.findByUuidInAndSendOrderIsNotNull(uuids);
        if (l != null && l.size() > 0) {
            throw new ServiceException(Messages.LETTER_IS_ORDERED);
        }

        // User send request
        User user = sessionRepository.findByToken(token).getUser();
        if (user == null) {
            throw new ServiceException(Messages.USER_NOT_FOUND);
        }
        // Build order
        SendOrder sendOrder = new SendOrder();
        sendOrder.setUser(user);

        // Order address
        sendOrder.setSendAddress(sendAddress);

        // Set service provider
        if (serviceProviderId == null) {
            throw new ServiceException(Messages.SERVICE_PROVIDER_NOT_FOUND);
        }

        ServiceProvider serviceProvider =
                serviceProviderRepository.findOne(UUID.fromString(serviceProviderId.trim()));
        if (serviceProvider == null) {
            throw new ServiceException(Messages.SERVICE_PROVIDER_NOT_FOUND);
        }
        sendOrder.setServiceProvider(serviceProvider);

        // Set send date required
        if (requireDate == null) {
            throw new ServiceException(Messages.DATE_IS_REQUIRED);
        }
        sendOrder.setRequireDate(requireDate);

        // Order note
        sendOrder.setNote(note);

        // Save order
        sendOrderRepository.save(sendOrder);
        sendOrder = sendOrderRepository.findOne(sendOrder.getUuid());

        // Update status of letters on order
        List<Letter> letters = letterRepository.findByUuidIn(uuids);
        for (Letter letter : letters) {
            if (letter == null) {
                throw new ServiceException(Messages.LETTER_NOT_FOUND);
            }

            letter.setIsSend(false);

            // If letter is requested to scan, remove scan request
//            if (letter.getIsScan() != null && !letter.getIsScan()) {
//                letter.setIsScan(null);
//            }

            // If letter is moved to recycle bin, remove it from recycle bin
            Boolean isTrash = letter.getIsTrash();
            if (isTrash != null && !isTrash) {
                letter.setIsTrash(null);
            }

            letter.setSendOrder(sendOrder);
            letterRepository.save(letter);
        }

        // Store send address
        if (sendAddressRepository.findByAddressAndUser(sendAddress.trim(), user) == null) {
            SendAddress address = new SendAddress();
            address.setAddress(sendAddress);
            address.setUser(user);
            sendAddressRepository.save(address);
        }

        pushNotificationService.pushToAdmin(JobAction.SEND, JobTarget.ORDER, sendOrder.getUuid());

        return true;
    }

    @Override
    public AdminSendOrdersDTO adminGetSendRequestList() throws ServiceException {
        return new AdminSendOrdersDTO(getImmediateSendOrder(), getOtherSendOrder());
    }

    /**
     * Get all order that requested send immediately
     */
    private List<SendOrderDTO> getImmediateSendOrder() {
        List<SendOrder> sendOrders = nativeQuery("get_immediate_orders", SendOrder.class);

        // Map Entities to DTOs
        List<SendOrderDTO> sendOrderDTOs = new ArrayList<>();

        // Find letters of each orders
        for (SendOrder sendOrder : sendOrders) {
            SendOrderDTO sendOrderDTO = sendOrderLoader.toDTO(sendOrder);

            // Add to SendOrderDTO list
            sendOrderDTOs.add(sendOrderDTO);
        }

        return sendOrderDTOs;
    }

    /**
     * Get all order that requested send in future
     */
    private List<SendOrderDTO> getOtherSendOrder() {
        List<SendOrder> sendOrders = nativeQuery("get_future_orders", SendOrder.class);

        // Map Entities to DTOs
        List<SendOrderDTO> sendOrderDTOs = new ArrayList<>();

        // Find letters of each orders
        for (SendOrder sendOrder : sendOrders) {
            SendOrderDTO sendOrderDTO = sendOrderLoader.toDTO(sendOrder);

            // Add to SendOrderDTO list
            sendOrderDTOs.add(sendOrderDTO);
        }

        return sendOrderDTOs;
    }

    @Override
    public Boolean adminConfirmSentOrder(String token, UUID orderId, String trackingCode, float price) throws ServiceException, AuthenticationException {
        // Admin handle task
        User admin = getUserFromToken(token);
        if (!admin.getIsAdmin()) {
            throw new ServiceException(Messages.USER_NOT_FOUND);
        }

        SendOrder sendOrder = sendOrderRepository.findOne(orderId);
        if (sendOrder == null) {
            throw new ServiceException(Messages.ORDER_NOT_FOUND);
        }

        // Update order status
        sendOrder.setIsSent(true);
        sendOrder.setPrice(price);
        sendOrder.setTrackingCode(trackingCode);
        sendOrder.setSendDate(Calendar.getInstance().getTime());
        sendOrderRepository.save(sendOrder);

        // Update letters status of order
        List<Letter> letters = letterRepository.findBySendOrder(sendOrder);
        for (Letter letter : letters) {
            letter.setIsSend(true);
            letterRepository.save(letter);
        }

        // Add Log to Job history
        JobHistory jobHistory = new JobHistory(admin, JobAction.SEND, sendOrder.getUuid(), JobTarget.ORDER);
        jobHistoryRepository.save(jobHistory);

        // Notify to user
        pushNotificationService.pushToUser(sendOrder.getUser(),
                JobAction.SEND, JobTarget.ORDER, sendOrder.getUuid());

        return true;
    }

    @Override
    public void checkOrderEmpty() throws ServiceException {
//        String query = FileUtils.readQuery("find_empty_order");
//        List<SendOrder> sendOrders = entityManager.createNativeQuery(query,
//                SendOrder.class).getResultList();
        List<SendOrder> sendOrders = nativeQuery("find_empty_order", SendOrder.class);

        // Delete empty orders
        if (sendOrders != null && sendOrders.size() > 0) {
            sendOrderRepository.delete(sendOrders);
        }
    }

    @Override
    public Boolean customerConfirmOrderArrived(String sendOrderId) throws ServiceException {
        if (StringUtils.isBlank(sendOrderId)) {
            throw new ServiceException(Messages.ORDER_NOT_FOUND);
        }

        // Find order
        SendOrder sendOrder = sendOrderRepository.findOne(UUID.fromString(sendOrderId));

        // Not found
        if (sendOrder == null) {
            throw new ServiceException(Messages.ORDER_NOT_FOUND);
        }

        // Order is not sent
        if (sendOrder.getIsSent() == null || !sendOrder.getIsSent()) {
            throw new ServiceException(Messages.ORDER_NOT_SENT);
        }

        sendOrder.setIsArrived(true);
        sendOrderRepository.save(sendOrder);

        return true;
    }

    @Override
    public SendOrderDTO orderDetail(String sendOrderId) throws ServiceException {
        try {
            UUID id = UUID.fromString(sendOrderId);

            SendOrder sendOrder = sendOrderRepository.findOne(id);

            return sendOrderLoader.toDTO(sendOrder);
        } catch (IllegalArgumentException ex) {
            throw new ServiceException(Messages.ORDER_NOT_FOUND);
        }
    }
}
