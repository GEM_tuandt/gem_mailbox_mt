package com.gem.mailbox.service.impl;

import com.gem.mailbox.repository.UserPhoneNumberRepository;
import com.gem.mailbox.service.UserPhoneNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implement for service provider services
 * Created by neo on 2/2/2016.
 */
@Transactional
@Service
public class UserPhoneNumberServiceImpl extends BaseServiceImpl implements UserPhoneNumberService {
    @Autowired
    UserPhoneNumberRepository userPhoneNumberRepository;
}
