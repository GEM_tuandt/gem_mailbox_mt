package com.gem.mailbox.service.impl;

import com.gem.mailbox.constant.Constants;
import com.gem.mailbox.dto.ServiceProviderDTO;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.mapping.MapUtils;
import com.gem.mailbox.model.ServiceProvider;
import com.gem.mailbox.repository.OtherNameRepository;
import com.gem.mailbox.service.OtherNameService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.Iterator;
import java.util.List;

/**
 * Implement for service provider services
 * Created by neo on 2/2/2016.
 */
@Transactional
@Service
public class OtherNameServiceImpl extends BaseServiceImpl implements OtherNameService {
    @Autowired
    OtherNameRepository otherNameRepository;
}
