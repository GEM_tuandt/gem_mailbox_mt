package com.gem.mailbox.service.impl;

import com.gem.mailbox.constant.Constants;
import com.gem.mailbox.dto.ServiceProviderDTO;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.mapping.MapUtils;
import com.gem.mailbox.model.ServiceProvider;
import com.gem.mailbox.repository.ServiceProviderRepository;
import com.gem.mailbox.service.ServiceProviderService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.Iterator;
import java.util.List;

/**
 * Implement for service provider services
 * Created by neo on 2/2/2016.
 */
@Transactional
@Service
public class ServiceProviderServiceImpl extends BaseServiceImpl implements ServiceProviderService {
    @Autowired
    ServiceProviderRepository serviceProviderRepository;

    @Override
    public List<ServiceProviderDTO> getServiceProviders() throws ServiceException {
        Iterator<ServiceProvider> serviceProviderIterator = serviceProviderRepository.findAll().iterator();

        List<ServiceProvider> serviceProviders = Lists.newArrayList(serviceProviderIterator);

        return MapUtils.mapList(serviceProviders, ServiceProviderDTO.class);
    }

    @PostConstruct
    public void initData() {
        Iterator<ServiceProvider> serviceProviderIterator = serviceProviderRepository.findAll().iterator();

        // Add Service provider if not exist
        for (String providerName : Constants.SERVICE_PROVIDERS) {
            ServiceProvider serviceProvider = serviceProviderRepository.findByProviderName(providerName);
            if (serviceProvider == null) {
                serviceProvider = new ServiceProvider();
                serviceProvider.setProviderName(providerName);
                serviceProviderRepository.save(serviceProvider);
            }
        }
    }
}
