package com.gem.mailbox.service.impl;

import com.gem.mailbox.constant.Constants;
import com.gem.mailbox.constant.Messages;
import com.gem.mailbox.dto.*;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.mapping.MapUtils;
import com.gem.mailbox.model.JobHistory;
import com.gem.mailbox.model.Letter;
import com.gem.mailbox.model.SendOrder;
import com.gem.mailbox.model.User;
import com.gem.mailbox.model.constant.JobAction;
import com.gem.mailbox.model.constant.JobTarget;
import com.gem.mailbox.repository.*;
import com.gem.mailbox.service.DocumentService;
import com.gem.mailbox.service.LetterService;
import com.gem.mailbox.service.PushNotificationService;
import com.gem.mailbox.service.SendOrderService;
import com.gem.mailbox.utils.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 2/25/15
 * Time: 11:39 AM
 */
@Transactional
@Service
public class LetterServiceImpl extends BaseServiceImpl implements LetterService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    SessionRepository sessionRepository;
    @Autowired
    DocumentService documentService;
    @Autowired
    LetterRepository letterRepository;
    @Autowired
    SendOrderRepository sendOrderRepository;
    @Autowired
    JobHistoryRepository jobHistoryRepository;
    @Autowired
    PushNotificationService pushNotificationService;
    @Autowired
    private SendOrderService sendOrderService;

    @Override
    public LetterDTO addNewLetter(String token, String userId, Long date, String qrCode, String note, MultipartFile thumbImage) throws ServiceException, AuthenticationException {
        // Admin handle task
        User admin = getUserFromToken(token);

        // User is assigned to letter
        User user = userRepository.findOne(UUID.fromString(userId));

        if (user == null || user.getIsAdmin()) {
            throw new ServiceException(Messages.USER_NOT_FOUND);
        }

        // New letter entity
        Letter letter = new Letter();

        // Thumb of letter
        String fileName = documentService.uploadLetterThumb(letter.getUuid(), thumbImage);

        // Date created new letter
        date = (date == null || date == 0 ? System.currentTimeMillis() : date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);

        // Bind letter data
        letter.setUser(user);
        letter.setDate(calendar.getTime());
        letter.setNote(note);
        letter.setQrCode(qrCode);
        letter.setThumbUrl(fileName);

        // Save to DB
        letterRepository.save(letter);

        // Return DTO
        LetterDTO letterDTO;

        try {
            letterDTO = MapUtils.map(letter, LetterDTO.class);
        } catch (MappingException e) {
            throw new ServiceException(Messages.ERROR_MAPPING);
        }

        // Add Log to Job history
        JobHistory jobHistory = new JobHistory(admin, JobAction.ADD, letter.getUuid(), JobTarget.LETTER);
        jobHistoryRepository.save(jobHistory);

        // Notify to user
        pushNotificationService.pushToUser(user,
                JobAction.ADD, JobTarget.LETTER, letter.getUuid());

        return letterDTO;
    }

    @Override
    public AdminSummaryDTO adminGetLetterSummary(String token) throws ServiceException, AuthenticationException {
        User user = getUserFromToken(token);

        if (user == null || !user.getIsAdmin()) {
            throw new ServiceException(Messages.USER_NOT_FOUND);
        }

        // Return object
        AdminSummaryDTO adminSummaryDTO = new AdminSummaryDTO();
        adminSummaryDTO.setUserId(user.getUuid());
//        adminSummaryDTO.setCountScan(letterRepository.findByIsScan(false).size());
        adminSummaryDTO.setCountScan(letterRepository.findByIsScanFalseAndIsSendIsNullAndIsTrashIsNull().size());
        adminSummaryDTO.setCountSend(sendOrderRepository.findByIsSentFalse().size());
        adminSummaryDTO.setCountTrash(letterRepository.findAdminTrashList(new Date()).size());

        return adminSummaryDTO;
    }

    @Override
    public List<UserDTO> adminGetScanList(String excludeLetters) throws ServiceException {
        String combineIds;

        combineIds = StringUtil.listToValuesParam(excludeLetters);

        List<User> users = nativeQuery("get_scan_letter_group_by_user", User.class, combineIds);
        List<UserDTO> userDTOs = new ArrayList<>();

        // Set letters are requesting to scan
        for (User user : users) {
            UserDTO userDTO = MapUtils.map(user, UserDTO.class);

            List<Letter> letters;
            if (StringUtils.isBlank(excludeLetters)) {
                letters = letterRepository.findByUserAndIsScanFalseAndIsSendIsNullAndIsTrashIsNullOrderByDateUpdatedAsc(user);
            } else {
                letters =
                        letterRepository.findByUserAndIsScanFalseAndIsSendIsNullAndIsTrashIsNullAndUuidNotInOrderByDateUpdatedAsc(
                                user, StringUtil.idsToUUIDList(excludeLetters));
            }
            userDTO.setRequestScanLetters(MapUtils.mapList(letters, LetterDTO.class));
            userDTOs.add(userDTO);
        }

        return userDTOs;
    }

    @Override
    // This method is unused now
    public List<LetterDTO> adminGetSendList() throws ServiceException {
        List<Letter> letters = letterRepository.findByIsSend(false);

        return MapUtils.map(letters, true);
    }

    @Override
    public List<UserDTO> adminGetTrashList() throws ServiceException {
        List<User> users = nativeQuery("get_trash_letter_group_by_user", User.class);
        List<UserDTO> userDTOs = new ArrayList<>();

        // Set letters are requesting to scan
        for (User user : users) {
            UserDTO userDTO = MapUtils.map(user, UserDTO.class);

            List<Letter> letters = letterRepository.findAdminTrashLetterByUser(user, new Date());
            userDTO.setRequestTrashLetters(MapUtils.mapList(letters, LetterDTO.class));
            userDTOs.add(userDTO);
        }

        return userDTOs;
    }

    @Override
    public CustomerLettersDTO customerGetLetterList(String token, String userId) throws ServiceException, AuthenticationException {
        User user;
        if (userId == null) {
            user = getUserFromToken(token);
        } else {
            try {
                user = userRepository.findOne(UUID.fromString(userId));

                if (user == null) {
                    throw new ServiceException(Messages.USER_NOT_FOUND);
                }
            }catch (IllegalArgumentException ex) {
                throw new ServiceException(Messages.USER_NOT_FOUND);
            }

        }
        if (user.getIsAdmin()) {
            throw new ServiceException(Messages.USER_NOT_FOUND);
        }

        // Get new or un-sent, not in document letters
        List<Letter> letters = letterRepository.findLettersCustomer(user);
        List<LetterDTO> letterDTOs = MapUtils.map(letters, false);

        // Get letters are in Document
        List<Letter> lettersInDocument = letterRepository.findLettersInDocument(user);
        List<LetterDTO> letterInDocumentDTOs = MapUtils.map(lettersInDocument, false);

        // Get letter orders are sent to me
        List<SendOrder> sendOrders = sendOrderRepository.findByUserAndIsArrivedFalseAndIsSentTrue(user);
        List<SendOrderDTO> sendOrderDTOs = new ArrayList<>();
        for (SendOrder sendOrder : sendOrders) {
            ServiceProviderDTO serviceProviderDTO =
                    MapUtils.map(sendOrder.getServiceProvider(), ServiceProviderDTO.class);
            SendOrderDTO sendOrderDTO = MapUtils.map(sendOrder, SendOrderDTO.class);
            sendOrderDTO.setServiceProviderDTO(serviceProviderDTO);

            // Set letters of order
            sendOrderDTO.setLetterDTOs(MapUtils.mapList(letterRepository.findBySendOrder(sendOrder), LetterDTO.class));

            sendOrderDTOs.add(sendOrderDTO);
        }

        // Get trashed letters
        List<Letter> trashedLetters = letterRepository.findTrashLettersByUser(user);
        List<LetterDTO> trashedLetterDTOs = MapUtils.map(trashedLetters, false);

        return new CustomerLettersDTO(letterDTOs, letterInDocumentDTOs, sendOrderDTOs, trashedLetterDTOs);
    }

    @Override
    public LetterDTO getLetterDetail(UUID letterId) throws ServiceException {
        LetterDTO letterDTO;
        try {
            Letter letter = letterRepository.findOne(letterId);
            if (letter == null) {
                throw new ServiceException(Messages.LETTER_NOT_FOUND);
            }
            letterDTO = MapUtils.map(letter, LetterDTO.class);

            // Add user to DTO
            User user = letter.getUser();
            if (user == null) {
                throw new ServiceException(Messages.USER_NOT_FOUND);
            }

            letterDTO.setUserDTO(MapUtils.map(user, UserDTO.class));

            // Mark letter is read
            letter.setIsNew(false);
            letterRepository.save(letter);
        } catch (MappingException e) {
            throw new ServiceException(Messages.ERROR_MAPPING);
        }
        return letterDTO;
    }

    @Override
    public SendOrderDTO getOrderFromLetter(UUID letterId) throws ServiceException {
        SendOrder sendOrder = letterRepository.findOne(letterId).getSendOrder();
        if (sendOrder == null) {
            return null;
        }
        
        ServiceProviderDTO serviceProviderDTO =
                MapUtils.map(sendOrder.getServiceProvider(), ServiceProviderDTO.class);
        SendOrderDTO sendOrderDTO = MapUtils.map(sendOrder, SendOrderDTO.class);
        sendOrderDTO.setServiceProviderDTO(serviceProviderDTO);

        // Set letters of order
        sendOrderDTO.setLetterDTOs(MapUtils.mapList(letterRepository.findBySendOrder(sendOrder), LetterDTO.class));

        return sendOrderDTO;
    }

    @Override
    public Boolean customerRequestScan(String uuids) throws ServiceException {
        List<UUID> letterIds = StringUtil.idsToUUIDList(uuids);

        List<Letter> letters = letterRepository.findByUuidIn(letterIds);
        for (Letter letter : letters) {
            if (letter == null) {
                throw new ServiceException(Messages.LETTER_NOT_FOUND);
            }

            // If letter was requested to send, return exception
            if (letter.getIsSend() != null && !letter.getIsSend()) {
                throw new ServiceException(Messages.LETTER_IS_ORDERED);
            }

            // Change letter status
            letter.setIsScan(false);

            // If letter is moved to recycle bin, remove it from recycle bin
            Boolean isTrash = letter.getIsTrash();
            if (isTrash != null && !isTrash) {
                letter.setIsTrash(null);
            }

            // Mark letter is not new
            letter.setIsNew(false);

            letterRepository.save(letter);
        }

        // Notify to admin
        if (letters.size() > 0) {
            pushNotificationService.pushToAdmin(JobAction.SCAN, JobTarget.LETTER, letters.get(0).getUuid());
        }

        return true;
    }

    @Override
    public Boolean customerRequestTrash(String uuids) throws ServiceException {
        // Parse uuid from String to UUID
        List<UUID> letterIds = StringUtil.idsToUUIDList(uuids);

        // Find letters
        List<Letter> letters = letterRepository.findByUuidIn(letterIds);

        for (Letter letter : letters) {
            letter.setIsTrash(false);
            letter.setIsInDocument(false);

            // Set date to destroy
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis() + Constants.TIME_TO_DESTROY);
            letter.setDateTrash(calendar.getTime());

            // If letter is on a send order, remove it from send order.
            letter.setSendOrder(null);
            letter.setIsSend(null);

            // If letter requesting to scan, cancel request
            if (letter.getIsScan() != null && !letter.getIsScan()) {
                letter.setIsScan(null);
            }
            letterRepository.save(letter);
        }
        sendOrderService.checkOrderEmpty();
        return true;
    }

    @Override
    public Boolean adminConfirmTrashed(String token, UUID letterId) throws ServiceException, AuthenticationException {
        // Admin handle task
        User admin = getUserFromToken(token);
        if (admin == null || !admin.getIsAdmin()) {
            throw new ServiceException(Messages.USER_NOT_FOUND);
        }

        Letter letter = letterRepository.findOne(letterId);
        if (letter == null) {
            throw new ServiceException(Messages.LETTER_NOT_FOUND);
        }

        letter.setIsTrash(true);
        letterRepository.save(letter);

        // Add Log to Job history
        JobHistory jobHistory = new JobHistory(admin, JobAction.DESTROY, letter.getUuid(), JobTarget.LETTER);
        jobHistoryRepository.save(jobHistory);

        // Notify to user
        pushNotificationService.pushToUser(letter.getUser(),
                JobAction.DESTROY, JobTarget.LETTER, letter.getUuid());

        return true;
    }

    @Override
    public Boolean adminScanLetter(String token, UUID letterId, MultipartFile pdfFile) throws ServiceException, AuthenticationException {
        // Admin handle task
        User admin = getUserFromToken(token);
        if (!admin.getIsAdmin()) {
            throw new ServiceException(Messages.USER_NOT_FOUND);
        }

        Letter letter = letterRepository.findOne(letterId);
        if (letter == null) {
            throw new ServiceException(Messages.LETTER_NOT_FOUND);
        }

        documentService.uploadScanFile(letterId, pdfFile);

        // Update letter status
        letter.setIsScan(true);
        letter.setDateScan(new Date());
        letterRepository.save(letter);

        // Add Log to Job history
        JobHistory jobHistory = new JobHistory(admin, JobAction.SCAN, letter.getUuid(), JobTarget.LETTER);
        jobHistoryRepository.save(jobHistory);

        // Notify to user
        pushNotificationService.pushToUser(letter.getUser(),
                JobAction.SCAN, JobTarget.LETTER, letter.getUuid());

        return true;
    }

    @Override
    public Boolean validateQrcode(String qrCode) throws ServiceException {
        // This QRCode is not used by any letter
        return letterRepository.findByQrCode(qrCode) == null;
    }

    @Override
    public LetterDTO findLetterByQrCode(String qrCode) throws ServiceException {
        Letter letter = letterRepository.findByQrCode(qrCode);
        if (letter == null) {
            throw new ServiceException(Messages.LETTER_NOT_FOUND);
        }

        LetterDTO letterDTO = MapUtils.map(letter, LetterDTO.class);

        // Set user
        letterDTO.setUserDTO(MapUtils.map(letter.getUser(), UserDTO.class));
        return letterDTO;
    }

    @Override
    public Boolean customerRestoreFromTrash(String letterIds) throws ServiceException {
        try {
            List<UUID> uuids = StringUtil.idsToUUIDList(letterIds);

            // Find letters
            List<Letter> letters = letterRepository.findByUuidIn(uuids);

            // Update status
            for (Letter letter : letters) {
                letter.setIsTrash(null);
                letter.setDateTrash(null);
            }

            // Save
            letterRepository.save(letters);
        } catch (IllegalArgumentException ex) {
            throw new ServiceException(Messages.LETTER_NOT_FOUND);
        }

        return true;
    }

    /**
     * Return Friday date of current week
     */
    @SuppressWarnings("unused")
    private Date getWeekendDate() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
        return c.getTime();
    }

    @Override
    public Boolean customerEditLetter(String token, String letterId, String title, String userNote, String letterFrom, String letterTo) throws ServiceException, AuthenticationException {
        try {
            // Find letter
            Letter letter = letterRepository.findOne(UUID.fromString(letterId));

            // Check user session
            User user = getUserFromToken(token);

            if (letter.getUser().getUuid() != user.getUuid()) {
                throw new ServiceException(Messages.USER_NOT_OWN_LETTER);
            }

            // Update letter info
            if (!StringUtils.isBlank(title)) {
                letter.setTitle(title);
            }
            if (!StringUtils.isBlank(userNote)) {
                letter.setUserNote(userNote);
            }
            if (!StringUtils.isBlank(letterFrom)) {
                letter.setLetterFrom(letterFrom);
            }
            if (!StringUtils.isBlank(letterTo)) {
                letter.setLetterTo(letterTo);
            }

            letterRepository.save(letter);
        } catch (IllegalArgumentException ex) {
            throw new ServiceException(Messages.LETTER_NOT_FOUND);
        }
        return true;
    }

    @Override
    public Boolean customerRequestDestroy(String token, String letterId) throws ServiceException, AuthenticationException {
        User user = getUserFromToken(token);

        Letter letter = letterRepository.findOne(UUID.fromString(letterId));

        if (user.getIsAdmin() || letter.getUser().getUuid() != user.getUuid()) {
            throw new ServiceException(Messages.USER_NOT_OWN_LETTER);
        }

        letter.setIsTrash(false);
        // Set date trash to NULL mean customer want to destroy immediately
        letter.setDateTrash(null);
        letterRepository.save(letter);

        // Notify to admin
        pushNotificationService.pushToAdmin(JobAction.DESTROY, JobTarget.LETTER, letter.getUuid());
        return true;
    }

    @Override
    public Boolean customerSaveToDocument(String token, String uuids) throws ServiceException, AuthenticationException {
        List<UUID> letterIds = StringUtil.idsToUUIDList(uuids);
        List<Letter> letters = letterRepository.findByUuidIn(letterIds);
        for (Letter letter : letters) {
            // Remove from trash
            letter.setIsTrash(null);
            letter.setIsInDocument(true);
        }
        letterRepository.save(letters);
        return true;
    }

    @Override
    public Boolean customerRemoveFromDocument(String uuids) throws ServiceException {
        List<UUID> letterIds = StringUtil.idsToUUIDList(uuids);
        List<Letter> letters = letterRepository.findByUuidIn(letterIds);
        for (Letter letter : letters) {
            letter.setIsInDocument(false);
        }
        letterRepository.save(letters);
        return true;
    }
}
