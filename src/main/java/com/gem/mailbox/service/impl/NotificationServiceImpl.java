package com.gem.mailbox.service.impl;

import com.gem.mailbox.dto.NotificationDTO;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.mapping.MapUtils;
import com.gem.mailbox.model.Letter;
import com.gem.mailbox.model.Notification;
import com.gem.mailbox.model.User;
import com.gem.mailbox.model.constant.JobAction;
import com.gem.mailbox.model.constant.JobTarget;
import com.gem.mailbox.repository.NotificationRepository;
import com.gem.mailbox.repository.UserEmailRepository;
import com.gem.mailbox.service.NotificationService;
import com.gem.mailbox.service.UserEmailService;
import com.gem.mailbox.utils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * Implement for service provider services
 * Created by neo on 2/2/2016.
 */
@Transactional
@Service
public class NotificationServiceImpl extends BaseServiceImpl implements NotificationService {
    @Autowired
    NotificationRepository notificationRepository;

    @Override
    public List<NotificationDTO> getNotifications(String token, Integer pageIndex, Integer pageSize) throws ServiceException, AuthenticationException {
        User user = getUserFromToken(token);

        // Paging
        Pageable pageable = new PageRequest(pageIndex, pageSize, new Sort(Sort.Direction.DESC, "dateUpdated"));

        // Query
        List<Notification> notifications = notificationRepository.findByUser(user, pageable);

        // Mapping object
        List<NotificationDTO> notificationDTOs = MapUtils.mapList(notifications, NotificationDTO.class);
        return notificationDTOs;
    }

    @Override
    public void add(User user, JobAction jobAction, JobTarget jobTarget, UUID targetId) {
        // Create new Notification record
        Notification notification = new Notification();
        notification.setUser(user);
        notification.setJobAction(jobAction);
        notification.setJobTarget(jobTarget);
        notification.setTargetId(targetId);

        notificationRepository.save(notification);
    }

    @Override
    public void remove(String token, String uuids) throws ServiceException, AuthenticationException {
        User user = getUserFromToken(token);

        List<UUID> notificationIds = StringUtil.idsToUUIDList(uuids);
        List<Notification> notifications = notificationRepository.findByUuidIn(notificationIds);
        notificationRepository.delete(notifications);
    }
}
