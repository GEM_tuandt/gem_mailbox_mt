package com.gem.mailbox.service.impl;

import com.gem.mailbox.model.LetterScan;
import com.gem.mailbox.repository.LetterScanRepository;
import com.gem.mailbox.service.FileService;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by quynhtq on 8/6/2015.
 */
@Service
@Transactional
public class FileServiceImpl implements FileService {
    @Value("${pdf.files.path}")
    private String pdfPath;

    @Value("${files.tempPath}")
    private String filePath;

    @Autowired
    LetterScanRepository letterScanRepository;

    private void addPhotoList(Document document, List<LetterScan> letterScen) throws IOException, DocumentException {
//        for (LetterScan letterScan1 : letterScen) {
//            File originalFile = new File(filePath + File.separator + letterScan1.getObjectKey());
//            String pdfImageFilePath = filePath + File.separator + Constants.PDF + letterScan1.getObjectKey();
//            Path path = Paths.get(pdfImageFilePath);
//            File pdfImageFile = new File(pdfImageFilePath);
//            if (!pdfImageFile.exists()) {
//                Thumbnails.of(originalFile)
//                        .size(530, 530)
//                        .toFile(pdfImageFilePath);
//            }
//            Image image = Image.getInstance(Files.readAllBytes(path));
//            document.add(image);
//        }
    }

//    private Map<UUID, List<LetterScan>> getUuidListMap(List<LetterScan> photoList) {
//        Map<UUID, List<LetterScan>> vaccinationDocumentMap = new HashMap<>();
//        for (LetterScan photo : photoList) {
//            List<LetterScan> letterScanList1 = vaccinationDocumentMap.get(photo.getEntityId());
//            if (letterScanList1 == null) {
//                letterScanList1 = new ArrayList<>();
//            }
//            letterScanList1.add(photo);
//            vaccinationDocumentMap.put(photo.getEntityId(), letterScanList1);
//        }
//        return vaccinationDocumentMap;
//    }

    private String getTemplateByPathName(String templateFilePath) throws IOException {
        //example: templates\profile_block
        Resource resource = new ClassPathResource(templateFilePath);
        BufferedReader br = new BufferedReader(new InputStreamReader(resource.getInputStream()), 1024);
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            stringBuilder.append(line).append('\n');
        }
        br.close();
        return stringBuilder.toString();
    }

}
