package com.gem.mailbox.service;

import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.model.Session;
import com.gem.mailbox.model.User;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 2/25/15
 * Time: 11:38 AM
 */
public interface BaseService
{
    public User getUserFromToken(String token) throws ServiceException, AuthenticationException;

    public User getUserFromEmail(String email, Boolean isAdmin) throws ServiceException;

    public Session getSessionFromToken(String token) throws ServiceException, AuthenticationException;
}
