package com.gem.mailbox.service;

import com.gem.mailbox.dto.NotificationDTO;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.model.Notification;
import com.gem.mailbox.model.User;
import com.gem.mailbox.model.constant.JobAction;
import com.gem.mailbox.model.constant.JobTarget;

import java.util.List;
import java.util.UUID;

/**
 * Notification Services
 * Created by neo on 2/2/2016.
 */
public interface NotificationService extends BaseService {
    List<NotificationDTO> getNotifications(String token, Integer pageIndex, Integer pageSize) throws ServiceException, AuthenticationException;

    void add(User user, JobAction jobAction, JobTarget jobTarget, UUID targetId);


    void remove(String token, String uuids) throws ServiceException, AuthenticationException;
}
