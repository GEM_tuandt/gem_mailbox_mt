package com.gem.mailbox.controller;

import com.gem.mailbox.dto.ResponseDTO;
import com.gem.mailbox.dto.SendAddressDTO;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.service.SendAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Service provider controller
 */
@Controller
public class SendAddressController {
    @Autowired
    SendAddressService sendAddressService;

    @RequestMapping(value = "/findSendAddress", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO findSendAddress(@RequestHeader String token,
                                       @RequestParam String keyword) throws ServiceException, AuthenticationException {
        List<SendAddressDTO> sendAddresses = sendAddressService.findSendAddress(token, keyword);
        return new ResponseDTO<>(sendAddresses);
    }
}

