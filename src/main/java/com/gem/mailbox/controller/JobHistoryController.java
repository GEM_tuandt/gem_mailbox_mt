package com.gem.mailbox.controller;

import com.gem.mailbox.dto.JobHistoryDTO;
import com.gem.mailbox.dto.ResponseDTO;
import com.gem.mailbox.dto.ServiceProviderDTO;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.mapping.MapUtils;
import com.gem.mailbox.model.JobHistory;
import com.gem.mailbox.model.ServiceProvider;
import com.gem.mailbox.repository.JobHistoryRepository;
import com.gem.mailbox.service.JobHistoryService;
import com.gem.mailbox.service.ServiceProviderService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Iterator;
import java.util.List;

/**
 * Service provider controller
 */
@Controller
public class JobHistoryController {
    @Autowired
    JobHistoryService jobHistoryService;

    @RequestMapping(value = "/admin/getJobHistory", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO adminGetJobHistory(@RequestHeader String token,
                                          @RequestParam Integer pageIndex, @RequestParam Integer pageSize) throws ServiceException, AuthenticationException {

        return new ResponseDTO<>(jobHistoryService.getJobHistory(token, pageIndex, pageSize));
    }
}

