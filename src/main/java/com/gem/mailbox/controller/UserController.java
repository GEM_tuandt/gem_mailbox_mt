package com.gem.mailbox.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gem.mailbox.dto.ErrorResponseDTO;
import com.gem.mailbox.dto.ResponseDTO;
import com.gem.mailbox.dto.MessageSuccessDTO;
import com.gem.mailbox.dto.UserDTO;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.model.constant.DeviceType;
import com.gem.mailbox.service.FileService;
import com.gem.mailbox.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 2/24/15
 * Time: 4:20 PM
 */
@Controller
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    FileService fileService;

    @RequestMapping(value = "/signup", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    @ResponseBody
    public ResponseDTO signUp(@Valid @RequestBody UserDTO userDTO) throws ServiceException {
        userService.signUp(userDTO);
        return new ResponseDTO<>(new MessageSuccessDTO());
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    @ResponseBody
    public ResponseDTO login(@Valid @RequestBody UserDTO userDTO,
                             @RequestParam(required = false) String deviceToken) throws ServiceException {
        UserDTO loggedInUser = userService.login(userDTO, deviceToken);
        return new ResponseDTO<>(loggedInUser);
    }


    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO logout(@RequestHeader String token) throws ServiceException, AuthenticationException {
        userService.logout(token);
        return new ResponseDTO<>(new MessageSuccessDTO());
    }


    @RequestMapping(value = "/user/changePassword", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO changePassword(@RequestHeader String token,
                                      @RequestParam String oldPassword,
                                      @RequestParam String newPassword,
                                      @RequestParam String deviceToken) throws ServiceException, AuthenticationException {
        userService.changePassword(token, oldPassword, newPassword, deviceToken);
        return new ResponseDTO<>(new MessageSuccessDTO());
    }

    @RequestMapping(value = "/user/newPassword", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO newPassword(@RequestHeader String token,
                                   @RequestParam String newPassword) throws ServiceException, AuthenticationException {
        userService.newPassword(token, newPassword);
        return new ResponseDTO<>(new MessageSuccessDTO());
    }

    @RequestMapping(value = "/user/edit", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO editUser(@RequestHeader String token,
                                @RequestParam(required = false) String user,
                                @RequestPart(required = false) MultipartFile avatar) throws ServiceException, AuthenticationException {
        ObjectMapper objectMapper = new ObjectMapper();
        UserDTO userDTO = null;
        try {
            userDTO = objectMapper.readValue(user, UserDTO.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        UserDTO returnUserDTO;
        UUID userUuid = userService.edit(token, userDTO, avatar);
        returnUserDTO = userService.getInfo(token, userUuid.toString());
        return new ResponseDTO<>(returnUserDTO);
    }
//    @RequestMapping(value = "/user/edit", method = RequestMethod.POST)
//    @ResponseBody
//    public ResponseDTO editUser(@RequestHeader String token,
//                                @RequestParam(required = false) String firstName,
//                                @RequestParam(required = false) String lastName,
//                                @RequestParam(required = false) String phoneNumber,
//                                @RequestParam(required = false) String address,
//                                @RequestParam(required = false) MultipartFile avatar) throws ServiceException, AuthenticationException {
//        UserDTO userDTO = userService.edit(token, firstName, lastName, phoneNumber, address, avatar);
//        return new ResponseDTO<>(userDTO);
//    }

    @RequestMapping(value = "/user/getInfo", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO getInfo(@RequestHeader String token,
                               @RequestParam(required = false) String userId) throws ServiceException, AuthenticationException {
        UserDTO userDTO = userService.getInfo(token, userId);
        return new ResponseDTO<>(userDTO);
    }

    @RequestMapping(value = "/user/search", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO searchUser(@RequestHeader String token,
                                  @RequestParam String keyword) throws ServiceException {
        List<UserDTO> userDTOs = userService.searchUser(keyword);
        return new ResponseDTO<>(userDTOs);
    }


    @RequestMapping(value = "/user/updateDeviceToken", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO userUpdateDeviceToken(@RequestHeader String token,
                                  @RequestParam DeviceType deviceType,
                                  @RequestParam String deviceToken) throws ServiceException, AuthenticationException {
               userService.updateDeviceToken(token, deviceType, deviceToken);
        return new ResponseDTO<>(new MessageSuccessDTO());
    }
}