package com.gem.mailbox.controller;

import com.gem.mailbox.dto.AdminSendOrdersDTO;
import com.gem.mailbox.dto.MessageSuccessDTO;
import com.gem.mailbox.dto.ResponseDTO;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.service.SendOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

/**
 * Send order controller
 * Created by neo on 2/2/2016.
 */
@Controller
public class SendOrderController {
    @Autowired
    private SendOrderService sendOrderService;

    @RequestMapping(value = "/customer/requestSend", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO customerRequestSend(@RequestHeader String token,
                                           @RequestParam String sendAddress,
                                           @RequestParam String serviceProviderId,
                                           @RequestParam Long requireDate,
                                           @RequestParam String note,
                                           @RequestParam String letterIds) throws ServiceException {
        sendOrderService.customerRequestSend(token, sendAddress, serviceProviderId,
                requireDate, note, letterIds);
        return new ResponseDTO<>(new MessageSuccessDTO());
    }

    @RequestMapping(value = "/admin/getSendRequestList", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO adminGetSendRequestList(@RequestHeader String token) throws ServiceException {
        AdminSendOrdersDTO sendOrderDTOs = sendOrderService.adminGetSendRequestList();
        return new ResponseDTO<>(sendOrderDTOs);
    }

    @RequestMapping(value = "/admin/confirmSent", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO adminConfirmSentOrder(@RequestHeader String token,
                                             @RequestParam String sendOrderId,
                                             @RequestParam String trackingCode,
                                             @RequestParam float price) throws ServiceException, AuthenticationException {
        sendOrderService.adminConfirmSentOrder(token, UUID.fromString(sendOrderId), trackingCode, price);
        return new ResponseDTO<>(new MessageSuccessDTO());
    }

    @RequestMapping(value = "/customer/confirmOrderArrived", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO customerConfirmOrderArrived(@RequestHeader String token,
                                                   @RequestParam String sendOrderId) throws ServiceException {
        sendOrderService.customerConfirmOrderArrived(sendOrderId);
        return new ResponseDTO<>(new MessageSuccessDTO());
    }

    @RequestMapping(value = "/orderDetail", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO orderDetail(@RequestHeader String token,
                                   @RequestParam String sendOrderId) throws ServiceException {
        return new ResponseDTO<>(sendOrderService.orderDetail(sendOrderId));
    }
}
