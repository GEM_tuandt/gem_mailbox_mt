package com.gem.mailbox.controller;

import com.gem.mailbox.dto.ResponseDTO;
import com.gem.mailbox.dto.ServiceProviderDTO;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.service.ServiceProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Service provider controller
 */
@Controller
public class ServiceProviderController {
    @Autowired
    ServiceProviderService serviceProviderService;

    @RequestMapping(value = "/serviceProviders", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO getServiceProvider(@RequestHeader String token) throws ServiceException {
        List<ServiceProviderDTO> serviceProviders = serviceProviderService.getServiceProviders();
        return new ResponseDTO<>(serviceProviders);
    }
}

