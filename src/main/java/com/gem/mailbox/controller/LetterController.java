package com.gem.mailbox.controller;

import com.gem.mailbox.dto.*;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.service.LetterService;
import com.gem.mailbox.service.SendOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

/**
 * Letter controller
 * Created by neo on 1/29/2016.
 */
@Controller
public class LetterController {
    @Autowired
    private LetterService letterService;
    @Autowired
    private SendOrderService sendOrderService;

    @RequestMapping(value = "/admin/addLetter", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO addLetter(@RequestHeader String token,
                                 @RequestParam String userId,
                                 @RequestParam(required = false) Long date,
                                 @RequestParam String qrCode,
                                 @RequestParam(required = false) String note,
                                 @RequestParam MultipartFile thumbFile) throws ServiceException, AuthenticationException {
        // Add new letter
        LetterDTO letter = letterService.addNewLetter(token, userId, date, qrCode, note, thumbFile);
        return new ResponseDTO<>(letter);
    }

    @RequestMapping(value = "/admin/summary", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO adminSummary(@RequestHeader String token) throws ServiceException, AuthenticationException {
        AdminSummaryDTO adminSummaryDTO = letterService.adminGetLetterSummary(token);
        return new ResponseDTO<>(adminSummaryDTO);
    }

    @RequestMapping(value = "/admin/scanList", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO adminGetScanList(@RequestHeader String token,
                                        @RequestParam(required = false) String excludeLetters) throws ServiceException {
        List<UserDTO> userDTOs = letterService.adminGetScanList(excludeLetters);
        return new ResponseDTO<>(userDTOs);
    }

    @RequestMapping(value = "/admin/sendList", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO adminGetSendList(@RequestHeader String token) throws ServiceException {
        List<LetterDTO> letterDTOs = letterService.adminGetSendList();
        return new ResponseDTO<>(letterDTOs);
    }

    @RequestMapping(value = "/admin/trashList", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO adminGetTrashList(@RequestHeader String token) throws ServiceException {
        List<UserDTO> userDTOs = letterService.adminGetTrashList();
        return new ResponseDTO<>(userDTOs);
    }

    @RequestMapping(value = "/admin/confirmTrashed", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO adminConfirmLetterTrashed(@RequestHeader String token,
                                                 @RequestParam String letterId) throws ServiceException, AuthenticationException {
        letterService.adminConfirmTrashed(token, UUID.fromString(letterId));
        return new ResponseDTO<>(new MessageSuccessDTO());
    }

    @RequestMapping(value = "/customer/letters", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO customerGetLetterList(@RequestHeader String token,
                                             @RequestParam(required = false) String userId) throws ServiceException, AuthenticationException {
        CustomerLettersDTO letters = letterService.customerGetLetterList(token, userId);
        return new ResponseDTO<>(letters);
    }


    @RequestMapping(value = "/letterDetail", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO getLetterDetail(@RequestHeader String token,
                                       @RequestParam String letterId) throws ServiceException {
        LetterDTO letterDTO = letterService.getLetterDetail(UUID.fromString(letterId));
        return new ResponseDTO<>(letterDTO);
    }

    @RequestMapping(value = "/letter/getOrder", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO getOrderFromLetter(@RequestHeader String token,
                                       @RequestParam String letterId) throws ServiceException {
        SendOrderDTO sendOrderDTO = letterService.getOrderFromLetter(UUID.fromString(letterId));
        return new ResponseDTO<>(sendOrderDTO);
    }

    @RequestMapping(value = "/customer/requestScan", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO customerRequestScan(@RequestHeader String token,
                                           @RequestParam String letterIds) throws ServiceException {
        letterService.customerRequestScan(letterIds);
        return new ResponseDTO<>(new MessageSuccessDTO());
    }

    @RequestMapping(value = "/customer/requestTrash", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO customerRequestTrash(@RequestHeader String token,
                                            @RequestParam String letterIds) throws ServiceException {
        letterService.customerRequestTrash(letterIds);
//        sendOrderService.checkOrderEmpty();
        return new ResponseDTO<>(new MessageSuccessDTO());
    }

    @RequestMapping(value = "/admin/scanLetter", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO adminScanLetter(@RequestHeader String token,
                                       @RequestParam String letterId,
                                       @RequestParam MultipartFile scanFile) throws ServiceException, AuthenticationException {
        letterService.adminScanLetter(token, UUID.fromString(letterId), scanFile);
        return new ResponseDTO<>(new MessageSuccessDTO());
    }

    @RequestMapping(value = "/validateQrcode", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO validateQrcode(@RequestHeader String token,
                                      @RequestParam String qrCode) throws ServiceException {
        return new ResponseDTO<>(new ValidateQrCodeDTO(letterService.validateQrcode(qrCode)));
    }

    @RequestMapping(value = "/findLetterByQrCode", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO findLetterByQrCode(@RequestHeader String token,
                                          @RequestParam String qrCode) throws ServiceException {
        return new ResponseDTO<>(letterService.findLetterByQrCode(qrCode));
    }

    @RequestMapping(value = "/customer/restoreFromTrash", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO customerRestoreFromTrash(@RequestHeader String token,
                                                @RequestParam String letterIds) throws ServiceException {
        letterService.customerRestoreFromTrash(letterIds);
        return new ResponseDTO<>(new MessageSuccessDTO());
    }

    @RequestMapping(value = "/customer/editLetter", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO customerEditLetter(@RequestHeader String token,
                                          @RequestParam String letterId,
                                          @RequestParam(required = false) String title,
                                          @RequestParam(required = false) String userNote,
                                          @RequestParam(required = false) String letterFrom,
                                          @RequestParam(required = false) String letterTo) throws ServiceException, AuthenticationException {
        letterService.customerEditLetter(token, letterId, title, userNote, letterFrom, letterTo);
        return new ResponseDTO<>(new MessageSuccessDTO());
    }

    @RequestMapping(value = "/customer/requestDestroy", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO customerRequestDestroy(@RequestHeader String token,
                                              @RequestParam String letterId) throws ServiceException, AuthenticationException {
        letterService.customerRequestDestroy(token, letterId);
        return new ResponseDTO<>(new MessageSuccessDTO());
    }

    @RequestMapping(value = "/customer/saveToDocument", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO customerSaveToDocument(@RequestHeader String token,
                                              @RequestParam String letterIds) throws ServiceException, AuthenticationException {
        letterService.customerSaveToDocument(token, letterIds);
        return new ResponseDTO<>(new MessageSuccessDTO());
    }

    @RequestMapping(value = "/customer/removeFromDocument", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO customerRemoveFromDocument(@RequestParam String letterIds) throws ServiceException {
        letterService.customerRemoveFromDocument(letterIds);
        return new ResponseDTO<>(new MessageSuccessDTO());
    }
}
