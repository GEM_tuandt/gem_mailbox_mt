package com.gem.mailbox.controller;

import com.gem.mailbox.dto.MessageSuccessDTO;
import com.gem.mailbox.dto.NotificationDTO;
import com.gem.mailbox.dto.ResponseDTO;
import com.gem.mailbox.dto.ServiceProviderDTO;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.service.NotificationService;
import com.gem.mailbox.service.ServiceProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Notification controller
 */
@Controller
public class NotificationController {
    @Autowired
    NotificationService notificationService;

    @RequestMapping(value = "/notification/list", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO getNotifications(@RequestHeader String token,
                                        @RequestParam Integer pageIndex, @RequestParam Integer pageSize) throws ServiceException, AuthenticationException {
        List<NotificationDTO> notifications = notificationService.getNotifications(token, pageIndex, pageSize);
        return new ResponseDTO<>(notifications);
    }

    @RequestMapping(value = "/notification/remove", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO removeNotifications(@RequestHeader String token,
                                        @RequestParam String uuids) throws ServiceException, AuthenticationException {
        notificationService.remove(token, uuids);
        return new ResponseDTO<>(new MessageSuccessDTO());
    }
}

