package com.gem.mailbox.controller;

import com.gem.mailbox.dto.LetterScanDTO;
import com.gem.mailbox.dto.ResponseDTO;
import com.gem.mailbox.dto.MessageSuccessDTO;
import com.gem.mailbox.exception.AuthenticationException;
import com.gem.mailbox.exception.ServiceException;
import com.gem.mailbox.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 2/26/15
 * Time: 3:12 PM
 */
@Controller
public class DocumentController {
    @Autowired
    DocumentService documentService;


    @RequestMapping(value = "/document/{filePath}/", method = RequestMethod.GET, produces = {"image/jpeg", "image/jpg", "image/png", "image/gif"})
    @ResponseBody
    public byte[] getDocument(@PathVariable("filePath") String filePath) throws ServiceException, IOException {
        return documentService.getDocumentByFilePath(filePath);
    }

    @RequestMapping(value = "/document/pdf/{filePath}/", method = RequestMethod.GET, produces = {"application/pdf"})
    @ResponseBody
    public byte[] getPdf(@PathVariable("filePath") String filePath) throws ServiceException, IOException {
        return documentService.getDocumentByFilePath(filePath);
    }

    @RequestMapping(value = "/document/uploadEntityImage", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO uploadEntityImage(@RequestParam String token, @RequestParam MultipartFile file, @RequestParam String entityId, @RequestParam String documentId) throws ServiceException {
        LetterScanDTO letterScanDTO = documentService.uploadDocument(token, file, UUID.fromString(entityId), UUID.fromString(documentId));
        return new ResponseDTO<>(letterScanDTO);
    }

    @RequestMapping(value = "/document/uploadProfileImage", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO uploadProfileImage(@RequestParam String token, @RequestParam MultipartFile file, @RequestParam String profileId, @RequestParam String documentId) throws ServiceException {
//        LetterScanDTO photoDTO = profileService.addAvatar(token, UUID.fromString(profileId), UUID.fromString(documentId), file);
//        return new ResponseDTO(photoDTO);
        // TODO fix
        return new ResponseDTO();
    }

    @RequestMapping(value = "/document/download", method = RequestMethod.GET, consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    @ResponseBody
    public ResponseDTO download(@RequestParam String documentId, @RequestParam(required = false, defaultValue = "true") Boolean isThumb) throws ServiceException {
        LetterScanDTO letterScanDTO = documentService.download(documentId, isThumb);
        return new ResponseDTO<>(letterScanDTO);
    }

    @RequestMapping(value = "/document/deleteImage", method = RequestMethod.GET, consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    @ResponseBody
    public ResponseDTO deleteImage(@RequestParam String token, @RequestParam String documentId) throws ServiceException {
        documentService.deleteImage(token, UUID.fromString(documentId));
        return new ResponseDTO<>(new MessageSuccessDTO());
    }

    @RequestMapping(value = "/document/getAllImageMetadata", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    @ResponseBody
    public ResponseDTO getAllImageMetadata(@RequestParam String token, @RequestBody Map<String, List<LetterScanDTO>> photoDTOMap) throws ServiceException {
        List<LetterScanDTO> letterScanDTOList = photoDTOMap.get("photo");
        return documentService.getAllImageMetadata(token, letterScanDTOList);
    }
}
