package com.gem.mailbox.model;

import com.gem.mailbox.model.constant.DeviceType;
import com.gem.mailbox.model.constant.JobTarget;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 2/12/15
 * Time: 9:06 AM
 */
@Entity
@Table(name = "user_session", indexes = {
        @Index(name = "user_session_index", columnList = "uuid, date_updated")
})
public class Session extends BaseEntity {
    public static int DATE_EXPIRE = 90; // 90 days to expire

    @Column(name = "device_token")
    private String deviceToken;

    @Column(name = "device_type")
    @Enumerated(EnumType.STRING)
    private DeviceType deviceType;

    @Column(name = "token", unique = true)
    private String token;

    @Column(name = "token_expire_on")
    private Date tokenExpireOn;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @LazyToOne(LazyToOneOption.PROXY)
    @Fetch(FetchMode.SELECT)
    private User user;

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getTokenExpireOn() {
        return tokenExpireOn;
    }

    public void setTokenExpireOn(Date tokenExpireOn) {
        this.tokenExpireOn = tokenExpireOn;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    @PrePersist
    public void onSave() {
        if (token != null && tokenExpireOn == null) {
            Calendar cal = Calendar.getInstance(); // creates calendar
            cal.setTime(new Date()); // sets calendar time/date
            cal.add(Calendar.DATE, DATE_EXPIRE); // adds 24 hour
            tokenExpireOn = cal.getTime(); // returns new date object, one hour in the future
        }
    }

    @PreUpdate
    public void onUpdate() {
        Calendar cal = Calendar.getInstance(); // creates calendar
        cal.setTime(new Date()); // sets calendar time/date
        cal.add(Calendar.DATE, DATE_EXPIRE); // adds 24 hour
        tokenExpireOn = cal.getTime(); // returns new date object, one hour in the future
    }
}
