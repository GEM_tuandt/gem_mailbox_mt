package com.gem.mailbox.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * Service provider
 * Created by neo on 2/2/2016.
 */
@Entity
@Table(name = "service_provider", indexes = {
        @Index(name = "service_provider_index", columnList = "uuid, date_updated")
})
public class ServiceProvider extends BaseEntity {
    @Column(name = "provider_name")
    private String providerName;

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }
}
