package com.gem.mailbox.model;

import com.gem.mailbox.model.constant.JobAction;
import com.gem.mailbox.model.constant.JobTarget;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import java.util.Date;
import java.util.UUID;

/**
 * Service provider
 * Created by neo on 2/2/2016.
 */
@Entity
@Table(name = "notification", indexes = {
        @Index(name = "notification_index", columnList = "uuid, date_updated")
})
public class Notification extends BaseEntity {
    @Column(name = "job_action")
    @Enumerated(EnumType.ORDINAL)
    private JobAction jobAction;

    @Column(name = "job_target")
    @Enumerated(EnumType.ORDINAL)
    private JobTarget jobTarget;

    @Column(name = "target_id", columnDefinition = "varchar(36)")
    @Type(type = "uuid-char")
    private UUID targetId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @LazyToOne(LazyToOneOption.PROXY)
    @Fetch(FetchMode.SELECT)
    private User user;

    @Column(name = "action_date")
    private Date actionDate;

    public JobAction getJobAction() {
        return jobAction;
    }

    public void setJobAction(JobAction jobAction) {
        this.jobAction = jobAction;
    }

    public JobTarget getJobTarget() {
        return jobTarget;
    }

    public void setJobTarget(JobTarget jobTarget) {
        this.jobTarget = jobTarget;
    }

    public UUID getTargetId() {
        return targetId;
    }

    public void setTargetId(UUID targetId) {
        this.targetId = targetId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getActionDate() {
        return actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }
}
