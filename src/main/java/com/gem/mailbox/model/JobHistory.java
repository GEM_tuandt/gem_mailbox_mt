package com.gem.mailbox.model;

import com.gem.mailbox.model.constant.JobAction;
import com.gem.mailbox.model.constant.JobTarget;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import java.util.Date;
import java.util.UUID;

/**
 * Service provider
 * Created by neo on 2/2/2016.
 */
@Entity
@Table(name = "job_history", indexes = {
        @Index(name = "job_history_index", columnList = "uuid, date_updated")
})
public class JobHistory extends BaseEntity {
    @Column(name = "job_action")
    @Enumerated(EnumType.ORDINAL)
    private JobAction jobAction;

    @Column(name = "target_id", columnDefinition = "varchar(36)")
    @Type(type = "uuid-char")
    private UUID targetId;

    @Column(name = "target_type")
    @Enumerated(EnumType.ORDINAL)
    private JobTarget targetType;

    @Column(name = "action_date")
    private Date actionDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @LazyToOne(LazyToOneOption.PROXY)
    @Fetch(FetchMode.SELECT)
    private User user;

    public JobHistory() {
        this.actionDate = new Date();
    }

    public JobHistory(User user, JobAction jobAction, UUID targetId, JobTarget targetType) {
        this.user = user;
        this.actionDate = new Date();
        this.jobAction = jobAction;
        this.targetId = targetId;
        this.targetType = targetType;
    }

    public Date getActionDate() {
        return actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    public JobAction getJobAction() {
        return jobAction;
    }

    public void setJobAction(JobAction jobAction) {
        this.jobAction = jobAction;
    }

    public UUID getTargetId() {
        return targetId;
    }

    public void setTargetId(UUID targetId) {
        this.targetId = targetId;
    }

    public JobTarget getTargetType() {
        return targetType;
    }

    public void setTargetType(JobTarget targetType) {
        this.targetType = targetType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
