package com.gem.mailbox.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * Base for entities
 * Created by qsoft on 10/22/14.
 */
@MappedSuperclass
public class BaseEntity implements Serializable {
    @Id
    @Column(name = "uuid", unique = true, columnDefinition = "varchar(36)")
    @Type(type = "uuid-char")
    @JsonProperty("hiddenUUID")
    private UUID uuid = UUID.randomUUID();
    @Version
    private Integer version;

    @JsonProperty("uuid")
    @Transient
    private UUID visibleUUID;

    @Column(name = "date_created")
    @JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
    private Date dateCreated;

    @Column(name = "date_updated")
    @JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
    private Date dateUpdated;

    @PostLoad
    public void onBaseAfterLoad() {
        visibleUUID = uuid;
    }

    @PrePersist
    public void onBaseSave() {
        if (uuid == null) {
            uuid = UUID.randomUUID();
        }
        if (dateCreated == null) {
            dateCreated = new Date();
        }
        dateUpdated = new Date();
    }

    @PreUpdate
    public void onBaseUpdate() {
        this.setDateUpdated(new Date());
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public UUID getVisibleUUID() {
        return visibleUUID;
    }

    public void setVisibleUUID(UUID visibleUUID) {
        this.visibleUUID = visibleUUID;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
