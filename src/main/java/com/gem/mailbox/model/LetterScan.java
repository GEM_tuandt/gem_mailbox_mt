package com.gem.mailbox.model;

import com.gem.mailbox.model.constant.ImageType;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * Created with IntelliJ IDEA.
 * User: neo
 */
@Entity
@Table(name = "letter_scan", indexes = {
        @Index(name = "letter_scan_index", columnList = "uuid, date_updated")
})
public class LetterScan extends BaseEntity {
    @Column(name = "index")
    private Integer index;

    @Column(name = "name")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "letter_id", nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    @Fetch(FetchMode.SELECT)
    private Letter letter;

    @Column(name = "image_type")
    @Enumerated(EnumType.ORDINAL)
    private ImageType imageType = ImageType.NORMAL_PHOTO;

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ImageType getImageType() {
        return imageType;
    }

    public void setImageType(ImageType imageType) {
        this.imageType = imageType;
    }

    public Letter getLetter() {
        return letter;
    }

    public void setLetter(Letter letter) {
        this.letter = letter;
    }
}
