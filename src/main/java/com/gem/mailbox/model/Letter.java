package com.gem.mailbox.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;

import javax.persistence.*;
import java.util.Date;

/**
 * Letter entity
 * Created by qsoft on 10/22/14.
 */
@Entity
@Table(name = "letter", indexes = {
        @Index(name = "letter_index", columnList = "uuid, date_updated")
})
public class Letter extends BaseEntity {
    // ------------------------------ FIELDS ------------------------------
    // Thumbnail of letter
    @Column(name = "thumb_url")
    private String thumbUrl;

    // Note of letter
    @Column(name = "note")
    private String note;

    // Date that admin created letter
    @Column(name = "date")
    @JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
    private Date date;

    // Owner of letter
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    @Fetch(FetchMode.SELECT)
    private User user;

    // QR Code using for management
    @Column(name = "qr_code", unique = true)
    private String qrCode;

    // Is new letter: true = new, false = read
    @Column(name = "is_new")
    private Boolean isNew = true;

    // Request trash: null = not request, false = requested letterTo destroy, true = request completed
    @Column(name = "is_trash")
    private Boolean isTrash;

    // Request send: null = not request, false = requested letterTo send letter, true = letter is sent
    @Column(name = "is_send")
    private Boolean isSend;

    // Request scan: null = not request, false = requested letterTo scan letter, true = letter is scanned
    @Column(name = "is_scan")
    private Boolean isScan;

    @Column(name = "is_in_document")
    private Boolean isInDocument = false;

    @Column(name = "date_trash")
    @JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
    private Date dateTrash;

    @Column(name = "date_scan")
    @JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
    private Date dateScan;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "send_order_id")
    @LazyToOne(LazyToOneOption.PROXY)
    @Fetch(FetchMode.SELECT)
    private SendOrder sendOrder;

    // Title of letter
    @Column(name = "title")
    private String title;

    // From
    @Column(name = "letterFrom")
    private String letterFrom;

    // To
    @Column(name = "letterTo")
    private String letterTo;

    // User note
    @Column(name = "user_note")
    private String userNote;

// --------------------- GETTER / SETTER METHODS ---------------------

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public Boolean getIsNew() {
        return isNew;
    }

    public void setIsNew(Boolean isNew) {
        this.isNew = isNew;
    }

    public Boolean getIsTrash() {
        return isTrash;
    }

    public void setIsTrash(Boolean trash) {
        isTrash = trash;
    }

    public Boolean getIsSend() {
        return isSend;
    }

    public void setIsSend(Boolean isSend) {
        this.isSend = isSend;
    }

    public Boolean getIsScan() {
        return isScan;
    }

    public void setIsScan(Boolean isScan) {
        this.isScan = isScan;
    }

    public Date getDateTrash() {
        return dateTrash;
    }

    public void setDateTrash(Date dateTrash) {
        this.dateTrash = dateTrash;
    }

    public SendOrder getSendOrder() {
        return sendOrder;
    }

    public void setSendOrder(SendOrder sendOrder) {
        this.sendOrder = sendOrder;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLetterFrom() {
        return letterFrom;
    }

    public void setLetterFrom(String letterFrom) {
        this.letterFrom = letterFrom;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLetterTo() {
        return letterTo;
    }

    public void setLetterTo(String letterTo) {
        this.letterTo = letterTo;
    }

    public String getUserNote() {
        return userNote;
    }

    public void setUserNote(String userNote) {
        this.userNote = userNote;
    }

    public Date getDateScan() {
        return dateScan;
    }

    public void setDateScan(Date dateScan) {
        this.dateScan = dateScan;
    }

    public Boolean getIsInDocument() {
        return isInDocument;
    }

    public void setIsInDocument(Boolean isInDocument) {
        this.isInDocument = isInDocument;
    }
}
