package com.gem.mailbox.model.constant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Actions of job that Admin had been done
 * Created by neo on 2/26/2016.
 */
public enum JobAction {
    ADD, SCAN, SEND, DESTROY;


    @JsonCreator
    public static JobAction forValue(int ordinal) {
        return JobAction.values()[ordinal];
    }

    @JsonValue
    public int toValue() {
        return this.ordinal();
    }
}
