package com.gem.mailbox.model.constant;

/**
 * Created by quynhtq on 7/13/2015.
 */
public enum ImageType {
    NORMAL_PHOTO,
    PROFILE_AVATAR,
    INSURANCE_FRONT,
    INSURANCE_BACK
}
