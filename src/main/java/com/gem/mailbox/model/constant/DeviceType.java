package com.gem.mailbox.model.constant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Types of device
 * Created by neo on 3/2/2016.
 */
public enum DeviceType {
    ANDROID, IOS;

    @JsonCreator
    public static DeviceType forValue(String name) {
        return DeviceType.valueOf(name);
    }

    @JsonValue
    public String toValue() {
        return this.name();
    }
}
