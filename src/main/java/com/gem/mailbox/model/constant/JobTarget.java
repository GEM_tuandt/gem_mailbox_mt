package com.gem.mailbox.model.constant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Target of job action: Letter or order
 * Created by neo on 2/29/2016.
 */
public enum JobTarget {
    LETTER, ORDER;

    @JsonCreator
    public static JobAction forValue(int ordinal) {
        return JobAction.values()[ordinal];
    }

    @JsonValue
    public int toValue() {
        return this.ordinal();
    }
}
