package com.gem.mailbox.model.constant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Types of user
 * Created by neo on 4/11/2016.
 */
public enum UserType {
    ADMIN, USER;

    @JsonCreator
    public static UserType forValue(int ordinal) {
        return UserType.values()[ordinal];
    }

    @JsonValue
    public int toValue() {
        return this.ordinal();
    }
}
