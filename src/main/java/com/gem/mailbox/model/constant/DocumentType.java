package com.gem.mailbox.model.constant;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 2/24/15
 * Time: 9:48 AM
 */
@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum DocumentType
{
    JPG,
    JPEG,
    PNG,
    GIF,
    PDF,
    DOC,
    DOCX
}
