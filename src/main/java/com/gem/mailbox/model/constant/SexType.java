package com.gem.mailbox.model.constant;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 2/12/15
 * Time: 11:06 AM
 */
@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum SexType
{
    MALE,
    FEMALE
}
