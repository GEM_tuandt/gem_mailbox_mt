package com.gem.mailbox.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Order send letters
 * Created by neo on 2/2/2016.
 */
@Entity
@Table(name = "send_order", indexes = {
        @Index(name = "send_order_index", columnList = "uuid, date_updated")
})
public class SendOrder extends BaseEntity {
    @Column(name = "require_date")
    private Long requireDate;

    @Column(name = "send_address")
    private String sendAddress;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_provider_id", nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    @Fetch(FetchMode.SELECT)
    private ServiceProvider serviceProvider;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    @Fetch(FetchMode.SELECT)
    private User user;

    @Column(name = "send_date")
    @JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
    private Date sendDate;

    @Column(name = "note")
    private String note;

    @Column(name = "price")
    private Float price;

    @Column(name = "is_sent")
    private Boolean isSent = false;

    @Column(name = "tracking_code")
    private String trackingCode;

    @Column(name = "is_arrived")
    private Boolean isArrived = false;

//    @OneToMany(mappedBy = "sendOrder", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    private List<Letter> letters;

    public Long getRequireDate() {
        return requireDate;
    }

    public void setRequireDate(Long requireDate) {
        this.requireDate = requireDate;
    }

    public String getSendAddress() {
        return sendAddress;
    }

    public void setSendAddress(String sendAddress) {
        this.sendAddress = sendAddress;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public ServiceProvider getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(ServiceProvider serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

//    public List<Letter> getLetters() {
//        return letters;
//    }

//    public void setLetters(List<Letter> letters) {
//        this.letters = letters;
//    }

    public Boolean getIsSent() {
        return isSent;
    }

    public void setIsSent(Boolean isSent) {
        this.isSent = isSent;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTrackingCode() {
        return trackingCode;
    }

    public void setTrackingCode(String trackingCode) {
        this.trackingCode = trackingCode;
    }

    public Boolean getIsArrived() {
        return isArrived;
    }

    public void setIsArrived(Boolean isArrived) {
        this.isArrived = isArrived;
    }
}
