package com.gem.mailbox.config;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 3/12/15
 * Time: 9:41 AM
 */
public class ConfigConstants {
    public static final String DEFAULT_DB_ENCRYPTOR_MASTER_KEY = "zQs2PVsrsULrAqnj";
    public static final String DEFAULT_DB_ENCRYPTOR_NAME = "DB_ENCRYPTOR";

    public static final String DEFAULT_PROPERTY_ENCRYPTOR_MASTER_KEY = "1e3q2wadS";
    public static final String ADMINISTRATOR = "administrator";
    public static final String CONTACT = "contact";
    public static final String PROFILE = "profile";
    public static final String COMMUNICATION_DETAIL = "communicationDetail";
    public static final String OCULAR = "ocular";
    public static final String SUPPLIER = "supplier";
    public static final String SUPPLIER_RECORD = "supplierRecord";
    public static final String INSURANCE = "insurance";
    public static final String VACCINATION = "vaccination";
    public static final String EYE_PRESCRIPTION = "eyePrescription";
    public static final String MEDICATION = "medication";
    public static final String ALLERGY_SENSITIVITY = "allergySensitivity";
    public static final String ALLERGY_SENSITIVITY_MEDICATION = "allergySensitivityMedication";
    public static final String ADDRESS = "address";
    public static final String EMERGENCY = "emergency";
    public static final String PHOTO = "photo";
}
