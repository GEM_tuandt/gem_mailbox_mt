package com.gem.mailbox.config;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.ReconnectPolicy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.io.IOException;

/**
 * Configurations for push notification to iOS devices
 * Created by neo on 3/2/16.
 */
@Configuration
@EnableAsync
public class NotificationConfig {
    @Value("${notification.file.path}")
    private String filePath;

    @Value("${notification.authenticate.password}")
    private String password;

    @Bean
    public ApnsService apnsService() throws IOException {
        ClassPathResource cert
                = new ClassPathResource("Certificates_mailbox_adhoc_09052016.p12");

        return APNS.newService()
                .withCert(cert.getInputStream(), password)
                .withReconnectPolicy(ReconnectPolicy.Provided.EVERY_NOTIFICATION)
//                .withSandboxDestination()
                .withProductionDestination()
                .asPool(25)
                .build();
    }

    @Bean
    public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(5);
        taskExecutor.setMaxPoolSize(10);
        taskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        return taskExecutor;
    }
}
