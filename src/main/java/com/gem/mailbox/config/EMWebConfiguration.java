package com.gem.mailbox.config;


import com.gem.mailbox.converter.DateConverter;
import com.gem.mailbox.interceptor.RequestProcessingTimeInterceptor;
import com.gem.mailbox.interceptor.TokenCheckInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class EMWebConfiguration extends WebMvcConfigurerAdapter {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(tokenCheckInterceptor());
        registry.addInterceptor(requestProcessingTimeInterceptor());
    }

    @Bean
    public TokenCheckInterceptor tokenCheckInterceptor() {
        return new TokenCheckInterceptor();
    }

    @Bean
    public RequestProcessingTimeInterceptor requestProcessingTimeInterceptor() {
        return new RequestProcessingTimeInterceptor();
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addFormatter(dateConverter());
    }

    @Bean
    public DateConverter dateConverter() {
        return new DateConverter();
    }
}
