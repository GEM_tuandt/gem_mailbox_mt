package com.gem.mailbox.config;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.kms.AWSKMSClient;
import com.amazonaws.services.s3.AmazonS3Client;
import com.gem.mailbox.utils.JasyptSecureUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 2/26/15
 * Time: 2:27 PM
 */
@Configuration
public class AmazonConfiguration
{
    private final String ACCESS_KEY = "aws.access.key";
    private final String SECRET_KEY = "aws.secret.key";

    @Bean
    public AWSCredentials awsCredentials() throws IOException
    {
        return new BasicAWSCredentials(JasyptSecureUtils.getSecuredValue(ACCESS_KEY), JasyptSecureUtils.getSecuredValue(SECRET_KEY));
    }

    @Bean
    public AmazonS3Client amazonS3Client() throws IOException
    {
        AmazonS3Client amazonS3Client = new AmazonS3Client(awsCredentials());
        return amazonS3Client;
    }

    @Bean
    public AWSKMSClient awskmsClient() throws Exception
    {
        AWSKMSClient awskmsClient = new AWSKMSClient(awsCredentials());
        return awskmsClient;
    }
}
