package com.gem.mailbox.config;

import com.gem.mailbox.interceptor.RequestProcessingTimeInterceptor;
import com.gem.mailbox.interceptor.TokenCheckInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class EMRepositoryRestMvcConfiguration extends RepositoryRestMvcConfiguration {
    @Override
    public RequestMappingHandlerMapping repositoryExporterHandlerMapping() {
        RequestMappingHandlerMapping mapping = super.repositoryExporterHandlerMapping();

        mapping.setInterceptors(new Object[]{tokenCheckInterceptor(), requestProcessingTimeInterceptor()});
        return mapping;
    }

    @Bean
    public TokenCheckInterceptor tokenCheckInterceptor() {
        return new TokenCheckInterceptor();
    }

    @Bean
    public RequestProcessingTimeInterceptor requestProcessingTimeInterceptor() {
        return new RequestProcessingTimeInterceptor();
    }

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        List<HttpMessageConverter<?>> list = new ArrayList<>();
        list.add(new MappingJackson2HttpMessageConverter());
        restTemplate.setMessageConverters(list);

        return restTemplate;
    }
}

