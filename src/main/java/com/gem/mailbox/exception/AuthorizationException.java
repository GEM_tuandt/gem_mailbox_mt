package com.gem.mailbox.exception;

import org.springframework.http.HttpStatus;

/**
 * [File description]
 *
 * @author lent
 * @version 1.0
 * @since 4/29/14
 */
public class AuthorizationException extends Exception
{
// ------------------------------ FIELDS ------------------------------

    // TODO: suport i18n, resource string instead
    private String message;
    private HttpStatus httpStatusCode;
    private Throwable cause;
    private StackTraceElement[] stackTraceElements;

// --------------------------- CONSTRUCTORS ---------------------------

    public AuthorizationException(String message)
    {
        this(message, HttpStatus.UNAUTHORIZED, null);
    }

    public AuthorizationException(String message, HttpStatus errorCode)
    {
        this(message, errorCode, null);
    }

    public AuthorizationException(String message, Throwable cause)
    {
        this(message, HttpStatus.UNAUTHORIZED, cause);
    }

    public AuthorizationException(String message, HttpStatus errorCode, Throwable cause)
    {
        this.message = message;
        this.cause = cause;
        this.httpStatusCode = errorCode;
        if (cause != null)
        {
            this.stackTraceElements = cause.getStackTrace();
        }
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    @Override
    public Throwable getCause()
    {
        return cause;
    }

    public HttpStatus getHttpStatusCode()
    {
        return httpStatusCode;
    }

    @Override
    public String getMessage()
    {
        return this.message;
    }

    public StackTraceElement[] getStackTraceElements()
    {
        return stackTraceElements;
    }
}
