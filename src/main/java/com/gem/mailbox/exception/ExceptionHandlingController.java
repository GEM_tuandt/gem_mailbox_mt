package com.gem.mailbox.exception;

import com.gem.mailbox.dto.ErrorDTO;
import com.gem.mailbox.dto.ResponseDTO;
import org.hibernate.HibernateException;
import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * Central exception handling controller.
 * + ServiceException show
 */
@ControllerAdvice
public class ExceptionHandlingController {
// ------------------------------ FIELDS ------------------------------

    public static final Logger logger = LoggerFactory.getLogger(ExceptionHandlingController.class);

// -------------------------- OTHER METHODS --------------------------

    @ExceptionHandler({AuthenticationException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public ResponseDTO authenticationError(AuthenticationException ex,
                                            HttpServletRequest request,
                                            HttpServletResponse response) {
        Map<String, Object> errorMap = new HashMap<String, Object>();
        errorMap.put(ErrorDTO.Properties.STATUS_CODE, ex.getHttpStatusCode().value());
        errorMap.put(ErrorDTO.Properties.STATUS_DESCRIPTION, ex.getHttpStatusCode().getReasonPhrase());
        errorMap.put(ErrorDTO.Properties.ERROR_MESSAGE, ex.getMessage());

        MappingJackson2JsonView jsonView = new MappingJackson2JsonView();
//        return new ModelAndView(jsonView, errorMap);
        return ResponseDTO.newErrorInstance(HttpStatus.UNAUTHORIZED, ex.getMessage());
    }

    @ExceptionHandler({AuthorizationException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public ResponseDTO authorizationError(AuthorizationException ex,
                                           HttpServletRequest request,
                                           HttpServletResponse response) {
        Map<String, Object> errorMap = new HashMap<String, Object>();
        errorMap.put(ErrorDTO.Properties.STATUS_CODE, ex.getHttpStatusCode().value());
        errorMap.put(ErrorDTO.Properties.STATUS_DESCRIPTION, ex.getHttpStatusCode().getReasonPhrase());
        errorMap.put(ErrorDTO.Properties.ERROR_MESSAGE, ex.getMessage());

        MappingJackson2JsonView jsonView = new MappingJackson2JsonView();
//        return new ModelAndView(jsonView, errorMap);
        return ResponseDTO.newErrorInstance(HttpStatus.UNAUTHORIZED, ex.getMessage());
    }

    @ExceptionHandler(BindException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    private ResponseDTO handlingBindException(BindException ex) {
        Map<String, Object> errorMap = new HashMap<String, Object>();
        errorMap.put(ErrorDTO.Properties.STATUS_CODE, HttpStatus.BAD_REQUEST);
        errorMap.put(ErrorDTO.Properties.STATUS_DESCRIPTION, HttpStatus.BAD_REQUEST.getReasonPhrase());

        String errorMessage = "";
        for (ObjectError objectError : ex.getAllErrors()) {
            errorMessage += objectError.getDefaultMessage() + "\n";
        }
        if (errorMessage.length() > 0) {
            errorMessage = errorMessage.substring(0, errorMessage.length() - 1);
        }
        errorMap.put(ErrorDTO.Properties.ERROR_MESSAGE, errorMessage);

        MappingJackson2JsonView jsonView = new MappingJackson2JsonView();
//        return new ModelAndView(jsonView, errorMap);
        return ResponseDTO.newErrorInstance(HttpStatus.BAD_REQUEST, ex.getMessage());
    }

    @ExceptionHandler(ValidateException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    private ResponseDTO handlingValidateException(ValidateException ex) {
        logger.error("=====Error====", ex);
        Map<String, Object> errorMap = new HashMap<String, Object>();
        errorMap.put(ErrorDTO.Properties.STATUS_CODE, HttpStatus.BAD_REQUEST);
        errorMap.put(ErrorDTO.Properties.STATUS_DESCRIPTION, HttpStatus.BAD_REQUEST.getReasonPhrase());
        errorMap.put(ErrorDTO.Properties.ERROR_MESSAGE, ex.getMessage());

        MappingJackson2JsonView jsonView = new MappingJackson2JsonView();
//        return new ModelAndView(jsonView, errorMap);
        return ResponseDTO.newErrorInstance(HttpStatus.BAD_REQUEST, ex.getMessage());
    }

    @ExceptionHandler({PSQLException.class, HibernateException.class, DataIntegrityViolationException.class,
            DataAccessResourceFailureException.class, DataAccessException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    private ResponseDTO sqlError(Exception ex) {
        logger.error("=====Error====", ex);
        Map<String, Object> errorMap = new HashMap<String, Object>();
        errorMap.put(ErrorDTO.Properties.STATUS_CODE, HttpStatus.INTERNAL_SERVER_ERROR);
        errorMap.put(ErrorDTO.Properties.STATUS_DESCRIPTION, HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        errorMap.put(ErrorDTO.Properties.ERROR_MESSAGE, "SQL Execute Error");

        MappingJackson2JsonView jsonView = new MappingJackson2JsonView();
//        return new ModelAndView(jsonView, errorMap);
        return ResponseDTO.newErrorInstance(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
    }

    @ExceptionHandler({ServiceException.class})
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ResponseDTO serviceError(ServiceException ex,
                                     HttpServletRequest request,
                                     HttpServletResponse response) {
        Map<String, Object> errorMap = new HashMap<String, Object>();
        errorMap.put(ErrorDTO.Properties.STATUS_CODE, ex.getHttpStatusCode().value());
        errorMap.put(ErrorDTO.Properties.STATUS_DESCRIPTION, ex.getHttpStatusCode().getReasonPhrase());
        errorMap.put(ErrorDTO.Properties.ERROR_MESSAGE, ex.getMessage());
        errorMap.put(ErrorDTO.Properties.ADDITION_INFO, ex.getAdditionInfo());
        errorMap.put(ErrorDTO.Properties.OBJECT, ex.getObject());
        logger.error("=====Error====", ex);

        MappingJackson2JsonView jsonView = new MappingJackson2JsonView();
//        return new ModelAndView(jsonView, errorMap);
        return ResponseDTO.newErrorInstance(ex.getHttpStatusCode(), ex.getMessage());
    }

    @ExceptionHandler({Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ResponseDTO serviceError(Exception ex,
                                     HttpServletRequest request,
                                     HttpServletResponse response) {
        logger.error("=====Error====", ex);
        Map<String, Object> errorMap = new HashMap<String, Object>();
        errorMap.put(ErrorDTO.Properties.STATUS_CODE, HttpStatus.INTERNAL_SERVER_ERROR.value());
        errorMap.put(ErrorDTO.Properties.STATUS_DESCRIPTION, HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        errorMap.put(ErrorDTO.Properties.ERROR_MESSAGE, ex.getMessage() != null ? ex.getMessage() : ex.toString());

        MappingJackson2JsonView jsonView = new MappingJackson2JsonView();
//        return new ModelAndView(jsonView, errorMap);

        return ResponseDTO.newErrorInstance(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
    }

//    @ResponseStatus(value = HttpStatus.CONFLICT, reason = "Data integrity violation")  // 409
//    @ExceptionHandler(DataIntegrityViolationException.class)
//    public void conflict()
//    {
//        // Nothing to do
//    }

    // Specify the name of a specific view that will be used to display the error:
//    @ExceptionHandler({SQLException.class, DataAccessException.class})
//    public String databaseError()
//    {
//        // Nothing to do.  Returns the logical view name of an error page, passed to
//        // the view-resolver(s) in usual way.
//        // Note that the exception is _not_ available to this view (it is not added to
//        // the model) but see "Extending ExceptionHandlerExceptionResolver" below.
//        return "databaseError";
//    }

    // Total control - setup a model and return the view name yourself. Or consider
    // subclassing ExceptionHandlerExceptionResolver (see below).
//    @ExceptionHandler(Exception.class)
//    public ModelAndView handleError(HttpServletRequest req, Exception exception)
//    {
//        logger.error("Request: " + req.getRequestURL() + " raised " + exception);
//
//        ModelAndView mav = new ModelAndView();
//        mav.addObject("exception", exception);
//        mav.addObject("url", req.getRequestURL());
//        mav.setViewName("error");
//        return mav;
//    }
}
