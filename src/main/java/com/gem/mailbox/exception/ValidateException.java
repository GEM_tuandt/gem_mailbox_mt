package com.gem.mailbox.exception;

import org.springframework.http.HttpStatus;

/**
 * User: tienhd
 * Date: 4/19/14
 * Time: 2:51 PM
 */
public class ValidateException extends Exception
{
    // TODO: suport i18n, resource string instead
    private String message;
    private HttpStatus httpStatusCode;
    private Throwable cause;
    private StackTraceElement[] stackTraceElements;

// --------------------------- CONSTRUCTORS ---------------------------

    public ValidateException(String message)
    {
        this(message, HttpStatus.BAD_REQUEST, null);
    }

    public ValidateException(String message, HttpStatus errorCode)
    {
        this(message, errorCode, null);
    }

    public ValidateException(String message, Throwable cause)
    {
        this(message, HttpStatus.BAD_REQUEST, cause);
    }

    public ValidateException(String message, HttpStatus errorCode, Throwable cause)
    {
        this.message = message;
        this.cause = cause;
        this.httpStatusCode = errorCode;
        if (cause != null)
        {
            this.stackTraceElements = cause.getStackTrace();
        }
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    @Override
    public Throwable getCause()
    {
        return cause;
    }

    public HttpStatus getHttpStatusCode()
    {
        return httpStatusCode;
    }

    @Override
    public String getMessage()
    {
        return this.message;
    }

    public StackTraceElement[] getStackTraceElements()
    {
        return stackTraceElements;
    }
}
