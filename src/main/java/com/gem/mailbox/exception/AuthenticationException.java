package com.gem.mailbox.exception;

import com.gem.mailbox.constant.Messages;
import org.springframework.http.HttpStatus;

/**
 * [File description]
 *
 * @author lent
 * @version 1.0
 * @since 4/29/14
 */
public class AuthenticationException extends Exception {
// ------------------------------ FIELDS ------------------------------

    // TODO: suport i18n, resource string instead
    private String message;
    private HttpStatus httpStatusCode;
    private Throwable cause;
    private StackTraceElement[] stackTraceElements;

// --------------------------- CONSTRUCTORS ---------------------------

    public AuthenticationException(String message) {
        this(message, HttpStatus.UNAUTHORIZED, null);
    }
    public AuthenticationException(Messages message) {
        this(message.toString(), HttpStatus.UNAUTHORIZED, null);
    }

    public AuthenticationException(String message, HttpStatus errorCode) {
        this(message, errorCode, null);
    }

    public AuthenticationException(String message, Throwable cause) {
        this(message, HttpStatus.UNAUTHORIZED, cause);
    }

    public AuthenticationException(String message, HttpStatus errorCode, Throwable cause) {
        this.message = message;
        this.cause = cause;
        this.httpStatusCode = errorCode;
        if (cause != null) {
            this.stackTraceElements = cause.getStackTrace();
        }
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    @Override
    public Throwable getCause() {
        return cause;
    }

    public HttpStatus getHttpStatusCode() {
        return httpStatusCode;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    public StackTraceElement[] getStackTraceElements() {
        return stackTraceElements;
    }
}
