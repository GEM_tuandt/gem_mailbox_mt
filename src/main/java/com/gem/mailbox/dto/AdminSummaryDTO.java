package com.gem.mailbox.dto;

import java.util.UUID;

/**
 * Letters summary
 * Created by neo on 1/30/2016.
 */
public class AdminSummaryDTO {
    private UUID userId;
    private int countScan;
    private int countSend;
    private int countTrash;

    public int getCountScan() {
        return countScan;
    }

    public void setCountScan(int countScan) {
        this.countScan = countScan;
    }

    public int getCountSend() {
        return countSend;
    }

    public void setCountSend(int countSend) {
        this.countSend = countSend;
    }

    public int getCountTrash() {
        return countTrash;
    }

    public void setCountTrash(int countTrash) {
        this.countTrash = countTrash;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }
}
