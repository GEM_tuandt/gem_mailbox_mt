package com.gem.mailbox.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * [File description]
 *
 * @author neo
 */
public class ErrorResponseDTO implements Serializable {
// ------------------------------ FIELDS ------------------------------

    @JsonProperty
    private int errorCode;
    @JsonProperty
    private String errorMessage;

// --------------------- CONSTRUCTOR ---------------------

    private ErrorResponseDTO(int errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public static ErrorResponseDTO newInstance(HttpStatus errorCode, String errorMessage) {
        return new ErrorResponseDTO(errorCode.value(), errorMessage);
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}
