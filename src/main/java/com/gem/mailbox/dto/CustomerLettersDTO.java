package com.gem.mailbox.dto;

import com.gem.mailbox.model.SendOrder;

import java.util.List;

/**
 * Letters of customer response
 * Include new/un-requested letters, letters are sent, and trashed letters
 * Created by neo on 2/1/2016.
 */
public class CustomerLettersDTO {
    private List<LetterDTO> letters;
    private List<LetterDTO> documentLetters;
    private List<SendOrderDTO> sendOrders;
    private List<LetterDTO> trashLetters;

    public CustomerLettersDTO(List<LetterDTO> letters, List<LetterDTO> documentLetters,
                              List<SendOrderDTO> sendOrders, List<LetterDTO> trashLetters) {
        this.letters = letters;
        this.sendOrders = sendOrders;
        this.trashLetters = trashLetters;
        this.documentLetters = documentLetters;
    }

    public List<LetterDTO> getLetters() {
        return letters;
    }

    public void setLetters(List<LetterDTO> letters) {
        this.letters = letters;
    }

    public List<LetterDTO> getTrashLetters() {
        return trashLetters;
    }

    public void setTrashLetters(List<LetterDTO> trashLetters) {
        this.trashLetters = trashLetters;
    }

    public List<SendOrderDTO> getSendOrders() {
        return sendOrders;
    }

    public void setSendOrders(List<SendOrderDTO> sendOrders) {
        this.sendOrders = sendOrders;
    }

    public List<LetterDTO> getDocumentLetters() {
        return documentLetters;
    }

    public void setDocumentLetters(List<LetterDTO> documentLetters) {
        this.documentLetters = documentLetters;
    }
}
