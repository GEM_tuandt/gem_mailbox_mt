package com.gem.mailbox.dto;

import com.gem.mailbox.model.constant.JobAction;
import com.gem.mailbox.model.constant.JobTarget;

import java.util.Date;
import java.util.UUID;

/**
 * Job History DTO
 * Created by neo on 2/26/2016.
 */
public class JobHistoryDTO extends BaseDTO {
    private JobAction jobAction;
    private UUID targetId;
    private JobTarget targetType;
    private Date actionDate;
    private UserDTO userDTO;

    public Date getActionDate() {
        return actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    public JobAction getJobAction() {
        return jobAction;
    }

    public void setJobAction(JobAction jobAction) {
        this.jobAction = jobAction;
    }

    public UUID getTargetId() {
        return targetId;
    }

    public void setTargetId(UUID targetId) {
        this.targetId = targetId;
    }

    public JobTarget getTargetType() {
        return targetType;
    }

    public void setTargetType(JobTarget targetType) {
        this.targetType = targetType;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }
}
