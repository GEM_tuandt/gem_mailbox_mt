package com.gem.mailbox.dto;

/**
 * Service Provider DTO
 * Created by neo on 2/2/2016.
 */
public class ServiceProviderDTO extends BaseDTO {
    private String providerName;

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }
}
