package com.gem.mailbox.dto;

/**
 * Validated QrCode DTO
 * Created by neo on 2/15/2016.
 */
public class ValidateQrCodeDTO {
    private Boolean validation;

    public ValidateQrCodeDTO(Boolean validation) {
        this.validation = validation;
    }

    public Boolean getValidation() {
        return validation;
    }

    public void setValidation(Boolean validation) {
        this.validation = validation;
    }
}
