package com.gem.mailbox.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gem.mailbox.model.Letter;
import com.gem.mailbox.model.ServiceProvider;

import java.util.Date;
import java.util.List;

/**
 * Send Order DTO
 * Created by neo on 2/2/2016.
 */
public class SendOrderDTO extends BaseDTO {
    @JsonProperty("letters")
    public List<LetterDTO> letterDTOs;
    private Long requireDate;
    private String sendAddress;
    @JsonProperty("serviceProvider")
    private ServiceProviderDTO serviceProviderDTO;
    private Date sendDate;
    private String note;
    private Float price;
    private UserDTO userDTO;
    private String trackingCode;
    private Boolean isArrived;

    public List<LetterDTO> getLetterDTOs() {
        return letterDTOs;
    }

    public void setLetterDTOs(List<LetterDTO> letterDTOs) {
        this.letterDTOs = letterDTOs;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Long getRequireDate() {
        return requireDate;
    }

    public void setRequireDate(Long requireDate) {
        this.requireDate = requireDate;
    }

    public String getSendAddress() {
        return sendAddress;
    }

    public void setSendAddress(String sendAddress) {
        this.sendAddress = sendAddress;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public ServiceProviderDTO getServiceProviderDTO() {
        return serviceProviderDTO;
    }

    public void setServiceProviderDTO(ServiceProviderDTO serviceProviderDTO) {
        this.serviceProviderDTO = serviceProviderDTO;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public Boolean getIsArrived() {
        return isArrived;
    }

    public void setIsArrived(Boolean isArrived) {
        this.isArrived = isArrived;
    }

    public String getTrackingCode() {
        return trackingCode;
    }

    public void setTrackingCode(String trackingCode) {
        this.trackingCode = trackingCode;
    }
}
