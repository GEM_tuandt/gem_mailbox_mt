package com.gem.mailbox.dto;

import com.gem.mailbox.model.constant.ImageType;
import com.gem.mailbox.model.LetterScan;

/**
 * DTO for scan of letter
 * Created by neo on 1/28/2016.
 */
public class LetterScanDTO extends BaseDTO {
    private String name;
    private ImageType type;
    private String downloadLink;

    public LetterScanDTO() {

    }

    public LetterScanDTO(LetterScan letterScan) {
        super(letterScan);
        this.name = letterScan.getName();
        this.type = letterScan.getImageType();
    }

    public LetterScanDTO(LetterScan letterScan, String downloadLink) {
        this(letterScan);
        this.downloadLink = downloadLink;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDownloadLink() {
        return downloadLink;
    }

    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    }

    public ImageType getType() {
        return type;
    }

    public void setType(ImageType type) {
        this.type = type;
    }
}
