package com.gem.mailbox.dto;

import java.util.List;

/**
 * Letters of customer response
 * Include new/un-requested letters, letters are sent, and trashed letters
 * Created by neo on 2/1/2016.
 */
public class AdminSendOrdersDTO {
    private List<SendOrderDTO> immediate;
    private List<SendOrderDTO> others;

    public AdminSendOrdersDTO(List<SendOrderDTO> immediate, List<SendOrderDTO> others) {
        this.immediate = immediate;
        this.others = others;
    }

    public List<SendOrderDTO> getImmediate() {
        return immediate;
    }

    public void setImmediate(List<SendOrderDTO> immediate) {
        this.immediate = immediate;
    }

    public List<SendOrderDTO> getOthers() {
        return others;
    }

    public void setOthers(List<SendOrderDTO> others) {
        this.others = others;
    }
}
