package com.gem.mailbox.dto;

import java.util.Date;

/**
 * LetterDTO
 * Created by neo on 1/29/2016.
 */
public class LetterDTO extends BaseDTO {
    private String thumbUrl;
    private String qrCode;
    private Boolean isNew;
    private Boolean isScan;
    private Boolean isSend;
    private Boolean isTrash;
    private Date dateTrash;
    private Date dateScan;
    private Date date;
    private String note;
    private UserDTO userDTO;
    private String title;
    private String letterFrom;
    private String letterTo;
    private String userNote;
    private Boolean isInDocument;

    public Date getDateTrash() {
        return dateTrash;
    }

    public void setDateTrash(Date dateTrash) {
        this.dateTrash = dateTrash;
    }

    public Boolean getIsNew() {
        return isNew;
    }

    public void setIsNew(Boolean isNew) {
        this.isNew = isNew;
    }

    public Boolean getIsScan() {
        return isScan;
    }

    public void setIsScan(Boolean isScan) {
        this.isScan = isScan;
    }

    public Boolean getIsSend() {
        return isSend;
    }

    public void setIsSend(Boolean isSend) {
        this.isSend = isSend;
    }

    public Boolean getIsTrash() {
        return isTrash;
    }

    public void setIsTrash(Boolean isTrash) {
        this.isTrash = isTrash;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLetterFrom() {
        return letterFrom;
    }

    public void setLetterFrom(String letterFrom) {
        this.letterFrom = letterFrom;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLetterTo() {
        return letterTo;
    }

    public void setLetterTo(String letterTo) {
        this.letterTo = letterTo;
    }

    public String getUserNote() {
        return userNote;
    }

    public void setUserNote(String userNote) {
        this.userNote = userNote;
    }

    public Date getDateScan() {
        return dateScan;
    }

    public void setDateScan(Date dateScan) {
        this.dateScan = dateScan;
    }

    public Boolean getIsInDocument() {
        return isInDocument;
    }

    public void setIsInDocument(Boolean isInDocument) {
        this.isInDocument = isInDocument;
    }
}
