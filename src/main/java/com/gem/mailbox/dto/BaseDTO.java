package com.gem.mailbox.dto;

import com.gem.mailbox.model.BaseEntity;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 2/24/15
 * Time: 4:27 PM
 */
public class BaseDTO implements Serializable {
    protected UUID uuid;

    public BaseDTO() {
    }

    public BaseDTO(BaseEntity baseEntity) {
        this.uuid = baseEntity.getUuid();
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
}
