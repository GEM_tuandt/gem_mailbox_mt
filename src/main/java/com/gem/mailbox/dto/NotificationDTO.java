package com.gem.mailbox.dto;

import com.gem.mailbox.model.BaseEntity;
import com.gem.mailbox.model.User;
import com.gem.mailbox.model.constant.JobAction;
import com.gem.mailbox.model.constant.JobTarget;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import java.util.UUID;

/**
 * Notification
 * Created by neo on 2/2/2016.
 */
public class NotificationDTO extends BaseEntity {
    private JobAction jobAction;
    private JobTarget jobTarget;
    private UUID targetId;
    private UserDTO userDTO;

    public JobAction getJobAction() {
        return jobAction;
    }

    public void setJobAction(JobAction jobAction) {
        this.jobAction = jobAction;
    }

    public JobTarget getJobTarget() {
        return jobTarget;
    }

    public void setJobTarget(JobTarget jobTarget) {
        this.jobTarget = jobTarget;
    }

    public UUID getTargetId() {
        return targetId;
    }

    public void setTargetId(UUID targetId) {
        this.targetId = targetId;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }
}
