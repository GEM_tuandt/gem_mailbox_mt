package com.gem.mailbox.dto;

import com.gem.mailbox.model.BaseEntity;
import com.gem.mailbox.model.User;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;

import javax.persistence.*;

/**
 * User's phone number
 * Created by neo on 2/2/2016.
 */
public class UserPhoneNumberDTO extends BaseDTO {
    private String phoneNumber;
    private Boolean isPrimary = false;
    private Boolean isDeleted = false;

    public Boolean getIsPrimary() {
        return isPrimary;
    }

    public void setIsPrimary(Boolean isPrimary) {
        this.isPrimary = isPrimary;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
}
