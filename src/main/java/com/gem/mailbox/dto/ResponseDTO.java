package com.gem.mailbox.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gem.mailbox.constant.Messages;
import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by quynhtq on 12/15/14.
 */
public class ResponseDTO<DTO> implements Serializable {
    private boolean success = true;

    @JsonProperty("data")
    private DTO result;

    @JsonProperty("error")
    private ErrorResponseDTO errorResponseDTO;

    public ResponseDTO() {

    }

    public ResponseDTO(ErrorResponseDTO errorResponseDTO) {
        this.errorResponseDTO = errorResponseDTO;
        success = false;
    }

    public ResponseDTO(DTO result) {
        this.result = result;
    }

    public DTO getResult() {
        return result;
    }

    public void setResult(DTO result) {
        this.result = result;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ErrorResponseDTO getErrorResponseDTO() {
        return errorResponseDTO;
    }

    public void setErrorResponseDTO(ErrorResponseDTO errorResponseDTO) {
        this.errorResponseDTO = errorResponseDTO;
    }

    public static ResponseDTO newErrorInstance(HttpStatus httpStatus, String msg) {
        return new ResponseDTO(ErrorResponseDTO.newInstance(httpStatus, msg));
    }

    public static ResponseDTO newErrorInstance(String msg) {
        return new ResponseDTO(ErrorResponseDTO.newInstance(HttpStatus.INTERNAL_SERVER_ERROR, msg));
    }

    public static ResponseDTO newErrorInstance(Messages msg) {
        return new ResponseDTO(ErrorResponseDTO.newInstance(HttpStatus.INTERNAL_SERVER_ERROR, msg.toString()));
    }
}

