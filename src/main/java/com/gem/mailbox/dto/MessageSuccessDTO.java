package com.gem.mailbox.dto;

/**
 * Response Success DTO
 * Created by neo on 2/17/2016.
 */
public class MessageSuccessDTO {
    private String message;

    public MessageSuccessDTO() {
        message = "Success";
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
