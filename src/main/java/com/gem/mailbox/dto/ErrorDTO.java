package com.gem.mailbox.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * [File description]
 *
 * @author lent
 * @version 1.0
 * @since 5/3/14
 */
public class ErrorDTO implements Serializable {
// ------------------------------ FIELDS ------------------------------

    @JsonProperty
    private String statusCode;
    @JsonProperty
    private String statusDescription;
    @JsonProperty
    private String errorMessage;

// --------------------- GETTER / SETTER METHODS ---------------------

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

// -------------------------- INNER CLASSES --------------------------

    public interface Properties {
        String STATUS_CODE = "statusCode";
        String STATUS_DESCRIPTION = "statusDescription";
        String ERROR_MESSAGE = "errorMessage";
        String ADDITION_INFO = "additionInfo";
        String OBJECT = "object";
    }
}
