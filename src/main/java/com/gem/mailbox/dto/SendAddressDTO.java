package com.gem.mailbox.dto;

/**
 * Send address DTO
 * Created by neo on 2/2/2016.
 */
public class SendAddressDTO extends BaseDTO {
    private String address;
    private Boolean isPrimary = false;
    private Boolean isDeleted = false;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Boolean getIsPrimary() {
        return isPrimary;
    }

    public void setIsPrimary(Boolean isPrimary) {
        this.isPrimary = isPrimary;
    }
}
