package com.gem.mailbox.dto;

import com.gem.mailbox.model.User;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * Created with IntelliJ IDEA.
 * User: neo
 */
public class UserDTO extends BaseDTO {
    private String firstName;
    private String lastName;
    private String avatarName;
    private Boolean isAdmin = false;
    private String address;
    private String phoneNumber;
    private List<SendAddressDTO> sendAddressDTOs;
    private List<OtherNameDTO> otherNameDTOs;
    private List<UserEmailDTO> userEmailDTOs;
    private List<UserPhoneNumberDTO> userPhoneNumberDTOs;

    // TODO removed NotNull NotBlank
    private String email;
    private String password;

    private String token;

    private List<LetterDTO> requestScanLetters;

    private List<LetterDTO> requestTrashLetters;


    public UserDTO() {
    }

//    public UserDTO(User user, String token) {
//        super(user);
//        this.firstName = user.getFirstName();
//        this.lastName = user.getLastName();
//        this.email = user.getEmail();
//        this.password = user.getPassword();
//        this.avatarName = user.getAvatarName();
//        this.token = token;
//    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAvatarName() {
        return avatarName;
    }

    public void setAvatarName(String avatarName) {
        this.avatarName = avatarName;
    }

    public Boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Boolean isAdminUser) {
        this.isAdmin = isAdminUser;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<LetterDTO> getRequestScanLetters() {
        return requestScanLetters;
    }

    public void setRequestScanLetters(List<LetterDTO> requestScanLetters) {
        this.requestScanLetters = requestScanLetters;
    }

    public List<LetterDTO> getRequestTrashLetters() {
        return requestTrashLetters;
    }

    public void setRequestTrashLetters(List<LetterDTO> requestTrashLetters) {
        this.requestTrashLetters = requestTrashLetters;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<OtherNameDTO> getOtherNameDTOs() {
        return otherNameDTOs;
    }

    public void setOtherNameDTOs(List<OtherNameDTO> otherNameDTOs) {
        this.otherNameDTOs = otherNameDTOs;
    }

    public List<SendAddressDTO> getSendAddressDTOs() {
        return sendAddressDTOs;
    }

    public void setSendAddressDTOs(List<SendAddressDTO> sendAddressDTOs) {
        this.sendAddressDTOs = sendAddressDTOs;
    }

    public List<UserEmailDTO> getUserEmailDTOs() {
        return userEmailDTOs;
    }

    public void setUserEmailDTOs(List<UserEmailDTO> userEmailDTOs) {
        this.userEmailDTOs = userEmailDTOs;
    }

    public List<UserPhoneNumberDTO> getUserPhoneNumberDTOs() {
        return userPhoneNumberDTOs;
    }

    public void setUserPhoneNumberDTOs(List<UserPhoneNumberDTO> userPhoneNumberDTOs) {
        this.userPhoneNumberDTOs = userPhoneNumberDTOs;
    }
}
