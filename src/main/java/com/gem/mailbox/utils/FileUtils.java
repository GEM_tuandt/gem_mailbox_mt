package com.gem.mailbox.utils;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * File Utils
 * Created by neo on 2/18/2016.
 */
public class FileUtils {
    public static String readQuery(String fileName) {
        return readFile(FileUtils.class, "/query/" + fileName + ".query");
    }

    /**
     * Get text content from resource file
     */
    public static String readFile(Class clazz, String fileName) {
        StringBuilder result = new StringBuilder("");

        //Get file from resources folder
        ClassLoader classLoader = clazz.getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try (Scanner scanner = new Scanner(file)) {

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                result.append(line).append("\n");
            }

            scanner.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return result.toString();
    }
}
