package com.gem.mailbox.utils;

import com.gem.mailbox.Application;
import com.gem.mailbox.config.ConfigConstants;
import org.apache.commons.lang.StringUtils;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.properties.EncryptableProperties;

import java.io.IOException;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 2/26/15
 * Time: 2:22 PM
 */
public class JasyptSecureUtils
{
    public static String getSecuredValue(String encrypted) throws IOException
    {
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        String jasyptKey = System.getProperty("tadirect.jasypt.key");
        if (StringUtils.isEmpty(jasyptKey))
        {
            jasyptKey = ConfigConstants.DEFAULT_PROPERTY_ENCRYPTOR_MASTER_KEY; // TODO: This could be a bootstrap value that interact with license.gemvietnam.com
        }
        encryptor.setPassword(jasyptKey);
        Properties props = new EncryptableProperties(encryptor);
        props.load(Application.class.getClassLoader().getResourceAsStream("application.properties"));
        return props.getProperty(encrypted);
    }
}
