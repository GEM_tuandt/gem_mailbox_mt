package com.gem.mailbox.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by quynhtq on 9/7/2015.
 */
public class DateUtils {
    private static final String dateTemplate = "MM/dd/yyyy";
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateTemplate);

    public static String convert(Date date) {
        if (date == null) {
            return null;
        }
        return simpleDateFormat.format(date);
    }
}
