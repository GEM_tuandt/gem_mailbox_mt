package com.gem.mailbox.utils;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * String utilities
 * Created by neo on 2/19/2016.
 */
public class StringUtil {

    /**
     * Separate by white space and join with <b>|</b> then
     *
     * @param keyword to search/find will be separate by white space
     * @return keywords is separated will be join by <b>|</b> to put on query statement
     */
    public static String keywordToQueryParam(String keyword) {
        keyword = keyword.trim();
        String[] keywords = keyword.split("\\s+");
        return StringUtils.join(keywords, "|");
    }

    /**
     * Split uuid in string and parse to list of UUID
     *
     * @param ids uuid strings that are joined by <b>,</b>
     * @return Split String and parse to {@link java.util.UUID} list
     * @throws IllegalArgumentException
     */
    public static List<UUID> idsToUUIDList(String ids) throws IllegalArgumentException {
        List<UUID> uuids = new ArrayList<>();
        if (StringUtils.isBlank(ids)) {
            return uuids;
        }

        String[] arrIds = ids.split(",");
        for (String id : arrIds) {
            uuids.add(UUID.fromString(id));
        }

        return uuids;
    }

    /**
     * Split uuid in string and parse to list of UUID
     *
     * @param ids uuid strings
     * @return {@link java.util.UUID} list
     * @throws IllegalArgumentException
     */
    public static List<UUID> idsToUUIDList(List<String> ids) throws IllegalArgumentException {
        List<UUID> uuids = new ArrayList<>();

        if (ids == null || ids.isEmpty()) {
            return uuids;
        }

        for (String id : ids) {
            uuids.add(UUID.fromString(id));
        }

        return uuids;
    }

    /**
     * Format list of values to 'value1','value2',...
     * @param values values joined by ,
     */
    public static String listToValuesParam(String values) {
        if (StringUtils.isBlank(values)) {
            return "''";
        }
        String[] arrValues = values.split(",");
        for (int i = 0; i < arrValues.length; i++) {
            arrValues[i] = "'" + arrValues[i] + "'";
        }
        return StringUtils.join(arrValues, ",");
    }

}
