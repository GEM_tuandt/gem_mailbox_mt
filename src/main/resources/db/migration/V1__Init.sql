/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : PostgreSQL
 Source Server Version : 90400
 Source Host           : localhost
 Source Database       : chartnotes
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 90400
 File Encoding         : utf-8

 Date: 03/13/2015 11:35:34 AM
*/

-- ----------------------------
--  Table structure for configuration
-- ----------------------------
DROP TABLE IF EXISTS "configuration";
CREATE TABLE "configuration" (
  "uuid"         VARCHAR(36)  NOT NULL COLLATE "default",
  "date_created" TIMESTAMP(6) NULL,
  "date_updated" TIMESTAMP(6) NULL,
  "deleted"      BOOL,
  "key"          VARCHAR(255) COLLATE "default",
  "value"        VARCHAR(255) COLLATE "default"
)
WITH (OIDS =FALSE
);
ALTER TABLE "configuration" OWNER TO "postgres";

-- ----------------------------
--  Table structure for ocular
-- ----------------------------
DROP TABLE IF EXISTS "ocular";
CREATE TABLE "ocular" (
  "uuid"         VARCHAR(36)  NOT NULL COLLATE "default",
  "date_created" TIMESTAMP(6) NULL,
  "date_updated" TIMESTAMP(6) NULL,
  "deleted"      BOOL,
  "axis"         VARCHAR(255) COLLATE "default",
  "base"         VARCHAR(255) COLLATE "default",
  "cylinder"     VARCHAR(255) COLLATE "default",
  "prism"        VARCHAR(255) COLLATE "default",
  "sphere"       VARCHAR(255) COLLATE "default",
  "user_id"      VARCHAR(36)  NOT NULL COLLATE "default"
)
WITH (OIDS =FALSE
);
ALTER TABLE "ocular" OWNER TO "postgres";

-- ----------------------------
--  Table structure for key_management
-- ----------------------------
DROP TABLE IF EXISTS "key_management";
CREATE TABLE "key_management" (
  "uuid"         VARCHAR(36)  NOT NULL COLLATE "default",
  "date_created" TIMESTAMP(6) NULL,
  "date_updated" TIMESTAMP(6) NULL,
  "deleted"      BOOL,
  "count"        INT4,
  "key_id"       VARCHAR(255) COLLATE "default"
)
WITH (OIDS =FALSE
);
ALTER TABLE "key_management" OWNER TO "postgres";

-- ----------------------------
--  Table structure for medication
-- ----------------------------
DROP TABLE IF EXISTS "medication";
CREATE TABLE "medication" (
  "uuid"               VARCHAR(36)  NOT NULL COLLATE "default",
  "date_created"       TIMESTAMP(6) NULL,
  "date_updated"       TIMESTAMP(6) NULL,
  "deleted"            BOOL,
  "dosage"             VARCHAR(255) COLLATE "default",
  "health_problem"     VARCHAR(255) COLLATE "default",
  "name"               VARCHAR(255) NOT NULL COLLATE "default",
  "note"               VARCHAR(255) COLLATE "default",
  "administrator_id"   VARCHAR(36) COLLATE "default",
  "profile_id"         VARCHAR(36)  NOT NULL COLLATE "default",
  "supplier_record_id" VARCHAR(36) COLLATE "default",
  "user_id"            VARCHAR(36)  NOT NULL COLLATE "default"
)
WITH (OIDS =FALSE
);
ALTER TABLE "medication" OWNER TO "postgres";

-- ----------------------------
--  Table structure for user_account_temp
-- ----------------------------
DROP TABLE IF EXISTS "user_account_temp";
CREATE TABLE "user_account_temp" (
  "uuid"                 VARCHAR(36)  NOT NULL COLLATE "default",
  "date_created"         TIMESTAMP(6) NULL,
  "date_updated"         TIMESTAMP(6) NULL,
  "deleted"              BOOL,
  "activation_code"      VARCHAR(255) COLLATE "default",
  "activation_expire_on" TIMESTAMP(6) NULL,
  "email"                VARCHAR(255) NOT NULL COLLATE "default",
  "encryption_code"      VARCHAR(255) COLLATE "default",
  "full_name"            VARCHAR(255) NOT NULL COLLATE "default",
  "password"             VARCHAR(255) NOT NULL COLLATE "default"
)
WITH (OIDS =FALSE
);
ALTER TABLE "user_account_temp" OWNER TO "postgres";

-- ----------------------------
--  Table structure for administrator
-- ----------------------------
DROP TABLE IF EXISTS "administrator";
CREATE TABLE "administrator" (
  "uuid"         VARCHAR(36)  NOT NULL COLLATE "default",
  "date_created" TIMESTAMP(6) NULL,
  "date_updated" TIMESTAMP(6) NULL,
  "deleted"      BOOL,
  "cert"         VARCHAR(255) COLLATE "default",
  "name"         VARCHAR(255) COLLATE "default",
  "signature"    BYTEA,
  "user_id"      VARCHAR(36)  NOT NULL COLLATE "default"
)
WITH (OIDS =FALSE
);
ALTER TABLE "administrator" OWNER TO "postgres";

-- ----------------------------
--  Table structure for user_account
-- ----------------------------
DROP TABLE IF EXISTS "user_account";
CREATE TABLE "user_account" (
  "uuid"                          VARCHAR(36)  NOT NULL COLLATE "default",
  "date_created"                  TIMESTAMP(6) NULL,
  "date_updated"                  TIMESTAMP(6) NULL,
  "deleted"                       BOOL,
  "email"                         VARCHAR(255) NOT NULL COLLATE "default",
  "encryption_code"               VARCHAR(255) COLLATE "default",
  "full_name"                     VARCHAR(255) NOT NULL COLLATE "default",
  "password"                      VARCHAR(255) NOT NULL COLLATE "default",
  "reset_password_code"           VARCHAR(255) COLLATE "default",
  "reset_password_code_expire_on" TIMESTAMP(6) NULL
)
WITH (OIDS =FALSE
);
ALTER TABLE "user_account" OWNER TO "postgres";

-- ----------------------------
--  Table structure for supplier_record
-- ----------------------------
DROP TABLE IF EXISTS "supplier_record";
CREATE TABLE "supplier_record" (
  "uuid"         VARCHAR(36)  NOT NULL COLLATE "default",
  "date_created" TIMESTAMP(6) NULL,
  "date_updated" TIMESTAMP(6) NULL,
  "deleted"      BOOL,
  "type"         INT4,
  "website"      VARCHAR(255) COLLATE "default",
  "user_id"      VARCHAR(36)  NOT NULL COLLATE "default"
)
WITH (OIDS =FALSE
);
ALTER TABLE "supplier_record" OWNER TO "postgres";

-- ----------------------------
--  Table structure for vaccination
-- ----------------------------
DROP TABLE IF EXISTS "vaccination";
CREATE TABLE "vaccination" (
  "uuid"               VARCHAR(36)  NOT NULL COLLATE "default",
  "date_created"       TIMESTAMP(6) NULL,
  "date_updated"       TIMESTAMP(6) NULL,
  "deleted"            BOOL,
  "date"               TIMESTAMP(6) NOT NULL,
  "dose"               VARCHAR(255) COLLATE "default",
  "exp_date"           TIMESTAMP(6) NULL,
  "lot_number"         VARCHAR(255) COLLATE "default",
  "manufacturer"       VARCHAR(255) COLLATE "default",
  "protect_against"    VARCHAR(255) COLLATE "default",
  "vaccine"            VARCHAR(255) NOT NULL COLLATE "default",
  "administrator_id"   VARCHAR(36) COLLATE "default",
  "profile_id"         VARCHAR(36)  NOT NULL COLLATE "default",
  "supplier_record_id" VARCHAR(36) COLLATE "default",
  "user_id"            VARCHAR(36)  NOT NULL COLLATE "default"
)
WITH (OIDS =FALSE
);
ALTER TABLE "vaccination" OWNER TO "postgres";

-- ----------------------------
--  Table structure for user_session
-- ----------------------------
DROP TABLE IF EXISTS "user_session";
CREATE TABLE "user_session" (
  "uuid"            VARCHAR(36)  NOT NULL COLLATE "default",
  "date_created"    TIMESTAMP(6) NULL,
  "date_updated"    TIMESTAMP(6) NULL,
  "deleted"         BOOL,
  "device_token"    VARCHAR(255) COLLATE "default",
  "token"           VARCHAR(255) COLLATE "default",
  "token_expire_on" TIMESTAMP(6) NULL,
  "user_id"         VARCHAR(36) COLLATE "default"
)
WITH (OIDS =FALSE
);
ALTER TABLE "user_session" OWNER TO "postgres";

-- ----------------------------
--  Table structure for supplier
-- ----------------------------
DROP TABLE IF EXISTS "supplier";
CREATE TABLE "supplier" (
  "uuid"         VARCHAR(36)  NOT NULL COLLATE "default",
  "date_created" TIMESTAMP(6) NULL,
  "date_updated" TIMESTAMP(6) NULL,
  "deleted"      BOOL,
  "type"         INT4,
  "website"      VARCHAR(255) COLLATE "default",
  "user_id"      VARCHAR(36)  NOT NULL COLLATE "default"
)
WITH (OIDS =FALSE
);
ALTER TABLE "supplier" OWNER TO "postgres";

-- ----------------------------
--  Table structure for profile_supplier
-- ----------------------------
DROP TABLE IF EXISTS "profile_supplier";
CREATE TABLE "profile_supplier" (
  "profile_id"  VARCHAR(36) NOT NULL COLLATE "default",
  "supplier_id" VARCHAR(36) NOT NULL COLLATE "default"
)
WITH (OIDS =FALSE
);
ALTER TABLE "profile_supplier" OWNER TO "postgres";

-- ----------------------------
--  Table structure for profile
-- ----------------------------
DROP TABLE IF EXISTS "profile";
CREATE TABLE "profile" (
  "uuid"           VARCHAR(36)  NOT NULL COLLATE "default",
  "date_created"   TIMESTAMP(6) NULL,
  "date_updated"   TIMESTAMP(6) NULL,
  "deleted"        BOOL,
  "birthday"       TIMESTAMP(6) NULL,
  "blood_type"     INT4,
  "main"           BOOL         NOT NULL,
  "biological_sex" INT4,
  "user_id"        VARCHAR(36)  NOT NULL COLLATE "default"
)
WITH (OIDS =FALSE
);
ALTER TABLE "profile" OWNER TO "postgres";

-- ----------------------------
--  Table structure for communication_detail
-- ----------------------------
DROP TABLE IF EXISTS "communication_detail";
CREATE TABLE "communication_detail" (
  "uuid"                 VARCHAR(36)  NOT NULL COLLATE "default",
  "date_created"         TIMESTAMP(6) NULL,
  "date_updated"         TIMESTAMP(6) NULL,
  "deleted"              BOOL,
  "country_code"         VARCHAR(255) COLLATE "default",
  "postfix"              VARCHAR(255) COLLATE "default",
  "prefix"               VARCHAR(255) COLLATE "default",
  "communication_method" INT4,
  "value"                VARCHAR(255) NOT NULL COLLATE "default",
  "profile_id"           VARCHAR(36) COLLATE "default",
  "supplier_id"          VARCHAR(36) COLLATE "default",
  "supplier_record_id"   VARCHAR(36) COLLATE "default",
  "user_id"              VARCHAR(36)  NOT NULL COLLATE "default"
)
WITH (OIDS =FALSE
);
ALTER TABLE "communication_detail" OWNER TO "postgres";

-- ----------------------------
--  Table structure for contact
-- ----------------------------
DROP TABLE IF EXISTS "contact";
CREATE TABLE "contact" (
  "uuid"               VARCHAR(36)  NOT NULL COLLATE "default",
  "date_created"       TIMESTAMP(6) NULL,
  "date_updated"       TIMESTAMP(6) NULL,
  "deleted"            BOOL,
  "city"               VARCHAR(255) COLLATE "default",
  "countryCode"            VARCHAR(255) COLLATE "default",
  "countyCode"             VARCHAR(255) COLLATE "default",
  "full_name"          VARCHAR(255) NOT NULL COLLATE "default",
  "state"              VARCHAR(255) COLLATE "default",
  "street"             VARCHAR(255) COLLATE "default",
  "zip_code"           VARCHAR(255) COLLATE "default",
  "profile_id"         VARCHAR(36) COLLATE "default",
  "supplier_id"        VARCHAR(36) COLLATE "default",
  "supplier_record_id" VARCHAR(36) COLLATE "default",
  "user_id"            VARCHAR(36)  NOT NULL COLLATE "default"
)
WITH (OIDS =FALSE
);
ALTER TABLE "contact" OWNER TO "postgres";

-- ----------------------------
--  Table structure for allergy_sensitivity
-- ----------------------------
DROP TABLE IF EXISTS "allergy_sensitivity";
CREATE TABLE "allergy_sensitivity" (
  "uuid"         VARCHAR(36)  NOT NULL COLLATE "default",
  "date_created" TIMESTAMP(6) NULL,
  "date_updated" TIMESTAMP(6) NULL,
  "deleted"      BOOL,
  "medications"  VARCHAR(255) COLLATE "default",
  "name"         VARCHAR(255) NOT NULL COLLATE "default",
  "note"         VARCHAR(255) COLLATE "default",
  "profile_id"   VARCHAR(36)  NOT NULL COLLATE "default",
  "user_id"      VARCHAR(36)  NOT NULL COLLATE "default"
)
WITH (OIDS =FALSE
);
ALTER TABLE "allergy_sensitivity" OWNER TO "postgres";

-- ----------------------------
--  Table structure for letterScan
-- ----------------------------
DROP TABLE IF EXISTS "letterScan";
CREATE TABLE "letterScan" (
  "uuid"                   VARCHAR(36)  NOT NULL COLLATE "default",
  "date_created"           TIMESTAMP(6) NULL,
  "date_updated"           TIMESTAMP(6) NULL,
  "deleted"                BOOL,
  "type"                   INT4,
  "index"                  INT4,
  "name"                   VARCHAR(255) COLLATE "default",
  "object_key"             VARCHAR(255) NOT NULL COLLATE "default",
  "allergy_sensitivity_id" VARCHAR(36) COLLATE "default",
  "eye_prescription_id"    VARCHAR(36) COLLATE "default",
  "medication_id"          VARCHAR(36) COLLATE "default",
  "profile_id"             VARCHAR(36) COLLATE "default",
  "user_id"                VARCHAR(36)  NOT NULL COLLATE "default",
  "vaccination_id"         VARCHAR(36) COLLATE "default"
)
WITH (OIDS =FALSE
);
ALTER TABLE "letterScan" OWNER TO "postgres";

-- ----------------------------
--  Table structure for eye_prescription
-- ----------------------------
DROP TABLE IF EXISTS "eye_prescription";
CREATE TABLE "eye_prescription" (
  "uuid"               VARCHAR(36)  NOT NULL COLLATE "default",
  "date_created"       TIMESTAMP(6) NULL,
  "date_updated"       TIMESTAMP(6) NULL,
  "deleted"            BOOL,
  "date"               TIMESTAMP(6) NOT NULL,
  "exp_date"           TIMESTAMP(6) NULL,
  "type"               INT4         NOT NULL,
  "administrator_id"   VARCHAR(36) COLLATE "default",
  "ocular_dexter_id"   VARCHAR(36) COLLATE "default",
  "ocular_sinister_id" VARCHAR(36) COLLATE "default",
  "profile_id"         VARCHAR(36)  NOT NULL COLLATE "default",
  "supplier_record_id" VARCHAR(36) COLLATE "default",
  "user_id"            VARCHAR(36)  NOT NULL COLLATE "default"
)
WITH (OIDS =FALSE
);
ALTER TABLE "eye_prescription" OWNER TO "postgres";

-- ----------------------------
--  Primary key structure for table configuration
-- ----------------------------
ALTER TABLE "configuration" ADD PRIMARY KEY ("uuid") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table configuration
-- ----------------------------
CREATE INDEX "configuration_index" ON "configuration" USING BTREE (uuid COLLATE
                                                                   "default" ASC NULLS LAST, date_updated ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table ocular
-- ----------------------------
ALTER TABLE "ocular" ADD PRIMARY KEY ("uuid") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table ocular
-- ----------------------------
CREATE INDEX "ocular_index" ON "ocular" USING BTREE (uuid COLLATE
                                                     "default" ASC NULLS LAST, date_updated ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table key_management
-- ----------------------------
ALTER TABLE "key_management" ADD PRIMARY KEY ("uuid") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table key_management
-- ----------------------------
CREATE INDEX "key_management_index" ON "key_management" USING BTREE (uuid COLLATE
                                                                     "default" ASC NULLS LAST, date_updated ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table medication
-- ----------------------------
ALTER TABLE "medication" ADD PRIMARY KEY ("uuid") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table medication
-- ----------------------------
CREATE INDEX "medication_index" ON "medication" USING BTREE (uuid COLLATE
                                                             "default" ASC NULLS LAST, date_updated ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table user_account_temp
-- ----------------------------
ALTER TABLE "user_account_temp" ADD PRIMARY KEY ("uuid") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table user_account_temp
-- ----------------------------
CREATE INDEX "user_account_temp_index" ON "user_account_temp" USING BTREE (uuid COLLATE
                                                                           "default" ASC NULLS LAST, date_updated ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table administrator
-- ----------------------------
ALTER TABLE "administrator" ADD PRIMARY KEY ("uuid") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table administrator
-- ----------------------------
CREATE INDEX "administrator_index" ON "administrator" USING BTREE (uuid COLLATE
                                                                   "default" ASC NULLS LAST, date_updated ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table user_account
-- ----------------------------
ALTER TABLE "user_account" ADD PRIMARY KEY ("uuid") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table user_account
-- ----------------------------
CREATE INDEX "user_account_index" ON "user_account" USING BTREE (uuid COLLATE
                                                                 "default" ASC NULLS LAST, date_updated ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table supplier_record
-- ----------------------------
ALTER TABLE "supplier_record" ADD PRIMARY KEY ("uuid") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table supplier_record
-- ----------------------------
CREATE INDEX "supplier_record_index" ON "supplier_record" USING BTREE (uuid COLLATE
                                                                       "default" ASC NULLS LAST, date_updated ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table vaccination
-- ----------------------------
ALTER TABLE "vaccination" ADD PRIMARY KEY ("uuid") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table vaccination
-- ----------------------------
CREATE INDEX "vaccination_index" ON "vaccination" USING BTREE (uuid COLLATE
                                                               "default" ASC NULLS LAST, date_updated ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table user_session
-- ----------------------------
ALTER TABLE "user_session" ADD PRIMARY KEY ("uuid") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table user_session
-- ----------------------------
CREATE INDEX "user_session_index" ON "user_session" USING BTREE (uuid COLLATE
                                                                 "default" ASC NULLS LAST, date_updated ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table supplier
-- ----------------------------
ALTER TABLE "supplier" ADD PRIMARY KEY ("uuid") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table supplier
-- ----------------------------
CREATE INDEX "supplier_index" ON "supplier" USING BTREE (uuid COLLATE
                                                         "default" ASC NULLS LAST, date_updated ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table profile
-- ----------------------------
ALTER TABLE "profile" ADD PRIMARY KEY ("uuid") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table profile
-- ----------------------------
CREATE INDEX "profile_index" ON "profile" USING BTREE (uuid COLLATE
                                                       "default" ASC NULLS LAST, date_updated ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table communication_detail
-- ----------------------------
ALTER TABLE "communication_detail" ADD PRIMARY KEY ("uuid") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table communication_detail
-- ----------------------------
CREATE INDEX "communication_detail_index" ON "communication_detail" USING BTREE (uuid COLLATE
                                                                                 "default" ASC NULLS LAST, date_updated ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table contact
-- ----------------------------
ALTER TABLE "contact" ADD PRIMARY KEY ("uuid") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table contact
-- ----------------------------
CREATE INDEX "contact_index" ON "contact" USING BTREE (uuid COLLATE
                                                       "default" ASC NULLS LAST, date_updated ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table allergy_sensitivity
-- ----------------------------
ALTER TABLE "allergy_sensitivity" ADD PRIMARY KEY ("uuid") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table allergy_sensitivity
-- ----------------------------
CREATE INDEX "allergy_sensitivity_index" ON "allergy_sensitivity" USING BTREE (uuid COLLATE
                                                                               "default" ASC NULLS LAST, date_updated ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table letterScan
-- ----------------------------
ALTER TABLE "letterScan" ADD PRIMARY KEY ("uuid") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table letterScan
-- ----------------------------
CREATE INDEX "document_index" ON "letterScan" USING BTREE (uuid COLLATE
                                                         "default" ASC NULLS LAST, date_updated ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table eye_prescription
-- ----------------------------
ALTER TABLE "eye_prescription" ADD PRIMARY KEY ("uuid") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table eye_prescription
-- ----------------------------
CREATE INDEX "eye_prescription_index" ON "eye_prescription" USING BTREE (uuid COLLATE
                                                                         "default" ASC NULLS LAST, date_updated ASC NULLS LAST);

